import { Field } from '@nestjs/graphql';
import { ObjectType } from '@nestjs/graphql';
import { ArgsType } from '@nestjs/graphql';
import { Type } from 'class-transformer';
import { ValidateNested } from 'class-validator';
import { Int } from '@nestjs/graphql';
import { InputType } from '@nestjs/graphql';
import { HideField } from '@nestjs/graphql';
import * as Validator from 'class-validator';
import { registerEnumType } from '@nestjs/graphql';
import { ID } from '@nestjs/graphql';
import { Decimal } from '@prisma/client/runtime';
import { GraphQLDecimal } from 'prisma-graphql-type-decimal';
import { transformToDecimal } from 'prisma-graphql-type-decimal';
import { Transform } from 'class-transformer';
import { Float } from '@nestjs/graphql';

export enum UserScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    email = "email",
    name = "name",
    password = "password",
    phone = "phone",
    roles = "roles"
}

export enum ToppingScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    description = "description",
    name = "name",
    price = "price",
    optionId = "optionId",
    ingredientLabelId = "ingredientLabelId"
}

export enum RestaurantScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    address = "address",
    city = "city"
}

export enum TransactionIsolationLevel {
    ReadUncommitted = "ReadUncommitted",
    ReadCommitted = "ReadCommitted",
    RepeatableRead = "RepeatableRead",
    Serializable = "Serializable"
}

export enum SortOrder {
    asc = "asc",
    desc = "desc"
}

export enum QueryMode {
    'default' = "default",
    insensitive = "insensitive"
}

export enum EnumRole {
    USER = "USER",
    ADMIN = "ADMIN"
}

export enum EnumPaymentType {
    CASH = "CASH",
    CARD = "CARD"
}

export enum EnumOrderType {
    DELIVERY = "DELIVERY",
    PICKUP = "PICKUP"
}

export enum EnumOrderStatus {
    PENDING = "PENDING",
    SHIPPED = "SHIPPED",
    COMPLETED = "COMPLETED",
    CANCELED = "CANCELED"
}

export enum OrderItemScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    quantity = "quantity",
    price = "price",
    orderId = "orderId",
    goodId = "goodId",
    customerDishId = "customerDishId"
}

export enum OrderScalarFieldEnum {
    id = "id",
    number = "number",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    status = "status",
    orderType = "orderType",
    paymentType = "paymentType",
    totalPrice = "totalPrice",
    description = "description",
    deliveryAddress = "deliveryAddress",
    changeFor = "changeFor",
    userId = "userId",
    restaurantId = "restaurantId"
}

export enum OptionsScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    description = "description",
    price = "price"
}

export enum IngradientLabelScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    image = "image"
}

export enum GoodScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    description = "description",
    price = "price",
    images = "images",
    slug = "slug",
    categoryId = "categoryId"
}

export enum DishScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    description = "description",
    price = "price",
    images = "images",
    slug = "slug",
    categoryId = "categoryId"
}

export enum CustomerDishScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    dishId = "dishId",
    optionsId = "optionsId",
    price = "price"
}

export enum CategoryScalarFieldEnum {
    id = "id",
    createdAt = "createdAt",
    updatedAt = "updatedAt",
    name = "name",
    image = "image"
}

registerEnumType(CategoryScalarFieldEnum, { name: 'CategoryScalarFieldEnum', description: undefined })
registerEnumType(CustomerDishScalarFieldEnum, { name: 'CustomerDishScalarFieldEnum', description: undefined })
registerEnumType(DishScalarFieldEnum, { name: 'DishScalarFieldEnum', description: undefined })
registerEnumType(GoodScalarFieldEnum, { name: 'GoodScalarFieldEnum', description: undefined })
registerEnumType(IngradientLabelScalarFieldEnum, { name: 'IngradientLabelScalarFieldEnum', description: undefined })
registerEnumType(OptionsScalarFieldEnum, { name: 'OptionsScalarFieldEnum', description: undefined })
registerEnumType(OrderScalarFieldEnum, { name: 'OrderScalarFieldEnum', description: undefined })
registerEnumType(OrderItemScalarFieldEnum, { name: 'OrderItemScalarFieldEnum', description: undefined })
registerEnumType(EnumOrderStatus, { name: 'EnumOrderStatus', description: undefined })
registerEnumType(EnumOrderType, { name: 'EnumOrderType', description: undefined })
registerEnumType(EnumPaymentType, { name: 'EnumPaymentType', description: undefined })
registerEnumType(EnumRole, { name: 'EnumRole', description: undefined })
registerEnumType(QueryMode, { name: 'QueryMode', description: undefined })
registerEnumType(SortOrder, { name: 'SortOrder', description: undefined })
registerEnumType(TransactionIsolationLevel, { name: 'TransactionIsolationLevel', description: undefined })
registerEnumType(RestaurantScalarFieldEnum, { name: 'RestaurantScalarFieldEnum', description: undefined })
registerEnumType(ToppingScalarFieldEnum, { name: 'ToppingScalarFieldEnum', description: undefined })
registerEnumType(UserScalarFieldEnum, { name: 'UserScalarFieldEnum', description: undefined })

@ObjectType()
export class AggregateCategory {
    @Field(() => CategoryCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CategoryCountAggregate>;
    @Field(() => CategoryMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CategoryMinAggregate>;
    @Field(() => CategoryMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CategoryMaxAggregate>;
}

@ArgsType()
export class CategoryAggregateArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => [CategoryOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CategoryOrderByWithRelationInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CategoryCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CategoryCountAggregateInput>;
    @Field(() => CategoryMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CategoryMinAggregateInput>;
    @Field(() => CategoryMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CategoryMaxAggregateInput>;
}

@InputType()
export class CategoryCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class CategoryCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    image!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class CategoryCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@ObjectType()
export class CategoryCount {
    @Field(() => Int, {nullable:false})
    dishes?: number;
    @Field(() => Int, {nullable:false})
    goods?: number;
}

@InputType()
export class CategoryCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
}

@InputType()
export class CategoryCreateNestedOneWithoutDishesInput {
    @Field(() => CategoryCreateWithoutDishesInput, {nullable:true})
    @Type(() => CategoryCreateWithoutDishesInput)
    create?: InstanceType<typeof CategoryCreateWithoutDishesInput>;
    @Field(() => CategoryCreateOrConnectWithoutDishesInput, {nullable:true})
    @Type(() => CategoryCreateOrConnectWithoutDishesInput)
    connectOrCreate?: InstanceType<typeof CategoryCreateOrConnectWithoutDishesInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    @Type(() => CategoryWhereUniqueInput)
    connect?: InstanceType<typeof CategoryWhereUniqueInput>;
}

@InputType()
export class CategoryCreateNestedOneWithoutGoodsInput {
    @Field(() => CategoryCreateWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryCreateWithoutGoodsInput)
    create?: InstanceType<typeof CategoryCreateWithoutGoodsInput>;
    @Field(() => CategoryCreateOrConnectWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryCreateOrConnectWithoutGoodsInput)
    connectOrCreate?: InstanceType<typeof CategoryCreateOrConnectWithoutGoodsInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    @Type(() => CategoryWhereUniqueInput)
    connect?: InstanceType<typeof CategoryWhereUniqueInput>;
}

@InputType()
export class CategoryCreateOrConnectWithoutDishesInput {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => CategoryCreateWithoutDishesInput, {nullable:false})
    @Type(() => CategoryCreateWithoutDishesInput)
    create!: InstanceType<typeof CategoryCreateWithoutDishesInput>;
}

@InputType()
export class CategoryCreateOrConnectWithoutGoodsInput {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => CategoryCreateWithoutGoodsInput, {nullable:false})
    @Type(() => CategoryCreateWithoutGoodsInput)
    create!: InstanceType<typeof CategoryCreateWithoutGoodsInput>;
}

@InputType()
export class CategoryCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => GoodCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => GoodCreateNestedManyWithoutCategoryInput)
    goods?: InstanceType<typeof GoodCreateNestedManyWithoutCategoryInput>;
}

@InputType()
export class CategoryCreateWithoutGoodsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutCategoryInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutCategoryInput>;
}

@InputType()
export class CategoryCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutCategoryInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutCategoryInput>;
    @Field(() => GoodCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => GoodCreateNestedManyWithoutCategoryInput)
    goods?: InstanceType<typeof GoodCreateNestedManyWithoutCategoryInput>;
}

@ArgsType()
export class CategoryGroupByArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => [CategoryOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<CategoryOrderByWithAggregationInput>;
    @Field(() => [CategoryScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof CategoryScalarFieldEnum>;
    @Field(() => CategoryScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof CategoryScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CategoryCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CategoryCountAggregateInput>;
    @Field(() => CategoryMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CategoryMinAggregateInput>;
    @Field(() => CategoryMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CategoryMaxAggregateInput>;
}

@ObjectType()
export class CategoryGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => CategoryCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CategoryCountAggregate>;
    @Field(() => CategoryMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CategoryMinAggregate>;
    @Field(() => CategoryMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CategoryMaxAggregate>;
}

@InputType()
export class CategoryMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
}

@ObjectType()
export class CategoryMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class CategoryMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@InputType()
export class CategoryMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
}

@ObjectType()
export class CategoryMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class CategoryMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@InputType()
export class CategoryOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
    @Field(() => CategoryCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof CategoryCountOrderByAggregateInput>;
    @Field(() => CategoryMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof CategoryMaxOrderByAggregateInput>;
    @Field(() => CategoryMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof CategoryMinOrderByAggregateInput>;
}

@InputType()
export class CategoryOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
    @Field(() => DishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => DishOrderByRelationAggregateInput)
    dishes?: InstanceType<typeof DishOrderByRelationAggregateInput>;
    @Field(() => GoodOrderByRelationAggregateInput, {nullable:true})
    @Type(() => GoodOrderByRelationAggregateInput)
    goods?: InstanceType<typeof GoodOrderByRelationAggregateInput>;
}

@InputType()
export class CategoryRelationFilter {
    @Field(() => CategoryWhereInput, {nullable:true})
    is?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => CategoryWhereInput, {nullable:true})
    isNot?: InstanceType<typeof CategoryWhereInput>;
}

@InputType()
export class CategoryScalarWhereWithAggregatesInput {
    @Field(() => [CategoryScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<CategoryScalarWhereWithAggregatesInput>;
    @Field(() => [CategoryScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<CategoryScalarWhereWithAggregatesInput>;
    @Field(() => [CategoryScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<CategoryScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    image?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class CategoryUncheckedCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => GoodUncheckedCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => GoodUncheckedCreateNestedManyWithoutCategoryInput)
    goods?: InstanceType<typeof GoodUncheckedCreateNestedManyWithoutCategoryInput>;
}

@InputType()
export class CategoryUncheckedCreateWithoutGoodsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishUncheckedCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutCategoryInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutCategoryInput>;
}

@InputType()
export class CategoryUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishUncheckedCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutCategoryInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutCategoryInput>;
    @Field(() => GoodUncheckedCreateNestedManyWithoutCategoryInput, {nullable:true})
    @Type(() => GoodUncheckedCreateNestedManyWithoutCategoryInput)
    goods?: InstanceType<typeof GoodUncheckedCreateNestedManyWithoutCategoryInput>;
}

@InputType()
export class CategoryUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class CategoryUncheckedUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => GoodUncheckedUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => GoodUncheckedUpdateManyWithoutCategoryNestedInput)
    goods?: InstanceType<typeof GoodUncheckedUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUncheckedUpdateWithoutGoodsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUncheckedUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutCategoryNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUncheckedUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutCategoryNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutCategoryNestedInput>;
    @Field(() => GoodUncheckedUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => GoodUncheckedUpdateManyWithoutCategoryNestedInput)
    goods?: InstanceType<typeof GoodUncheckedUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class CategoryUpdateOneRequiredWithoutDishesNestedInput {
    @Field(() => CategoryCreateWithoutDishesInput, {nullable:true})
    @Type(() => CategoryCreateWithoutDishesInput)
    create?: InstanceType<typeof CategoryCreateWithoutDishesInput>;
    @Field(() => CategoryCreateOrConnectWithoutDishesInput, {nullable:true})
    @Type(() => CategoryCreateOrConnectWithoutDishesInput)
    connectOrCreate?: InstanceType<typeof CategoryCreateOrConnectWithoutDishesInput>;
    @Field(() => CategoryUpsertWithoutDishesInput, {nullable:true})
    @Type(() => CategoryUpsertWithoutDishesInput)
    upsert?: InstanceType<typeof CategoryUpsertWithoutDishesInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    @Type(() => CategoryWhereUniqueInput)
    connect?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => CategoryUpdateWithoutDishesInput, {nullable:true})
    @Type(() => CategoryUpdateWithoutDishesInput)
    update?: InstanceType<typeof CategoryUpdateWithoutDishesInput>;
}

@InputType()
export class CategoryUpdateOneRequiredWithoutGoodsNestedInput {
    @Field(() => CategoryCreateWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryCreateWithoutGoodsInput)
    create?: InstanceType<typeof CategoryCreateWithoutGoodsInput>;
    @Field(() => CategoryCreateOrConnectWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryCreateOrConnectWithoutGoodsInput)
    connectOrCreate?: InstanceType<typeof CategoryCreateOrConnectWithoutGoodsInput>;
    @Field(() => CategoryUpsertWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryUpsertWithoutGoodsInput)
    upsert?: InstanceType<typeof CategoryUpsertWithoutGoodsInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    @Type(() => CategoryWhereUniqueInput)
    connect?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => CategoryUpdateWithoutGoodsInput, {nullable:true})
    @Type(() => CategoryUpdateWithoutGoodsInput)
    update?: InstanceType<typeof CategoryUpdateWithoutGoodsInput>;
}

@InputType()
export class CategoryUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => GoodUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => GoodUpdateManyWithoutCategoryNestedInput)
    goods?: InstanceType<typeof GoodUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUpdateWithoutGoodsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutCategoryNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutCategoryNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutCategoryNestedInput>;
    @Field(() => GoodUpdateManyWithoutCategoryNestedInput, {nullable:true})
    @Type(() => GoodUpdateManyWithoutCategoryNestedInput)
    goods?: InstanceType<typeof GoodUpdateManyWithoutCategoryNestedInput>;
}

@InputType()
export class CategoryUpsertWithoutDishesInput {
    @Field(() => CategoryUpdateWithoutDishesInput, {nullable:false})
    @Type(() => CategoryUpdateWithoutDishesInput)
    update!: InstanceType<typeof CategoryUpdateWithoutDishesInput>;
    @Field(() => CategoryCreateWithoutDishesInput, {nullable:false})
    @Type(() => CategoryCreateWithoutDishesInput)
    create!: InstanceType<typeof CategoryCreateWithoutDishesInput>;
}

@InputType()
export class CategoryUpsertWithoutGoodsInput {
    @Field(() => CategoryUpdateWithoutGoodsInput, {nullable:false})
    @Type(() => CategoryUpdateWithoutGoodsInput)
    update!: InstanceType<typeof CategoryUpdateWithoutGoodsInput>;
    @Field(() => CategoryCreateWithoutGoodsInput, {nullable:false})
    @Type(() => CategoryCreateWithoutGoodsInput)
    create!: InstanceType<typeof CategoryCreateWithoutGoodsInput>;
}

@InputType()
export class CategoryWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
}

@InputType()
export class CategoryWhereInput {
    @Field(() => [CategoryWhereInput], {nullable:true})
    AND?: Array<CategoryWhereInput>;
    @Field(() => [CategoryWhereInput], {nullable:true})
    OR?: Array<CategoryWhereInput>;
    @Field(() => [CategoryWhereInput], {nullable:true})
    NOT?: Array<CategoryWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    image?: InstanceType<typeof StringFilter>;
    @Field(() => DishListRelationFilter, {nullable:true})
    @Type(() => DishListRelationFilter)
    dishes?: InstanceType<typeof DishListRelationFilter>;
    @Field(() => GoodListRelationFilter, {nullable:true})
    @Type(() => GoodListRelationFilter)
    goods?: InstanceType<typeof GoodListRelationFilter>;
}

@ObjectType()
export class Category {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => [Dish], {nullable:true})
    dishes?: Array<Dish>;
    @Field(() => [Good], {nullable:true})
    goods?: Array<Good>;
    @Field(() => CategoryCount, {nullable:false})
    _count?: InstanceType<typeof CategoryCount>;
}

@ArgsType()
export class CreateManyCategoryArgs {
    @Field(() => [CategoryCreateManyInput], {nullable:false})
    @Type(() => CategoryCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<CategoryCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneCategoryArgs {
    @Field(() => CategoryCreateInput, {nullable:false})
    @Type(() => CategoryCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CategoryCreateInput>;
}

@ArgsType()
export class DeleteManyCategoryArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
}

@ArgsType()
export class DeleteOneCategoryArgs {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
}

@ArgsType()
export class FindFirstCategoryArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => [CategoryOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CategoryOrderByWithRelationInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CategoryScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CategoryScalarFieldEnum>;
}

@ArgsType()
export class FindManyCategoryArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => [CategoryOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CategoryOrderByWithRelationInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CategoryScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CategoryScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueCategoryArgs {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyCategoryArgs {
    @Field(() => CategoryUpdateManyMutationInput, {nullable:false})
    @Type(() => CategoryUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CategoryUpdateManyMutationInput>;
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
}

@ArgsType()
export class UpdateOneCategoryArgs {
    @Field(() => CategoryUpdateInput, {nullable:false})
    @Type(() => CategoryUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CategoryUpdateInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneCategoryArgs {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => CategoryCreateInput, {nullable:false})
    @Type(() => CategoryCreateInput)
    create!: InstanceType<typeof CategoryCreateInput>;
    @Field(() => CategoryUpdateInput, {nullable:false})
    @Type(() => CategoryUpdateInput)
    update!: InstanceType<typeof CategoryUpdateInput>;
}

@ObjectType()
export class AggregateCustomerDish {
    @Field(() => CustomerDishCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CustomerDishCountAggregate>;
    @Field(() => CustomerDishAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof CustomerDishAvgAggregate>;
    @Field(() => CustomerDishSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof CustomerDishSumAggregate>;
    @Field(() => CustomerDishMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CustomerDishMinAggregate>;
    @Field(() => CustomerDishMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CustomerDishMaxAggregate>;
}

@ArgsType()
export class CreateManyCustomerDishArgs {
    @Field(() => [CustomerDishCreateManyInput], {nullable:false})
    @Type(() => CustomerDishCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<CustomerDishCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneCustomerDishArgs {
    @Field(() => CustomerDishCreateInput, {nullable:false})
    @Type(() => CustomerDishCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CustomerDishCreateInput>;
}

@ArgsType()
export class CustomerDishAggregateArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => [CustomerDishOrderByWithRelationInput], {nullable:true})
    @Type(() => CustomerDishOrderByWithRelationInput)
    orderBy?: Array<CustomerDishOrderByWithRelationInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    cursor?: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CustomerDishCountAggregateInput, {nullable:true})
    @Type(() => CustomerDishCountAggregateInput)
    _count?: InstanceType<typeof CustomerDishCountAggregateInput>;
    @Field(() => CustomerDishAvgAggregateInput, {nullable:true})
    @Type(() => CustomerDishAvgAggregateInput)
    _avg?: InstanceType<typeof CustomerDishAvgAggregateInput>;
    @Field(() => CustomerDishSumAggregateInput, {nullable:true})
    @Type(() => CustomerDishSumAggregateInput)
    _sum?: InstanceType<typeof CustomerDishSumAggregateInput>;
    @Field(() => CustomerDishMinAggregateInput, {nullable:true})
    @Type(() => CustomerDishMinAggregateInput)
    _min?: InstanceType<typeof CustomerDishMinAggregateInput>;
    @Field(() => CustomerDishMaxAggregateInput, {nullable:true})
    @Type(() => CustomerDishMaxAggregateInput)
    _max?: InstanceType<typeof CustomerDishMaxAggregateInput>;
}

@InputType()
export class CustomerDishAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class CustomerDishAvgAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class CustomerDishAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class CustomerDishCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    dishId?: true;
    @Field(() => Boolean, {nullable:true})
    optionsId?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class CustomerDishCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    dishId!: number;
    @Field(() => Int, {nullable:false})
    optionsId!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class CustomerDishCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    dishId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionsId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@ObjectType()
export class CustomerDishCount {
    @Field(() => Int, {nullable:false})
    selectedToppings?: number;
    @Field(() => Int, {nullable:false})
    orderItems?: number;
}

@InputType()
export class CustomerDishCreateManyParentDishInputEnvelope {
    @Field(() => [CustomerDishCreateManyParentDishInput], {nullable:false})
    @Type(() => CustomerDishCreateManyParentDishInput)
    data!: Array<CustomerDishCreateManyParentDishInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class CustomerDishCreateManyParentDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
}

@InputType()
export class CustomerDishCreateManySelectedOptionInputEnvelope {
    @Field(() => [CustomerDishCreateManySelectedOptionInput], {nullable:false})
    @Type(() => CustomerDishCreateManySelectedOptionInput)
    data!: Array<CustomerDishCreateManySelectedOptionInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class CustomerDishCreateManySelectedOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
}

@InputType()
export class CustomerDishCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
}

@InputType()
export class CustomerDishCreateNestedManyWithoutParentDishInput {
    @Field(() => [CustomerDishCreateWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create?: Array<CustomerDishCreateWithoutParentDishInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutParentDishInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutParentDishInput>;
    @Field(() => CustomerDishCreateManyParentDishInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManyParentDishInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManyParentDishInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishCreateNestedManyWithoutSelectedOptionInput {
    @Field(() => [CustomerDishCreateWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create?: Array<CustomerDishCreateWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedOptionInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedOptionInput>;
    @Field(() => CustomerDishCreateManySelectedOptionInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManySelectedOptionInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManySelectedOptionInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishCreateNestedManyWithoutSelectedToppingsInput {
    @Field(() => [CustomerDishCreateWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create?: Array<CustomerDishCreateWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedToppingsInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishCreateNestedOneWithoutOrderItemsInput {
    @Field(() => CustomerDishCreateWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateWithoutOrderItemsInput)
    create?: InstanceType<typeof CustomerDishCreateWithoutOrderItemsInput>;
    @Field(() => CustomerDishCreateOrConnectWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutOrderItemsInput)
    connectOrCreate?: InstanceType<typeof CustomerDishCreateOrConnectWithoutOrderItemsInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: InstanceType<typeof CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishCreateOrConnectWithoutOrderItemsInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishCreateWithoutOrderItemsInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutOrderItemsInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutOrderItemsInput>;
}

@InputType()
export class CustomerDishCreateOrConnectWithoutParentDishInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishCreateWithoutParentDishInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutParentDishInput>;
}

@InputType()
export class CustomerDishCreateOrConnectWithoutSelectedOptionInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishCreateWithoutSelectedOptionInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutSelectedOptionInput>;
}

@InputType()
export class CustomerDishCreateOrConnectWithoutSelectedToppingsInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishCreateWithoutSelectedToppingsInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutSelectedToppingsInput>;
}

@InputType()
export class CustomerDishCreateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedOneWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateNestedOneWithoutCustomerDishesInput)
    parentDish!: InstanceType<typeof DishCreateNestedOneWithoutCustomerDishesInput>;
    @Field(() => OptionsCreateNestedOneWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutCustomerDishInput)
    selectedOption!: InstanceType<typeof OptionsCreateNestedOneWithoutCustomerDishInput>;
    @Field(() => ToppingCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingCreateNestedManyWithoutCustomerDishesInput>;
}

@InputType()
export class CustomerDishCreateWithoutParentDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OptionsCreateNestedOneWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutCustomerDishInput)
    selectedOption!: InstanceType<typeof OptionsCreateNestedOneWithoutCustomerDishInput>;
    @Field(() => ToppingCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishCreateWithoutSelectedOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedOneWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateNestedOneWithoutCustomerDishesInput)
    parentDish!: InstanceType<typeof DishCreateNestedOneWithoutCustomerDishesInput>;
    @Field(() => ToppingCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishCreateWithoutSelectedToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedOneWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateNestedOneWithoutCustomerDishesInput)
    parentDish!: InstanceType<typeof DishCreateNestedOneWithoutCustomerDishesInput>;
    @Field(() => OptionsCreateNestedOneWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutCustomerDishInput)
    selectedOption!: InstanceType<typeof OptionsCreateNestedOneWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedOneWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateNestedOneWithoutCustomerDishesInput)
    parentDish!: InstanceType<typeof DishCreateNestedOneWithoutCustomerDishesInput>;
    @Field(() => OptionsCreateNestedOneWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutCustomerDishInput)
    selectedOption!: InstanceType<typeof OptionsCreateNestedOneWithoutCustomerDishInput>;
    @Field(() => ToppingCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutCustomerDishInput>;
}

@ArgsType()
export class CustomerDishGroupByArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => [CustomerDishOrderByWithAggregationInput], {nullable:true})
    @Type(() => CustomerDishOrderByWithAggregationInput)
    orderBy?: Array<CustomerDishOrderByWithAggregationInput>;
    @Field(() => [CustomerDishScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof CustomerDishScalarFieldEnum>;
    @Field(() => CustomerDishScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => CustomerDishScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof CustomerDishScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => CustomerDishCountAggregateInput, {nullable:true})
    @Type(() => CustomerDishCountAggregateInput)
    _count?: InstanceType<typeof CustomerDishCountAggregateInput>;
    @Field(() => CustomerDishAvgAggregateInput, {nullable:true})
    @Type(() => CustomerDishAvgAggregateInput)
    _avg?: InstanceType<typeof CustomerDishAvgAggregateInput>;
    @Field(() => CustomerDishSumAggregateInput, {nullable:true})
    @Type(() => CustomerDishSumAggregateInput)
    _sum?: InstanceType<typeof CustomerDishSumAggregateInput>;
    @Field(() => CustomerDishMinAggregateInput, {nullable:true})
    @Type(() => CustomerDishMinAggregateInput)
    _min?: InstanceType<typeof CustomerDishMinAggregateInput>;
    @Field(() => CustomerDishMaxAggregateInput, {nullable:true})
    @Type(() => CustomerDishMaxAggregateInput)
    _max?: InstanceType<typeof CustomerDishMaxAggregateInput>;
}

@ObjectType()
export class CustomerDishGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => CustomerDishCountAggregate, {nullable:true})
    _count?: InstanceType<typeof CustomerDishCountAggregate>;
    @Field(() => CustomerDishAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof CustomerDishAvgAggregate>;
    @Field(() => CustomerDishSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof CustomerDishSumAggregate>;
    @Field(() => CustomerDishMinAggregate, {nullable:true})
    _min?: InstanceType<typeof CustomerDishMinAggregate>;
    @Field(() => CustomerDishMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof CustomerDishMaxAggregate>;
}

@InputType()
export class CustomerDishListRelationFilter {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    every?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    some?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    none?: InstanceType<typeof CustomerDishWhereInput>;
}

@InputType()
export class CustomerDishMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    dishId?: true;
    @Field(() => Boolean, {nullable:true})
    optionsId?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class CustomerDishMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class CustomerDishMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    dishId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionsId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class CustomerDishMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    dishId?: true;
    @Field(() => Boolean, {nullable:true})
    optionsId?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class CustomerDishMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class CustomerDishMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    dishId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionsId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class CustomerDishOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class CustomerDishOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    dishId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionsId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => CustomerDishCountOrderByAggregateInput, {nullable:true})
    @Type(() => CustomerDishCountOrderByAggregateInput)
    _count?: InstanceType<typeof CustomerDishCountOrderByAggregateInput>;
    @Field(() => CustomerDishAvgOrderByAggregateInput, {nullable:true})
    @Type(() => CustomerDishAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof CustomerDishAvgOrderByAggregateInput>;
    @Field(() => CustomerDishMaxOrderByAggregateInput, {nullable:true})
    @Type(() => CustomerDishMaxOrderByAggregateInput)
    _max?: InstanceType<typeof CustomerDishMaxOrderByAggregateInput>;
    @Field(() => CustomerDishMinOrderByAggregateInput, {nullable:true})
    @Type(() => CustomerDishMinOrderByAggregateInput)
    _min?: InstanceType<typeof CustomerDishMinOrderByAggregateInput>;
    @Field(() => CustomerDishSumOrderByAggregateInput, {nullable:true})
    @Type(() => CustomerDishSumOrderByAggregateInput)
    _sum?: InstanceType<typeof CustomerDishSumOrderByAggregateInput>;
}

@InputType()
export class CustomerDishOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    dishId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionsId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => DishOrderByWithRelationInput, {nullable:true})
    @Type(() => DishOrderByWithRelationInput)
    parentDish?: InstanceType<typeof DishOrderByWithRelationInput>;
    @Field(() => OptionsOrderByWithRelationInput, {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    selectedOption?: InstanceType<typeof OptionsOrderByWithRelationInput>;
    @Field(() => ToppingOrderByRelationAggregateInput, {nullable:true})
    @Type(() => ToppingOrderByRelationAggregateInput)
    selectedToppings?: InstanceType<typeof ToppingOrderByRelationAggregateInput>;
    @Field(() => OrderItemOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OrderItemOrderByRelationAggregateInput)
    orderItems?: InstanceType<typeof OrderItemOrderByRelationAggregateInput>;
}

@InputType()
export class CustomerDishRelationFilter {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    is?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    isNot?: InstanceType<typeof CustomerDishWhereInput>;
}

@InputType()
export class CustomerDishScalarWhereWithAggregatesInput {
    @Field(() => [CustomerDishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereWithAggregatesInput)
    AND?: Array<CustomerDishScalarWhereWithAggregatesInput>;
    @Field(() => [CustomerDishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereWithAggregatesInput)
    OR?: Array<CustomerDishScalarWhereWithAggregatesInput>;
    @Field(() => [CustomerDishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereWithAggregatesInput)
    NOT?: Array<CustomerDishScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    dishId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    optionsId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
}

@InputType()
export class CustomerDishScalarWhereInput {
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    AND?: Array<CustomerDishScalarWhereInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    OR?: Array<CustomerDishScalarWhereInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    NOT?: Array<CustomerDishScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    dishId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    optionsId?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
}

@InputType()
export class CustomerDishSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class CustomerDishSumAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class CustomerDishSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class CustomerDishUncheckedCreateNestedManyWithoutParentDishInput {
    @Field(() => [CustomerDishCreateWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create?: Array<CustomerDishCreateWithoutParentDishInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutParentDishInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutParentDishInput>;
    @Field(() => CustomerDishCreateManyParentDishInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManyParentDishInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManyParentDishInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput {
    @Field(() => [CustomerDishCreateWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create?: Array<CustomerDishCreateWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedOptionInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedOptionInput>;
    @Field(() => CustomerDishCreateManySelectedOptionInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManySelectedOptionInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManySelectedOptionInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput {
    @Field(() => [CustomerDishCreateWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create?: Array<CustomerDishCreateWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedToppingsInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
}

@InputType()
export class CustomerDishUncheckedCreateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput>;
}

@InputType()
export class CustomerDishUncheckedCreateWithoutParentDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishUncheckedCreateWithoutSelectedOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishUncheckedCreateWithoutSelectedToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput>;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateManyWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class CustomerDishUncheckedUpdateManyWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput {
    @Field(() => [CustomerDishCreateWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create?: Array<CustomerDishCreateWithoutParentDishInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutParentDishInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutParentDishInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutParentDishInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutParentDishInput>;
    @Field(() => CustomerDishCreateManyParentDishInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManyParentDishInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManyParentDishInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutParentDishInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutParentDishInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutParentDishInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutParentDishInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput {
    @Field(() => [CustomerDishCreateWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create?: Array<CustomerDishCreateWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedOptionInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput>;
    @Field(() => CustomerDishCreateManySelectedOptionInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManySelectedOptionInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManySelectedOptionInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput {
    @Field(() => [CustomerDishCreateWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create?: Array<CustomerDishCreateWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedToppingsInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class CustomerDishUncheckedUpdateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateWithoutParentDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateWithoutSelectedOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateWithoutSelectedToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    dishId?: string;
    @Field(() => String, {nullable:true})
    optionsId?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class CustomerDishUpdateManyWithWhereWithoutParentDishInput {
    @Field(() => CustomerDishScalarWhereInput, {nullable:false})
    @Type(() => CustomerDishScalarWhereInput)
    where!: InstanceType<typeof CustomerDishScalarWhereInput>;
    @Field(() => CustomerDishUpdateManyMutationInput, {nullable:false})
    @Type(() => CustomerDishUpdateManyMutationInput)
    data!: InstanceType<typeof CustomerDishUpdateManyMutationInput>;
}

@InputType()
export class CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput {
    @Field(() => CustomerDishScalarWhereInput, {nullable:false})
    @Type(() => CustomerDishScalarWhereInput)
    where!: InstanceType<typeof CustomerDishScalarWhereInput>;
    @Field(() => CustomerDishUpdateManyMutationInput, {nullable:false})
    @Type(() => CustomerDishUpdateManyMutationInput)
    data!: InstanceType<typeof CustomerDishUpdateManyMutationInput>;
}

@InputType()
export class CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput {
    @Field(() => CustomerDishScalarWhereInput, {nullable:false})
    @Type(() => CustomerDishScalarWhereInput)
    where!: InstanceType<typeof CustomerDishScalarWhereInput>;
    @Field(() => CustomerDishUpdateManyMutationInput, {nullable:false})
    @Type(() => CustomerDishUpdateManyMutationInput)
    data!: InstanceType<typeof CustomerDishUpdateManyMutationInput>;
}

@InputType()
export class CustomerDishUpdateManyWithoutParentDishNestedInput {
    @Field(() => [CustomerDishCreateWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create?: Array<CustomerDishCreateWithoutParentDishInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutParentDishInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutParentDishInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutParentDishInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutParentDishInput>;
    @Field(() => CustomerDishCreateManyParentDishInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManyParentDishInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManyParentDishInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutParentDishInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutParentDishInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutParentDishInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutParentDishInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutParentDishInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUpdateManyWithoutSelectedOptionNestedInput {
    @Field(() => [CustomerDishCreateWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create?: Array<CustomerDishCreateWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedOptionInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput>;
    @Field(() => CustomerDishCreateManySelectedOptionInputEnvelope, {nullable:true})
    @Type(() => CustomerDishCreateManySelectedOptionInputEnvelope)
    createMany?: InstanceType<typeof CustomerDishCreateManySelectedOptionInputEnvelope>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutSelectedOptionInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUpdateManyWithoutSelectedToppingsNestedInput {
    @Field(() => [CustomerDishCreateWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create?: Array<CustomerDishCreateWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishCreateOrConnectWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutSelectedToppingsInput)
    connectOrCreate?: Array<CustomerDishCreateOrConnectWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput)
    upsert?: Array<CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    set?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    disconnect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    delete?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishWhereUniqueInput], {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: Array<CustomerDishWhereUniqueInput>;
    @Field(() => [CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput)
    update?: Array<CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput], {nullable:true})
    @Type(() => CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput)
    updateMany?: Array<CustomerDishUpdateManyWithWhereWithoutSelectedToppingsInput>;
    @Field(() => [CustomerDishScalarWhereInput], {nullable:true})
    @Type(() => CustomerDishScalarWhereInput)
    deleteMany?: Array<CustomerDishScalarWhereInput>;
}

@InputType()
export class CustomerDishUpdateOneWithoutOrderItemsNestedInput {
    @Field(() => CustomerDishCreateWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateWithoutOrderItemsInput)
    create?: InstanceType<typeof CustomerDishCreateWithoutOrderItemsInput>;
    @Field(() => CustomerDishCreateOrConnectWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateOrConnectWithoutOrderItemsInput)
    connectOrCreate?: InstanceType<typeof CustomerDishCreateOrConnectWithoutOrderItemsInput>;
    @Field(() => CustomerDishUpsertWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishUpsertWithoutOrderItemsInput)
    upsert?: InstanceType<typeof CustomerDishUpsertWithoutOrderItemsInput>;
    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;
    @Field(() => Boolean, {nullable:true})
    delete?: boolean;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    connect?: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishUpdateWithoutOrderItemsInput)
    update?: InstanceType<typeof CustomerDishUpdateWithoutOrderItemsInput>;
}

@InputType()
export class CustomerDishUpdateWithWhereUniqueWithoutParentDishInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutParentDishInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutParentDishInput)
    data!: InstanceType<typeof CustomerDishUpdateWithoutParentDishInput>;
}

@InputType()
export class CustomerDishUpdateWithWhereUniqueWithoutSelectedOptionInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutSelectedOptionInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutSelectedOptionInput)
    data!: InstanceType<typeof CustomerDishUpdateWithoutSelectedOptionInput>;
}

@InputType()
export class CustomerDishUpdateWithWhereUniqueWithoutSelectedToppingsInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutSelectedToppingsInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutSelectedToppingsInput)
    data!: InstanceType<typeof CustomerDishUpdateWithoutSelectedToppingsInput>;
}

@InputType()
export class CustomerDishUpdateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput)
    parentDish?: InstanceType<typeof DishUpdateOneRequiredWithoutCustomerDishesNestedInput>;
    @Field(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput)
    selectedOption?: InstanceType<typeof OptionsUpdateOneRequiredWithoutCustomerDishNestedInput>;
    @Field(() => ToppingUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUpdateManyWithoutCustomerDishesNestedInput>;
}

@InputType()
export class CustomerDishUpdateWithoutParentDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput)
    selectedOption?: InstanceType<typeof OptionsUpdateOneRequiredWithoutCustomerDishNestedInput>;
    @Field(() => ToppingUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUpdateWithoutSelectedOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput)
    parentDish?: InstanceType<typeof DishUpdateOneRequiredWithoutCustomerDishesNestedInput>;
    @Field(() => ToppingUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUpdateWithoutSelectedToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput)
    parentDish?: InstanceType<typeof DishUpdateOneRequiredWithoutCustomerDishesNestedInput>;
    @Field(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput)
    selectedOption?: InstanceType<typeof OptionsUpdateOneRequiredWithoutCustomerDishNestedInput>;
    @Field(() => OrderItemUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => DishUpdateOneRequiredWithoutCustomerDishesNestedInput)
    parentDish?: InstanceType<typeof DishUpdateOneRequiredWithoutCustomerDishesNestedInput>;
    @Field(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutCustomerDishNestedInput)
    selectedOption?: InstanceType<typeof OptionsUpdateOneRequiredWithoutCustomerDishNestedInput>;
    @Field(() => ToppingUpdateManyWithoutCustomerDishesNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutCustomerDishesNestedInput)
    selectedToppings?: InstanceType<typeof ToppingUpdateManyWithoutCustomerDishesNestedInput>;
    @Field(() => OrderItemUpdateManyWithoutCustomerDishNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutCustomerDishNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutCustomerDishNestedInput>;
}

@InputType()
export class CustomerDishUpsertWithWhereUniqueWithoutParentDishInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutParentDishInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutParentDishInput)
    update!: InstanceType<typeof CustomerDishUpdateWithoutParentDishInput>;
    @Field(() => CustomerDishCreateWithoutParentDishInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutParentDishInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutParentDishInput>;
}

@InputType()
export class CustomerDishUpsertWithWhereUniqueWithoutSelectedOptionInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutSelectedOptionInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutSelectedOptionInput)
    update!: InstanceType<typeof CustomerDishUpdateWithoutSelectedOptionInput>;
    @Field(() => CustomerDishCreateWithoutSelectedOptionInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutSelectedOptionInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutSelectedOptionInput>;
}

@InputType()
export class CustomerDishUpsertWithWhereUniqueWithoutSelectedToppingsInput {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishUpdateWithoutSelectedToppingsInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutSelectedToppingsInput)
    update!: InstanceType<typeof CustomerDishUpdateWithoutSelectedToppingsInput>;
    @Field(() => CustomerDishCreateWithoutSelectedToppingsInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutSelectedToppingsInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutSelectedToppingsInput>;
}

@InputType()
export class CustomerDishUpsertWithoutOrderItemsInput {
    @Field(() => CustomerDishUpdateWithoutOrderItemsInput, {nullable:false})
    @Type(() => CustomerDishUpdateWithoutOrderItemsInput)
    update!: InstanceType<typeof CustomerDishUpdateWithoutOrderItemsInput>;
    @Field(() => CustomerDishCreateWithoutOrderItemsInput, {nullable:false})
    @Type(() => CustomerDishCreateWithoutOrderItemsInput)
    create!: InstanceType<typeof CustomerDishCreateWithoutOrderItemsInput>;
}

@InputType()
export class CustomerDishWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class CustomerDishWhereInput {
    @Field(() => [CustomerDishWhereInput], {nullable:true})
    @Type(() => CustomerDishWhereInput)
    AND?: Array<CustomerDishWhereInput>;
    @Field(() => [CustomerDishWhereInput], {nullable:true})
    @Type(() => CustomerDishWhereInput)
    OR?: Array<CustomerDishWhereInput>;
    @Field(() => [CustomerDishWhereInput], {nullable:true})
    @Type(() => CustomerDishWhereInput)
    NOT?: Array<CustomerDishWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    dishId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    optionsId?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => DishRelationFilter, {nullable:true})
    @Type(() => DishRelationFilter)
    parentDish?: InstanceType<typeof DishRelationFilter>;
    @Field(() => OptionsRelationFilter, {nullable:true})
    @Type(() => OptionsRelationFilter)
    selectedOption?: InstanceType<typeof OptionsRelationFilter>;
    @Field(() => ToppingListRelationFilter, {nullable:true})
    @Type(() => ToppingListRelationFilter)
    selectedToppings?: InstanceType<typeof ToppingListRelationFilter>;
    @Field(() => OrderItemListRelationFilter, {nullable:true})
    @Type(() => OrderItemListRelationFilter)
    orderItems?: InstanceType<typeof OrderItemListRelationFilter>;
}

@ObjectType()
export class CustomerDish {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    dishId!: string;
    @Field(() => String, {nullable:false})
    optionsId!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => Dish, {nullable:false})
    parentDish?: InstanceType<typeof Dish>;
    @Field(() => Options, {nullable:false})
    selectedOption?: InstanceType<typeof Options>;
    @Field(() => [Topping], {nullable:true})
    selectedToppings?: Array<Topping>;
    @Field(() => [OrderItem], {nullable:true})
    orderItems?: Array<OrderItem>;
    @Field(() => CustomerDishCount, {nullable:false})
    _count?: InstanceType<typeof CustomerDishCount>;
}

@ArgsType()
export class DeleteManyCustomerDishArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
}

@ArgsType()
export class DeleteOneCustomerDishArgs {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
}

@ArgsType()
export class FindFirstCustomerDishArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => [CustomerDishOrderByWithRelationInput], {nullable:true})
    @Type(() => CustomerDishOrderByWithRelationInput)
    orderBy?: Array<CustomerDishOrderByWithRelationInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    cursor?: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CustomerDishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CustomerDishScalarFieldEnum>;
}

@ArgsType()
export class FindManyCustomerDishArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => [CustomerDishOrderByWithRelationInput], {nullable:true})
    @Type(() => CustomerDishOrderByWithRelationInput)
    orderBy?: Array<CustomerDishOrderByWithRelationInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    cursor?: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CustomerDishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CustomerDishScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueCustomerDishArgs {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyCustomerDishArgs {
    @Field(() => CustomerDishUpdateManyMutationInput, {nullable:false})
    @Type(() => CustomerDishUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CustomerDishUpdateManyMutationInput>;
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
}

@ArgsType()
export class UpdateOneCustomerDishArgs {
    @Field(() => CustomerDishUpdateInput, {nullable:false})
    @Type(() => CustomerDishUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof CustomerDishUpdateInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneCustomerDishArgs {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => CustomerDishCreateInput, {nullable:false})
    @Type(() => CustomerDishCreateInput)
    create!: InstanceType<typeof CustomerDishCreateInput>;
    @Field(() => CustomerDishUpdateInput, {nullable:false})
    @Type(() => CustomerDishUpdateInput)
    update!: InstanceType<typeof CustomerDishUpdateInput>;
}

@ObjectType()
export class AggregateDish {
    @Field(() => DishCountAggregate, {nullable:true})
    _count?: InstanceType<typeof DishCountAggregate>;
    @Field(() => DishAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof DishAvgAggregate>;
    @Field(() => DishSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof DishSumAggregate>;
    @Field(() => DishMinAggregate, {nullable:true})
    _min?: InstanceType<typeof DishMinAggregate>;
    @Field(() => DishMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof DishMaxAggregate>;
}

@ArgsType()
export class CreateManyDishArgs {
    @Field(() => [DishCreateManyInput], {nullable:false})
    @Type(() => DishCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<DishCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneDishArgs {
    @Field(() => DishCreateInput, {nullable:false})
    @Type(() => DishCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof DishCreateInput>;
}

@ArgsType()
export class DeleteManyDishArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
}

@ArgsType()
export class DeleteOneDishArgs {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof DishWhereUniqueInput>;
}

@ArgsType()
export class DishAggregateArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
    @Field(() => [DishOrderByWithRelationInput], {nullable:true})
    @Type(() => DishOrderByWithRelationInput)
    orderBy?: Array<DishOrderByWithRelationInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    cursor?: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => DishCountAggregateInput, {nullable:true})
    @Type(() => DishCountAggregateInput)
    _count?: InstanceType<typeof DishCountAggregateInput>;
    @Field(() => DishAvgAggregateInput, {nullable:true})
    @Type(() => DishAvgAggregateInput)
    _avg?: InstanceType<typeof DishAvgAggregateInput>;
    @Field(() => DishSumAggregateInput, {nullable:true})
    @Type(() => DishSumAggregateInput)
    _sum?: InstanceType<typeof DishSumAggregateInput>;
    @Field(() => DishMinAggregateInput, {nullable:true})
    @Type(() => DishMinAggregateInput)
    _min?: InstanceType<typeof DishMinAggregateInput>;
    @Field(() => DishMaxAggregateInput, {nullable:true})
    @Type(() => DishMaxAggregateInput)
    _max?: InstanceType<typeof DishMaxAggregateInput>;
}

@InputType()
export class DishAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class DishAvgAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class DishAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class DishCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    images?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class DishCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    description!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    images!: number;
    @Field(() => Int, {nullable:false})
    slug!: number;
    @Field(() => Int, {nullable:false})
    categoryId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class DishCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@ObjectType()
export class DishCount {
    @Field(() => Int, {nullable:false})
    options?: number;
    @Field(() => Int, {nullable:false})
    ingradients?: number;
    @Field(() => Int, {nullable:false})
    customerDishes?: number;
}

@InputType()
export class DishCreateManyCategoryInputEnvelope {
    @Field(() => [DishCreateManyCategoryInput], {nullable:false})
    @Type(() => DishCreateManyCategoryInput)
    data!: Array<DishCreateManyCategoryInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class DishCreateManyCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
}

@InputType()
export class DishCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
}

@InputType()
export class DishCreateNestedManyWithoutCategoryInput {
    @Field(() => [DishCreateWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateWithoutCategoryInput)
    create?: Array<DishCreateWithoutCategoryInput>;
    @Field(() => [DishCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutCategoryInput>;
    @Field(() => DishCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => DishCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof DishCreateManyCategoryInputEnvelope>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishCreateNestedManyWithoutIngradientsInput {
    @Field(() => [DishCreateWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateWithoutIngradientsInput)
    create?: Array<DishCreateWithoutIngradientsInput>;
    @Field(() => [DishCreateOrConnectWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutIngradientsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutIngradientsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishCreateNestedManyWithoutOptionsInput {
    @Field(() => [DishCreateWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateWithoutOptionsInput)
    create?: Array<DishCreateWithoutOptionsInput>;
    @Field(() => [DishCreateOrConnectWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutOptionsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutOptionsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishCreateNestedOneWithoutCustomerDishesInput {
    @Field(() => DishCreateWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishCreateWithoutCustomerDishesInput)
    create?: InstanceType<typeof DishCreateWithoutCustomerDishesInput>;
    @Field(() => DishCreateOrConnectWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: InstanceType<typeof DishCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: InstanceType<typeof DishWhereUniqueInput>;
}

@InputType()
export class DishCreateOrConnectWithoutCategoryInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishCreateWithoutCategoryInput, {nullable:false})
    @Type(() => DishCreateWithoutCategoryInput)
    create!: InstanceType<typeof DishCreateWithoutCategoryInput>;
}

@InputType()
export class DishCreateOrConnectWithoutCustomerDishesInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishCreateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateWithoutCustomerDishesInput)
    create!: InstanceType<typeof DishCreateWithoutCustomerDishesInput>;
}

@InputType()
export class DishCreateOrConnectWithoutIngradientsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishCreateWithoutIngradientsInput, {nullable:false})
    @Type(() => DishCreateWithoutIngradientsInput)
    create!: InstanceType<typeof DishCreateWithoutIngradientsInput>;
}

@InputType()
export class DishCreateOrConnectWithoutOptionsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishCreateWithoutOptionsInput, {nullable:false})
    @Type(() => DishCreateWithoutOptionsInput)
    create!: InstanceType<typeof DishCreateWithoutOptionsInput>;
}

@InputType()
export class DishCreateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OptionsCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsCreateNestedManyWithoutDishesInput>;
    @Field(() => IngradientLabelCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishCreateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OptionsCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsCreateNestedManyWithoutDishesInput>;
    @Field(() => CategoryCreateNestedOneWithoutDishesInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutDishesInput>;
    @Field(() => IngradientLabelCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelCreateNestedManyWithoutDishesInput>;
}

@InputType()
export class DishCreateWithoutIngradientsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OptionsCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsCreateNestedManyWithoutDishesInput>;
    @Field(() => CategoryCreateNestedOneWithoutDishesInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutDishesInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishCreateWithoutOptionsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => CategoryCreateNestedOneWithoutDishesInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutDishesInput>;
    @Field(() => IngradientLabelCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OptionsCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsCreateNestedManyWithoutDishesInput>;
    @Field(() => CategoryCreateNestedOneWithoutDishesInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutDishesInput>;
    @Field(() => IngradientLabelCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishCreateimagesInput {
    @Field(() => [String], {nullable:false})
    set!: Array<string>;
}

@ArgsType()
export class DishGroupByArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
    @Field(() => [DishOrderByWithAggregationInput], {nullable:true})
    @Type(() => DishOrderByWithAggregationInput)
    orderBy?: Array<DishOrderByWithAggregationInput>;
    @Field(() => [DishScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof DishScalarFieldEnum>;
    @Field(() => DishScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => DishScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof DishScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => DishCountAggregateInput, {nullable:true})
    @Type(() => DishCountAggregateInput)
    _count?: InstanceType<typeof DishCountAggregateInput>;
    @Field(() => DishAvgAggregateInput, {nullable:true})
    @Type(() => DishAvgAggregateInput)
    _avg?: InstanceType<typeof DishAvgAggregateInput>;
    @Field(() => DishSumAggregateInput, {nullable:true})
    @Type(() => DishSumAggregateInput)
    _sum?: InstanceType<typeof DishSumAggregateInput>;
    @Field(() => DishMinAggregateInput, {nullable:true})
    @Type(() => DishMinAggregateInput)
    _min?: InstanceType<typeof DishMinAggregateInput>;
    @Field(() => DishMaxAggregateInput, {nullable:true})
    @Type(() => DishMaxAggregateInput)
    _max?: InstanceType<typeof DishMaxAggregateInput>;
}

@ObjectType()
export class DishGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => DishCountAggregate, {nullable:true})
    _count?: InstanceType<typeof DishCountAggregate>;
    @Field(() => DishAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof DishAvgAggregate>;
    @Field(() => DishSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof DishSumAggregate>;
    @Field(() => DishMinAggregate, {nullable:true})
    _min?: InstanceType<typeof DishMinAggregate>;
    @Field(() => DishMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof DishMaxAggregate>;
}

@InputType()
export class DishListRelationFilter {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    every?: InstanceType<typeof DishWhereInput>;
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    some?: InstanceType<typeof DishWhereInput>;
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    none?: InstanceType<typeof DishWhereInput>;
}

@InputType()
export class DishMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
}

@ObjectType()
export class DishMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class DishMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@InputType()
export class DishMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
}

@ObjectType()
export class DishMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class DishMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@InputType()
export class DishOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class DishOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
    @Field(() => DishCountOrderByAggregateInput, {nullable:true})
    @Type(() => DishCountOrderByAggregateInput)
    _count?: InstanceType<typeof DishCountOrderByAggregateInput>;
    @Field(() => DishAvgOrderByAggregateInput, {nullable:true})
    @Type(() => DishAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof DishAvgOrderByAggregateInput>;
    @Field(() => DishMaxOrderByAggregateInput, {nullable:true})
    @Type(() => DishMaxOrderByAggregateInput)
    _max?: InstanceType<typeof DishMaxOrderByAggregateInput>;
    @Field(() => DishMinOrderByAggregateInput, {nullable:true})
    @Type(() => DishMinOrderByAggregateInput)
    _min?: InstanceType<typeof DishMinOrderByAggregateInput>;
    @Field(() => DishSumOrderByAggregateInput, {nullable:true})
    @Type(() => DishSumOrderByAggregateInput)
    _sum?: InstanceType<typeof DishSumOrderByAggregateInput>;
}

@InputType()
export class DishOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
    @Field(() => OptionsOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OptionsOrderByRelationAggregateInput)
    options?: InstanceType<typeof OptionsOrderByRelationAggregateInput>;
    @Field(() => CategoryOrderByWithRelationInput, {nullable:true})
    category?: InstanceType<typeof CategoryOrderByWithRelationInput>;
    @Field(() => IngradientLabelOrderByRelationAggregateInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelOrderByRelationAggregateInput>;
    @Field(() => CustomerDishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => CustomerDishOrderByRelationAggregateInput)
    customerDishes?: InstanceType<typeof CustomerDishOrderByRelationAggregateInput>;
}

@InputType()
export class DishRelationFilter {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    is?: InstanceType<typeof DishWhereInput>;
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    isNot?: InstanceType<typeof DishWhereInput>;
}

@InputType()
export class DishScalarWhereWithAggregatesInput {
    @Field(() => [DishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => DishScalarWhereWithAggregatesInput)
    AND?: Array<DishScalarWhereWithAggregatesInput>;
    @Field(() => [DishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => DishScalarWhereWithAggregatesInput)
    OR?: Array<DishScalarWhereWithAggregatesInput>;
    @Field(() => [DishScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => DishScalarWhereWithAggregatesInput)
    NOT?: Array<DishScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    description?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    slug?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class DishScalarWhereInput {
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    AND?: Array<DishScalarWhereInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    OR?: Array<DishScalarWhereInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    NOT?: Array<DishScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringFilter, {nullable:true})
    slug?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class DishSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class DishSumAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class DishSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class DishUncheckedCreateNestedManyWithoutCategoryInput {
    @Field(() => [DishCreateWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateWithoutCategoryInput)
    create?: Array<DishCreateWithoutCategoryInput>;
    @Field(() => [DishCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutCategoryInput>;
    @Field(() => DishCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => DishCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof DishCreateManyCategoryInputEnvelope>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishUncheckedCreateNestedManyWithoutIngradientsInput {
    @Field(() => [DishCreateWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateWithoutIngradientsInput)
    create?: Array<DishCreateWithoutIngradientsInput>;
    @Field(() => [DishCreateOrConnectWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutIngradientsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutIngradientsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishUncheckedCreateNestedManyWithoutOptionsInput {
    @Field(() => [DishCreateWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateWithoutOptionsInput)
    create?: Array<DishCreateWithoutOptionsInput>;
    @Field(() => [DishCreateOrConnectWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutOptionsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutOptionsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
}

@InputType()
export class DishUncheckedCreateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OptionsUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsUncheckedCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => IngradientLabelUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishUncheckedCreateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => OptionsUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsUncheckedCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => IngradientLabelUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedCreateNestedManyWithoutDishesInput>;
}

@InputType()
export class DishUncheckedCreateWithoutIngradientsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => OptionsUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsUncheckedCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishUncheckedCreateWithoutOptionsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => IngradientLabelUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => OptionsUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    @Type(() => OptionsUncheckedCreateNestedManyWithoutDishesInput)
    options?: InstanceType<typeof OptionsUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => IngradientLabelUncheckedCreateNestedManyWithoutDishesInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedCreateNestedManyWithoutDishesInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutParentDishInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutParentDishInput>;
}

@InputType()
export class DishUncheckedUpdateManyWithoutCategoryNestedInput {
    @Field(() => [DishCreateWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateWithoutCategoryInput)
    create?: Array<DishCreateWithoutCategoryInput>;
    @Field(() => [DishCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutCategoryInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutCategoryInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutCategoryInput>;
    @Field(() => DishCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => DishCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof DishCreateManyCategoryInputEnvelope>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutCategoryInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutCategoryInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutCategoryInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutCategoryInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUncheckedUpdateManyWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class DishUncheckedUpdateManyWithoutIngradientsNestedInput {
    @Field(() => [DishCreateWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateWithoutIngradientsInput)
    create?: Array<DishCreateWithoutIngradientsInput>;
    @Field(() => [DishCreateOrConnectWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutIngradientsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutIngradientsInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutIngradientsInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutIngradientsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutIngradientsInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutIngradientsInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutIngradientsInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutIngradientsInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUncheckedUpdateManyWithoutOptionsNestedInput {
    @Field(() => [DishCreateWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateWithoutOptionsInput)
    create?: Array<DishCreateWithoutOptionsInput>;
    @Field(() => [DishCreateOrConnectWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutOptionsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutOptionsInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutOptionsInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutOptionsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutOptionsInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutOptionsInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutOptionsInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutOptionsInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class DishUncheckedUpdateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUncheckedUpdateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
    @Field(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput>;
}

@InputType()
export class DishUncheckedUpdateWithoutIngradientsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
    @Field(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUncheckedUpdateWithoutOptionsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
    @Field(() => IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
    @Field(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUncheckedUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
}

@InputType()
export class DishUpdateManyWithWhereWithoutCategoryInput {
    @Field(() => DishScalarWhereInput, {nullable:false})
    @Type(() => DishScalarWhereInput)
    where!: InstanceType<typeof DishScalarWhereInput>;
    @Field(() => DishUpdateManyMutationInput, {nullable:false})
    @Type(() => DishUpdateManyMutationInput)
    data!: InstanceType<typeof DishUpdateManyMutationInput>;
}

@InputType()
export class DishUpdateManyWithWhereWithoutIngradientsInput {
    @Field(() => DishScalarWhereInput, {nullable:false})
    @Type(() => DishScalarWhereInput)
    where!: InstanceType<typeof DishScalarWhereInput>;
    @Field(() => DishUpdateManyMutationInput, {nullable:false})
    @Type(() => DishUpdateManyMutationInput)
    data!: InstanceType<typeof DishUpdateManyMutationInput>;
}

@InputType()
export class DishUpdateManyWithWhereWithoutOptionsInput {
    @Field(() => DishScalarWhereInput, {nullable:false})
    @Type(() => DishScalarWhereInput)
    where!: InstanceType<typeof DishScalarWhereInput>;
    @Field(() => DishUpdateManyMutationInput, {nullable:false})
    @Type(() => DishUpdateManyMutationInput)
    data!: InstanceType<typeof DishUpdateManyMutationInput>;
}

@InputType()
export class DishUpdateManyWithoutCategoryNestedInput {
    @Field(() => [DishCreateWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateWithoutCategoryInput)
    create?: Array<DishCreateWithoutCategoryInput>;
    @Field(() => [DishCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutCategoryInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutCategoryInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutCategoryInput>;
    @Field(() => DishCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => DishCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof DishCreateManyCategoryInputEnvelope>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutCategoryInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutCategoryInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutCategoryInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutCategoryInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutCategoryInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUpdateManyWithoutIngradientsNestedInput {
    @Field(() => [DishCreateWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateWithoutIngradientsInput)
    create?: Array<DishCreateWithoutIngradientsInput>;
    @Field(() => [DishCreateOrConnectWithoutIngradientsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutIngradientsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutIngradientsInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutIngradientsInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutIngradientsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutIngradientsInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutIngradientsInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutIngradientsInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutIngradientsInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutIngradientsInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUpdateManyWithoutOptionsNestedInput {
    @Field(() => [DishCreateWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateWithoutOptionsInput)
    create?: Array<DishCreateWithoutOptionsInput>;
    @Field(() => [DishCreateOrConnectWithoutOptionsInput], {nullable:true})
    @Type(() => DishCreateOrConnectWithoutOptionsInput)
    connectOrCreate?: Array<DishCreateOrConnectWithoutOptionsInput>;
    @Field(() => [DishUpsertWithWhereUniqueWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpsertWithWhereUniqueWithoutOptionsInput)
    upsert?: Array<DishUpsertWithWhereUniqueWithoutOptionsInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    set?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    disconnect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    delete?: Array<DishWhereUniqueInput>;
    @Field(() => [DishWhereUniqueInput], {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: Array<DishWhereUniqueInput>;
    @Field(() => [DishUpdateWithWhereUniqueWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpdateWithWhereUniqueWithoutOptionsInput)
    update?: Array<DishUpdateWithWhereUniqueWithoutOptionsInput>;
    @Field(() => [DishUpdateManyWithWhereWithoutOptionsInput], {nullable:true})
    @Type(() => DishUpdateManyWithWhereWithoutOptionsInput)
    updateMany?: Array<DishUpdateManyWithWhereWithoutOptionsInput>;
    @Field(() => [DishScalarWhereInput], {nullable:true})
    @Type(() => DishScalarWhereInput)
    deleteMany?: Array<DishScalarWhereInput>;
}

@InputType()
export class DishUpdateOneRequiredWithoutCustomerDishesNestedInput {
    @Field(() => DishCreateWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishCreateWithoutCustomerDishesInput)
    create?: InstanceType<typeof DishCreateWithoutCustomerDishesInput>;
    @Field(() => DishCreateOrConnectWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: InstanceType<typeof DishCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => DishUpsertWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishUpsertWithoutCustomerDishesInput)
    upsert?: InstanceType<typeof DishUpsertWithoutCustomerDishesInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    connect?: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutCustomerDishesInput, {nullable:true})
    @Type(() => DishUpdateWithoutCustomerDishesInput)
    update?: InstanceType<typeof DishUpdateWithoutCustomerDishesInput>;
}

@InputType()
export class DishUpdateWithWhereUniqueWithoutCategoryInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutCategoryInput, {nullable:false})
    @Type(() => DishUpdateWithoutCategoryInput)
    data!: InstanceType<typeof DishUpdateWithoutCategoryInput>;
}

@InputType()
export class DishUpdateWithWhereUniqueWithoutIngradientsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutIngradientsInput, {nullable:false})
    @Type(() => DishUpdateWithoutIngradientsInput)
    data!: InstanceType<typeof DishUpdateWithoutIngradientsInput>;
}

@InputType()
export class DishUpdateWithWhereUniqueWithoutOptionsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutOptionsInput, {nullable:false})
    @Type(() => DishUpdateWithoutOptionsInput)
    data!: InstanceType<typeof DishUpdateWithoutOptionsInput>;
}

@InputType()
export class DishUpdateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OptionsUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUpdateManyWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUpdateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OptionsUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUpdateManyWithoutDishesNestedInput>;
    @Field(() => CategoryUpdateOneRequiredWithoutDishesNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUpdateManyWithoutDishesNestedInput>;
}

@InputType()
export class DishUpdateWithoutIngradientsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OptionsUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUpdateManyWithoutDishesNestedInput>;
    @Field(() => CategoryUpdateOneRequiredWithoutDishesNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutDishesNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUpdateWithoutOptionsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => CategoryUpdateOneRequiredWithoutDishesNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OptionsUpdateManyWithoutDishesNestedInput, {nullable:true})
    @Type(() => OptionsUpdateManyWithoutDishesNestedInput)
    options?: InstanceType<typeof OptionsUpdateManyWithoutDishesNestedInput>;
    @Field(() => CategoryUpdateOneRequiredWithoutDishesNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutDishesNestedInput>;
    @Field(() => IngradientLabelUpdateManyWithoutDishesNestedInput, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelUpdateManyWithoutDishesNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutParentDishNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutParentDishNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutParentDishNestedInput>;
}

@InputType()
export class DishUpdateimagesInput {
    @Field(() => [String], {nullable:true})
    set?: Array<string>;
    @Field(() => [String], {nullable:true})
    push?: Array<string>;
}

@InputType()
export class DishUpsertWithWhereUniqueWithoutCategoryInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutCategoryInput, {nullable:false})
    @Type(() => DishUpdateWithoutCategoryInput)
    update!: InstanceType<typeof DishUpdateWithoutCategoryInput>;
    @Field(() => DishCreateWithoutCategoryInput, {nullable:false})
    @Type(() => DishCreateWithoutCategoryInput)
    create!: InstanceType<typeof DishCreateWithoutCategoryInput>;
}

@InputType()
export class DishUpsertWithWhereUniqueWithoutIngradientsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutIngradientsInput, {nullable:false})
    @Type(() => DishUpdateWithoutIngradientsInput)
    update!: InstanceType<typeof DishUpdateWithoutIngradientsInput>;
    @Field(() => DishCreateWithoutIngradientsInput, {nullable:false})
    @Type(() => DishCreateWithoutIngradientsInput)
    create!: InstanceType<typeof DishCreateWithoutIngradientsInput>;
}

@InputType()
export class DishUpsertWithWhereUniqueWithoutOptionsInput {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishUpdateWithoutOptionsInput, {nullable:false})
    @Type(() => DishUpdateWithoutOptionsInput)
    update!: InstanceType<typeof DishUpdateWithoutOptionsInput>;
    @Field(() => DishCreateWithoutOptionsInput, {nullable:false})
    @Type(() => DishCreateWithoutOptionsInput)
    create!: InstanceType<typeof DishCreateWithoutOptionsInput>;
}

@InputType()
export class DishUpsertWithoutCustomerDishesInput {
    @Field(() => DishUpdateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishUpdateWithoutCustomerDishesInput)
    update!: InstanceType<typeof DishUpdateWithoutCustomerDishesInput>;
    @Field(() => DishCreateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => DishCreateWithoutCustomerDishesInput)
    create!: InstanceType<typeof DishCreateWithoutCustomerDishesInput>;
}

@InputType()
export class DishWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
}

@InputType()
export class DishWhereInput {
    @Field(() => [DishWhereInput], {nullable:true})
    @Type(() => DishWhereInput)
    AND?: Array<DishWhereInput>;
    @Field(() => [DishWhereInput], {nullable:true})
    @Type(() => DishWhereInput)
    OR?: Array<DishWhereInput>;
    @Field(() => [DishWhereInput], {nullable:true})
    @Type(() => DishWhereInput)
    NOT?: Array<DishWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringFilter, {nullable:true})
    slug?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringFilter>;
    @Field(() => OptionsListRelationFilter, {nullable:true})
    @Type(() => OptionsListRelationFilter)
    options?: InstanceType<typeof OptionsListRelationFilter>;
    @Field(() => CategoryRelationFilter, {nullable:true})
    category?: InstanceType<typeof CategoryRelationFilter>;
    @Field(() => IngradientLabelListRelationFilter, {nullable:true})
    ingradients?: InstanceType<typeof IngradientLabelListRelationFilter>;
    @Field(() => CustomerDishListRelationFilter, {nullable:true})
    @Type(() => CustomerDishListRelationFilter)
    customerDishes?: InstanceType<typeof CustomerDishListRelationFilter>;
}

@ObjectType()
export class Dish {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false,defaultValue:''})
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    /** @Validator .@ArrayMaxSize(5) */
    @Field(() => [String], {nullable:true,description:'@Validator.@ArrayMaxSize(5)'})
    images!: Array<string>;
    @Field(() => String, {nullable:false})
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => [Options], {nullable:true})
    options?: Array<Options>;
    @Field(() => Category, {nullable:false})
    category?: InstanceType<typeof Category>;
    @Field(() => [IngradientLabel], {nullable:true})
    ingradients?: Array<IngradientLabel>;
    @Field(() => [CustomerDish], {nullable:true})
    customerDishes?: Array<CustomerDish>;
    @Field(() => DishCount, {nullable:false})
    _count?: InstanceType<typeof DishCount>;
}

@ArgsType()
export class FindFirstDishArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
    @Field(() => [DishOrderByWithRelationInput], {nullable:true})
    @Type(() => DishOrderByWithRelationInput)
    orderBy?: Array<DishOrderByWithRelationInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    cursor?: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DishScalarFieldEnum>;
}

@ArgsType()
export class FindManyDishArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
    @Field(() => [DishOrderByWithRelationInput], {nullable:true})
    @Type(() => DishOrderByWithRelationInput)
    orderBy?: Array<DishOrderByWithRelationInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    cursor?: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DishScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueDishArgs {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof DishWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyDishArgs {
    @Field(() => DishUpdateManyMutationInput, {nullable:false})
    @Type(() => DishUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof DishUpdateManyMutationInput>;
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
}

@ArgsType()
export class UpdateOneDishArgs {
    @Field(() => DishUpdateInput, {nullable:false})
    @Type(() => DishUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof DishUpdateInput>;
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof DishWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneDishArgs {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => DishCreateInput, {nullable:false})
    @Type(() => DishCreateInput)
    create!: InstanceType<typeof DishCreateInput>;
    @Field(() => DishUpdateInput, {nullable:false})
    @Type(() => DishUpdateInput)
    update!: InstanceType<typeof DishUpdateInput>;
}

@ObjectType()
export class AggregateGood {
    @Field(() => GoodCountAggregate, {nullable:true})
    _count?: InstanceType<typeof GoodCountAggregate>;
    @Field(() => GoodAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof GoodAvgAggregate>;
    @Field(() => GoodSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof GoodSumAggregate>;
    @Field(() => GoodMinAggregate, {nullable:true})
    _min?: InstanceType<typeof GoodMinAggregate>;
    @Field(() => GoodMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof GoodMaxAggregate>;
}

@ArgsType()
export class CreateManyGoodArgs {
    @Field(() => [GoodCreateManyInput], {nullable:false})
    @Type(() => GoodCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<GoodCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneGoodArgs {
    @Field(() => GoodCreateInput, {nullable:false})
    @Type(() => GoodCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof GoodCreateInput>;
}

@ArgsType()
export class DeleteManyGoodArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
}

@ArgsType()
export class DeleteOneGoodArgs {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof GoodWhereUniqueInput>;
}

@ArgsType()
export class FindFirstGoodArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
    @Field(() => [GoodOrderByWithRelationInput], {nullable:true})
    @Type(() => GoodOrderByWithRelationInput)
    orderBy?: Array<GoodOrderByWithRelationInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    cursor?: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [GoodScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof GoodScalarFieldEnum>;
}

@ArgsType()
export class FindManyGoodArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
    @Field(() => [GoodOrderByWithRelationInput], {nullable:true})
    @Type(() => GoodOrderByWithRelationInput)
    orderBy?: Array<GoodOrderByWithRelationInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    cursor?: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [GoodScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof GoodScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueGoodArgs {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof GoodWhereUniqueInput>;
}

@ArgsType()
export class GoodAggregateArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
    @Field(() => [GoodOrderByWithRelationInput], {nullable:true})
    @Type(() => GoodOrderByWithRelationInput)
    orderBy?: Array<GoodOrderByWithRelationInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    cursor?: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => GoodCountAggregateInput, {nullable:true})
    @Type(() => GoodCountAggregateInput)
    _count?: InstanceType<typeof GoodCountAggregateInput>;
    @Field(() => GoodAvgAggregateInput, {nullable:true})
    @Type(() => GoodAvgAggregateInput)
    _avg?: InstanceType<typeof GoodAvgAggregateInput>;
    @Field(() => GoodSumAggregateInput, {nullable:true})
    @Type(() => GoodSumAggregateInput)
    _sum?: InstanceType<typeof GoodSumAggregateInput>;
    @Field(() => GoodMinAggregateInput, {nullable:true})
    @Type(() => GoodMinAggregateInput)
    _min?: InstanceType<typeof GoodMinAggregateInput>;
    @Field(() => GoodMaxAggregateInput, {nullable:true})
    @Type(() => GoodMaxAggregateInput)
    _max?: InstanceType<typeof GoodMaxAggregateInput>;
}

@InputType()
export class GoodAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class GoodAvgAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class GoodAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class GoodCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    images?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class GoodCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    description!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    images!: number;
    @Field(() => Int, {nullable:false})
    slug!: number;
    @Field(() => Int, {nullable:false})
    categoryId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class GoodCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@ObjectType()
export class GoodCount {
    @Field(() => Int, {nullable:false})
    orderItems?: number;
}

@InputType()
export class GoodCreateManyCategoryInputEnvelope {
    @Field(() => [GoodCreateManyCategoryInput], {nullable:false})
    @Type(() => GoodCreateManyCategoryInput)
    data!: Array<GoodCreateManyCategoryInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class GoodCreateManyCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
}

@InputType()
export class GoodCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
}

@InputType()
export class GoodCreateNestedManyWithoutCategoryInput {
    @Field(() => [GoodCreateWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateWithoutCategoryInput)
    create?: Array<GoodCreateWithoutCategoryInput>;
    @Field(() => [GoodCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<GoodCreateOrConnectWithoutCategoryInput>;
    @Field(() => GoodCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => GoodCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof GoodCreateManyCategoryInputEnvelope>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: Array<GoodWhereUniqueInput>;
}

@InputType()
export class GoodCreateNestedOneWithoutOrderItemsInput {
    @Field(() => GoodCreateWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateWithoutOrderItemsInput)
    create?: InstanceType<typeof GoodCreateWithoutOrderItemsInput>;
    @Field(() => GoodCreateOrConnectWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutOrderItemsInput)
    connectOrCreate?: InstanceType<typeof GoodCreateOrConnectWithoutOrderItemsInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: InstanceType<typeof GoodWhereUniqueInput>;
}

@InputType()
export class GoodCreateOrConnectWithoutCategoryInput {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    where!: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodCreateWithoutCategoryInput, {nullable:false})
    @Type(() => GoodCreateWithoutCategoryInput)
    create!: InstanceType<typeof GoodCreateWithoutCategoryInput>;
}

@InputType()
export class GoodCreateOrConnectWithoutOrderItemsInput {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    where!: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodCreateWithoutOrderItemsInput, {nullable:false})
    @Type(() => GoodCreateWithoutOrderItemsInput)
    create!: InstanceType<typeof GoodCreateWithoutOrderItemsInput>;
}

@InputType()
export class GoodCreateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OrderItemCreateNestedManyWithoutGoodInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutGoodInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutGoodInput>;
}

@InputType()
export class GoodCreateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => CategoryCreateNestedOneWithoutGoodsInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutGoodsInput>;
}

@InputType()
export class GoodCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OrderItemCreateNestedManyWithoutGoodInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutGoodInput)
    orderItems?: InstanceType<typeof OrderItemCreateNestedManyWithoutGoodInput>;
    @Field(() => CategoryCreateNestedOneWithoutGoodsInput, {nullable:false})
    category!: InstanceType<typeof CategoryCreateNestedOneWithoutGoodsInput>;
}

@InputType()
export class GoodCreateimagesInput {
    @Field(() => [String], {nullable:false})
    set!: Array<string>;
}

@ArgsType()
export class GoodGroupByArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
    @Field(() => [GoodOrderByWithAggregationInput], {nullable:true})
    @Type(() => GoodOrderByWithAggregationInput)
    orderBy?: Array<GoodOrderByWithAggregationInput>;
    @Field(() => [GoodScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof GoodScalarFieldEnum>;
    @Field(() => GoodScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => GoodScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof GoodScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => GoodCountAggregateInput, {nullable:true})
    @Type(() => GoodCountAggregateInput)
    _count?: InstanceType<typeof GoodCountAggregateInput>;
    @Field(() => GoodAvgAggregateInput, {nullable:true})
    @Type(() => GoodAvgAggregateInput)
    _avg?: InstanceType<typeof GoodAvgAggregateInput>;
    @Field(() => GoodSumAggregateInput, {nullable:true})
    @Type(() => GoodSumAggregateInput)
    _sum?: InstanceType<typeof GoodSumAggregateInput>;
    @Field(() => GoodMinAggregateInput, {nullable:true})
    @Type(() => GoodMinAggregateInput)
    _min?: InstanceType<typeof GoodMinAggregateInput>;
    @Field(() => GoodMaxAggregateInput, {nullable:true})
    @Type(() => GoodMaxAggregateInput)
    _max?: InstanceType<typeof GoodMaxAggregateInput>;
}

@ObjectType()
export class GoodGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => GoodCountAggregate, {nullable:true})
    _count?: InstanceType<typeof GoodCountAggregate>;
    @Field(() => GoodAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof GoodAvgAggregate>;
    @Field(() => GoodSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof GoodSumAggregate>;
    @Field(() => GoodMinAggregate, {nullable:true})
    _min?: InstanceType<typeof GoodMinAggregate>;
    @Field(() => GoodMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof GoodMaxAggregate>;
}

@InputType()
export class GoodListRelationFilter {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    every?: InstanceType<typeof GoodWhereInput>;
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    some?: InstanceType<typeof GoodWhereInput>;
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    none?: InstanceType<typeof GoodWhereInput>;
}

@InputType()
export class GoodMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
}

@ObjectType()
export class GoodMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class GoodMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@InputType()
export class GoodMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    slug?: true;
    @Field(() => Boolean, {nullable:true})
    categoryId?: true;
}

@ObjectType()
export class GoodMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class GoodMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
}

@InputType()
export class GoodOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class GoodOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
    @Field(() => GoodCountOrderByAggregateInput, {nullable:true})
    @Type(() => GoodCountOrderByAggregateInput)
    _count?: InstanceType<typeof GoodCountOrderByAggregateInput>;
    @Field(() => GoodAvgOrderByAggregateInput, {nullable:true})
    @Type(() => GoodAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof GoodAvgOrderByAggregateInput>;
    @Field(() => GoodMaxOrderByAggregateInput, {nullable:true})
    @Type(() => GoodMaxOrderByAggregateInput)
    _max?: InstanceType<typeof GoodMaxOrderByAggregateInput>;
    @Field(() => GoodMinOrderByAggregateInput, {nullable:true})
    @Type(() => GoodMinOrderByAggregateInput)
    _min?: InstanceType<typeof GoodMinOrderByAggregateInput>;
    @Field(() => GoodSumOrderByAggregateInput, {nullable:true})
    @Type(() => GoodSumOrderByAggregateInput)
    _sum?: InstanceType<typeof GoodSumOrderByAggregateInput>;
}

@InputType()
export class GoodOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    images?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    slug?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    categoryId?: keyof typeof SortOrder;
    @Field(() => OrderItemOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OrderItemOrderByRelationAggregateInput)
    orderItems?: InstanceType<typeof OrderItemOrderByRelationAggregateInput>;
    @Field(() => CategoryOrderByWithRelationInput, {nullable:true})
    category?: InstanceType<typeof CategoryOrderByWithRelationInput>;
}

@InputType()
export class GoodRelationFilter {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    is?: InstanceType<typeof GoodWhereInput>;
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    isNot?: InstanceType<typeof GoodWhereInput>;
}

@InputType()
export class GoodScalarWhereWithAggregatesInput {
    @Field(() => [GoodScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => GoodScalarWhereWithAggregatesInput)
    AND?: Array<GoodScalarWhereWithAggregatesInput>;
    @Field(() => [GoodScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => GoodScalarWhereWithAggregatesInput)
    OR?: Array<GoodScalarWhereWithAggregatesInput>;
    @Field(() => [GoodScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => GoodScalarWhereWithAggregatesInput)
    NOT?: Array<GoodScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    description?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    slug?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class GoodScalarWhereInput {
    @Field(() => [GoodScalarWhereInput], {nullable:true})
    @Type(() => GoodScalarWhereInput)
    AND?: Array<GoodScalarWhereInput>;
    @Field(() => [GoodScalarWhereInput], {nullable:true})
    @Type(() => GoodScalarWhereInput)
    OR?: Array<GoodScalarWhereInput>;
    @Field(() => [GoodScalarWhereInput], {nullable:true})
    @Type(() => GoodScalarWhereInput)
    NOT?: Array<GoodScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringFilter, {nullable:true})
    slug?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class GoodSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class GoodSumAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class GoodSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class GoodUncheckedCreateNestedManyWithoutCategoryInput {
    @Field(() => [GoodCreateWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateWithoutCategoryInput)
    create?: Array<GoodCreateWithoutCategoryInput>;
    @Field(() => [GoodCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<GoodCreateOrConnectWithoutCategoryInput>;
    @Field(() => GoodCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => GoodCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof GoodCreateManyCategoryInputEnvelope>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: Array<GoodWhereUniqueInput>;
}

@InputType()
export class GoodUncheckedCreateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutGoodInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutGoodInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutGoodInput>;
}

@InputType()
export class GoodUncheckedCreateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
}

@InputType()
export class GoodUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutGoodInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutGoodInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutGoodInput>;
}

@InputType()
export class GoodUncheckedUpdateManyWithoutCategoryNestedInput {
    @Field(() => [GoodCreateWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateWithoutCategoryInput)
    create?: Array<GoodCreateWithoutCategoryInput>;
    @Field(() => [GoodCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<GoodCreateOrConnectWithoutCategoryInput>;
    @Field(() => [GoodUpsertWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpsertWithWhereUniqueWithoutCategoryInput)
    upsert?: Array<GoodUpsertWithWhereUniqueWithoutCategoryInput>;
    @Field(() => GoodCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => GoodCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof GoodCreateManyCategoryInputEnvelope>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    set?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    disconnect?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    delete?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodUpdateWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpdateWithWhereUniqueWithoutCategoryInput)
    update?: Array<GoodUpdateWithWhereUniqueWithoutCategoryInput>;
    @Field(() => [GoodUpdateManyWithWhereWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpdateManyWithWhereWithoutCategoryInput)
    updateMany?: Array<GoodUpdateManyWithWhereWithoutCategoryInput>;
    @Field(() => [GoodScalarWhereInput], {nullable:true})
    @Type(() => GoodScalarWhereInput)
    deleteMany?: Array<GoodScalarWhereInput>;
}

@InputType()
export class GoodUncheckedUpdateManyWithoutGoodsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
}

@InputType()
export class GoodUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class GoodUncheckedUpdateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OrderItemUncheckedUpdateManyWithoutGoodNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutGoodNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutGoodNestedInput>;
}

@InputType()
export class GoodUncheckedUpdateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
}

@InputType()
export class GoodUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => String, {nullable:true})
    categoryId?: string;
    @Field(() => OrderItemUncheckedUpdateManyWithoutGoodNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutGoodNestedInput)
    orderItems?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutGoodNestedInput>;
}

@InputType()
export class GoodUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
}

@InputType()
export class GoodUpdateManyWithWhereWithoutCategoryInput {
    @Field(() => GoodScalarWhereInput, {nullable:false})
    @Type(() => GoodScalarWhereInput)
    where!: InstanceType<typeof GoodScalarWhereInput>;
    @Field(() => GoodUpdateManyMutationInput, {nullable:false})
    @Type(() => GoodUpdateManyMutationInput)
    data!: InstanceType<typeof GoodUpdateManyMutationInput>;
}

@InputType()
export class GoodUpdateManyWithoutCategoryNestedInput {
    @Field(() => [GoodCreateWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateWithoutCategoryInput)
    create?: Array<GoodCreateWithoutCategoryInput>;
    @Field(() => [GoodCreateOrConnectWithoutCategoryInput], {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutCategoryInput)
    connectOrCreate?: Array<GoodCreateOrConnectWithoutCategoryInput>;
    @Field(() => [GoodUpsertWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpsertWithWhereUniqueWithoutCategoryInput)
    upsert?: Array<GoodUpsertWithWhereUniqueWithoutCategoryInput>;
    @Field(() => GoodCreateManyCategoryInputEnvelope, {nullable:true})
    @Type(() => GoodCreateManyCategoryInputEnvelope)
    createMany?: InstanceType<typeof GoodCreateManyCategoryInputEnvelope>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    set?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    disconnect?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    delete?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodWhereUniqueInput], {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: Array<GoodWhereUniqueInput>;
    @Field(() => [GoodUpdateWithWhereUniqueWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpdateWithWhereUniqueWithoutCategoryInput)
    update?: Array<GoodUpdateWithWhereUniqueWithoutCategoryInput>;
    @Field(() => [GoodUpdateManyWithWhereWithoutCategoryInput], {nullable:true})
    @Type(() => GoodUpdateManyWithWhereWithoutCategoryInput)
    updateMany?: Array<GoodUpdateManyWithWhereWithoutCategoryInput>;
    @Field(() => [GoodScalarWhereInput], {nullable:true})
    @Type(() => GoodScalarWhereInput)
    deleteMany?: Array<GoodScalarWhereInput>;
}

@InputType()
export class GoodUpdateOneWithoutOrderItemsNestedInput {
    @Field(() => GoodCreateWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateWithoutOrderItemsInput)
    create?: InstanceType<typeof GoodCreateWithoutOrderItemsInput>;
    @Field(() => GoodCreateOrConnectWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateOrConnectWithoutOrderItemsInput)
    connectOrCreate?: InstanceType<typeof GoodCreateOrConnectWithoutOrderItemsInput>;
    @Field(() => GoodUpsertWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodUpsertWithoutOrderItemsInput)
    upsert?: InstanceType<typeof GoodUpsertWithoutOrderItemsInput>;
    @Field(() => Boolean, {nullable:true})
    disconnect?: boolean;
    @Field(() => Boolean, {nullable:true})
    delete?: boolean;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    connect?: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodUpdateWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodUpdateWithoutOrderItemsInput)
    update?: InstanceType<typeof GoodUpdateWithoutOrderItemsInput>;
}

@InputType()
export class GoodUpdateWithWhereUniqueWithoutCategoryInput {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    where!: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodUpdateWithoutCategoryInput, {nullable:false})
    @Type(() => GoodUpdateWithoutCategoryInput)
    data!: InstanceType<typeof GoodUpdateWithoutCategoryInput>;
}

@InputType()
export class GoodUpdateWithoutCategoryInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OrderItemUpdateManyWithoutGoodNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutGoodNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutGoodNestedInput>;
}

@InputType()
export class GoodUpdateWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => CategoryUpdateOneRequiredWithoutGoodsNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutGoodsNestedInput>;
}

@InputType()
export class GoodUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => [String], {nullable:true})
    @Validator.IsString({each: true})
    images?: Array<string>;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
    @Field(() => OrderItemUpdateManyWithoutGoodNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutGoodNestedInput)
    orderItems?: InstanceType<typeof OrderItemUpdateManyWithoutGoodNestedInput>;
    @Field(() => CategoryUpdateOneRequiredWithoutGoodsNestedInput, {nullable:true})
    category?: InstanceType<typeof CategoryUpdateOneRequiredWithoutGoodsNestedInput>;
}

@InputType()
export class GoodUpdateimagesInput {
    @Field(() => [String], {nullable:true})
    set?: Array<string>;
    @Field(() => [String], {nullable:true})
    push?: Array<string>;
}

@InputType()
export class GoodUpsertWithWhereUniqueWithoutCategoryInput {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    where!: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodUpdateWithoutCategoryInput, {nullable:false})
    @Type(() => GoodUpdateWithoutCategoryInput)
    update!: InstanceType<typeof GoodUpdateWithoutCategoryInput>;
    @Field(() => GoodCreateWithoutCategoryInput, {nullable:false})
    @Type(() => GoodCreateWithoutCategoryInput)
    create!: InstanceType<typeof GoodCreateWithoutCategoryInput>;
}

@InputType()
export class GoodUpsertWithoutOrderItemsInput {
    @Field(() => GoodUpdateWithoutOrderItemsInput, {nullable:false})
    @Type(() => GoodUpdateWithoutOrderItemsInput)
    update!: InstanceType<typeof GoodUpdateWithoutOrderItemsInput>;
    @Field(() => GoodCreateWithoutOrderItemsInput, {nullable:false})
    @Type(() => GoodCreateWithoutOrderItemsInput)
    create!: InstanceType<typeof GoodCreateWithoutOrderItemsInput>;
}

@InputType()
export class GoodWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    slug?: string;
}

@InputType()
export class GoodWhereInput {
    @Field(() => [GoodWhereInput], {nullable:true})
    @Type(() => GoodWhereInput)
    AND?: Array<GoodWhereInput>;
    @Field(() => [GoodWhereInput], {nullable:true})
    @Type(() => GoodWhereInput)
    OR?: Array<GoodWhereInput>;
    @Field(() => [GoodWhereInput], {nullable:true})
    @Type(() => GoodWhereInput)
    NOT?: Array<GoodWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringNullableListFilter, {nullable:true})
    images?: InstanceType<typeof StringNullableListFilter>;
    @Field(() => StringFilter, {nullable:true})
    slug?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    categoryId?: InstanceType<typeof StringFilter>;
    @Field(() => OrderItemListRelationFilter, {nullable:true})
    @Type(() => OrderItemListRelationFilter)
    orderItems?: InstanceType<typeof OrderItemListRelationFilter>;
    @Field(() => CategoryRelationFilter, {nullable:true})
    category?: InstanceType<typeof CategoryRelationFilter>;
}

@ObjectType()
export class Good {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false,defaultValue:''})
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    /** @Validator .@ArrayMaxSize(5) */
    @Field(() => [String], {nullable:true,description:'@Validator.@ArrayMaxSize(5)'})
    images!: Array<string>;
    @Field(() => String, {nullable:false})
    slug!: string;
    @Field(() => String, {nullable:false})
    categoryId!: string;
    @Field(() => [OrderItem], {nullable:true})
    orderItems?: Array<OrderItem>;
    @Field(() => Category, {nullable:false})
    category?: InstanceType<typeof Category>;
    @Field(() => GoodCount, {nullable:false})
    _count?: InstanceType<typeof GoodCount>;
}

@ArgsType()
export class UpdateManyGoodArgs {
    @Field(() => GoodUpdateManyMutationInput, {nullable:false})
    @Type(() => GoodUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof GoodUpdateManyMutationInput>;
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
}

@ArgsType()
export class UpdateOneGoodArgs {
    @Field(() => GoodUpdateInput, {nullable:false})
    @Type(() => GoodUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof GoodUpdateInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof GoodWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneGoodArgs {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => GoodCreateInput, {nullable:false})
    @Type(() => GoodCreateInput)
    create!: InstanceType<typeof GoodCreateInput>;
    @Field(() => GoodUpdateInput, {nullable:false})
    @Type(() => GoodUpdateInput)
    update!: InstanceType<typeof GoodUpdateInput>;
}

@ObjectType()
export class AggregateIngradientLabel {
    @Field(() => IngradientLabelCountAggregate, {nullable:true})
    _count?: InstanceType<typeof IngradientLabelCountAggregate>;
    @Field(() => IngradientLabelMinAggregate, {nullable:true})
    _min?: InstanceType<typeof IngradientLabelMinAggregate>;
    @Field(() => IngradientLabelMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof IngradientLabelMaxAggregate>;
}

@ArgsType()
export class CreateManyIngradientLabelArgs {
    @Field(() => [IngradientLabelCreateManyInput], {nullable:false})
    @Type(() => IngradientLabelCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<IngradientLabelCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneIngradientLabelArgs {
    @Field(() => IngradientLabelCreateInput, {nullable:false})
    @Type(() => IngradientLabelCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof IngradientLabelCreateInput>;
}

@ArgsType()
export class DeleteManyIngradientLabelArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
}

@ArgsType()
export class DeleteOneIngradientLabelArgs {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
}

@ArgsType()
export class FindFirstIngradientLabelArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<IngradientLabelOrderByWithRelationInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [IngradientLabelScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof IngradientLabelScalarFieldEnum>;
}

@ArgsType()
export class FindManyIngradientLabelArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<IngradientLabelOrderByWithRelationInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [IngradientLabelScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof IngradientLabelScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueIngradientLabelArgs {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
}

@ArgsType()
export class IngradientLabelAggregateArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<IngradientLabelOrderByWithRelationInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => IngradientLabelCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof IngradientLabelCountAggregateInput>;
    @Field(() => IngradientLabelMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof IngradientLabelMinAggregateInput>;
    @Field(() => IngradientLabelMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof IngradientLabelMaxAggregateInput>;
}

@InputType()
export class IngradientLabelCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class IngradientLabelCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    image!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class IngradientLabelCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@ObjectType()
export class IngradientLabelCount {
    @Field(() => Int, {nullable:false})
    toppings?: number;
    @Field(() => Int, {nullable:false})
    dishes?: number;
}

@InputType()
export class IngradientLabelCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
}

@InputType()
export class IngradientLabelCreateNestedManyWithoutDishesInput {
    @Field(() => [IngradientLabelCreateWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create?: Array<IngradientLabelCreateWithoutDishesInput>;
    @Field(() => [IngradientLabelCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<IngradientLabelCreateOrConnectWithoutDishesInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: Array<IngradientLabelWhereUniqueInput>;
}

@InputType()
export class IngradientLabelCreateNestedOneWithoutToppingsInput {
    @Field(() => IngradientLabelCreateWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelCreateWithoutToppingsInput)
    create?: InstanceType<typeof IngradientLabelCreateWithoutToppingsInput>;
    @Field(() => IngradientLabelCreateOrConnectWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutToppingsInput)
    connectOrCreate?: InstanceType<typeof IngradientLabelCreateOrConnectWithoutToppingsInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
}

@InputType()
export class IngradientLabelCreateOrConnectWithoutDishesInput {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelCreateWithoutDishesInput, {nullable:false})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create!: InstanceType<typeof IngradientLabelCreateWithoutDishesInput>;
}

@InputType()
export class IngradientLabelCreateOrConnectWithoutToppingsInput {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelCreateWithoutToppingsInput, {nullable:false})
    @Type(() => IngradientLabelCreateWithoutToppingsInput)
    create!: InstanceType<typeof IngradientLabelCreateWithoutToppingsInput>;
}

@InputType()
export class IngradientLabelCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => ToppingCreateNestedManyWithoutIngredientLabelInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutIngredientLabelInput)
    toppings?: InstanceType<typeof ToppingCreateNestedManyWithoutIngredientLabelInput>;
}

@InputType()
export class IngradientLabelCreateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishCreateNestedManyWithoutIngradientsInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutIngradientsInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutIngradientsInput>;
}

@InputType()
export class IngradientLabelCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => ToppingCreateNestedManyWithoutIngredientLabelInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutIngredientLabelInput)
    toppings?: InstanceType<typeof ToppingCreateNestedManyWithoutIngredientLabelInput>;
    @Field(() => DishCreateNestedManyWithoutIngradientsInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutIngradientsInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutIngradientsInput>;
}

@ArgsType()
export class IngradientLabelGroupByArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<IngradientLabelOrderByWithAggregationInput>;
    @Field(() => [IngradientLabelScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof IngradientLabelScalarFieldEnum>;
    @Field(() => IngradientLabelScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof IngradientLabelScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => IngradientLabelCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof IngradientLabelCountAggregateInput>;
    @Field(() => IngradientLabelMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof IngradientLabelMinAggregateInput>;
    @Field(() => IngradientLabelMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof IngradientLabelMaxAggregateInput>;
}

@ObjectType()
export class IngradientLabelGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => IngradientLabelCountAggregate, {nullable:true})
    _count?: InstanceType<typeof IngradientLabelCountAggregate>;
    @Field(() => IngradientLabelMinAggregate, {nullable:true})
    _min?: InstanceType<typeof IngradientLabelMinAggregate>;
    @Field(() => IngradientLabelMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof IngradientLabelMaxAggregate>;
}

@InputType()
export class IngradientLabelListRelationFilter {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    every?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    some?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    none?: InstanceType<typeof IngradientLabelWhereInput>;
}

@InputType()
export class IngradientLabelMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
}

@ObjectType()
export class IngradientLabelMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class IngradientLabelMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@InputType()
export class IngradientLabelMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    image?: true;
}

@ObjectType()
export class IngradientLabelMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class IngradientLabelMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
}

@InputType()
export class IngradientLabelOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class IngradientLabelOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
    @Field(() => IngradientLabelCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof IngradientLabelCountOrderByAggregateInput>;
    @Field(() => IngradientLabelMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof IngradientLabelMaxOrderByAggregateInput>;
    @Field(() => IngradientLabelMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof IngradientLabelMinOrderByAggregateInput>;
}

@InputType()
export class IngradientLabelOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    image?: keyof typeof SortOrder;
    @Field(() => ToppingOrderByRelationAggregateInput, {nullable:true})
    @Type(() => ToppingOrderByRelationAggregateInput)
    toppings?: InstanceType<typeof ToppingOrderByRelationAggregateInput>;
    @Field(() => DishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => DishOrderByRelationAggregateInput)
    dishes?: InstanceType<typeof DishOrderByRelationAggregateInput>;
}

@InputType()
export class IngradientLabelRelationFilter {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    is?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    isNot?: InstanceType<typeof IngradientLabelWhereInput>;
}

@InputType()
export class IngradientLabelScalarWhereWithAggregatesInput {
    @Field(() => [IngradientLabelScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<IngradientLabelScalarWhereWithAggregatesInput>;
    @Field(() => [IngradientLabelScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<IngradientLabelScalarWhereWithAggregatesInput>;
    @Field(() => [IngradientLabelScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<IngradientLabelScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    image?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class IngradientLabelScalarWhereInput {
    @Field(() => [IngradientLabelScalarWhereInput], {nullable:true})
    AND?: Array<IngradientLabelScalarWhereInput>;
    @Field(() => [IngradientLabelScalarWhereInput], {nullable:true})
    OR?: Array<IngradientLabelScalarWhereInput>;
    @Field(() => [IngradientLabelScalarWhereInput], {nullable:true})
    NOT?: Array<IngradientLabelScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    image?: InstanceType<typeof StringFilter>;
}

@InputType()
export class IngradientLabelUncheckedCreateNestedManyWithoutDishesInput {
    @Field(() => [IngradientLabelCreateWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create?: Array<IngradientLabelCreateWithoutDishesInput>;
    @Field(() => [IngradientLabelCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<IngradientLabelCreateOrConnectWithoutDishesInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: Array<IngradientLabelWhereUniqueInput>;
}

@InputType()
export class IngradientLabelUncheckedCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput)
    toppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput>;
}

@InputType()
export class IngradientLabelUncheckedCreateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => DishUncheckedCreateNestedManyWithoutIngradientsInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutIngradientsInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutIngradientsInput>;
}

@InputType()
export class IngradientLabelUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput)
    toppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput>;
    @Field(() => DishUncheckedCreateNestedManyWithoutIngradientsInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutIngradientsInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutIngradientsInput>;
}

@InputType()
export class IngradientLabelUncheckedUpdateManyWithoutDishesNestedInput {
    @Field(() => [IngradientLabelCreateWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create?: Array<IngradientLabelCreateWithoutDishesInput>;
    @Field(() => [IngradientLabelCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<IngradientLabelCreateOrConnectWithoutDishesInput>;
    @Field(() => [IngradientLabelUpsertWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpsertWithWhereUniqueWithoutDishesInput)
    upsert?: Array<IngradientLabelUpsertWithWhereUniqueWithoutDishesInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    set?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    disconnect?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    delete?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelUpdateWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpdateWithWhereUniqueWithoutDishesInput)
    update?: Array<IngradientLabelUpdateWithWhereUniqueWithoutDishesInput>;
    @Field(() => [IngradientLabelUpdateManyWithWhereWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpdateManyWithWhereWithoutDishesInput)
    updateMany?: Array<IngradientLabelUpdateManyWithWhereWithoutDishesInput>;
    @Field(() => [IngradientLabelScalarWhereInput], {nullable:true})
    @Type(() => IngradientLabelScalarWhereInput)
    deleteMany?: Array<IngradientLabelScalarWhereInput>;
}

@InputType()
export class IngradientLabelUncheckedUpdateManyWithoutIngradientsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class IngradientLabelUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class IngradientLabelUncheckedUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput)
    toppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput>;
}

@InputType()
export class IngradientLabelUncheckedUpdateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUncheckedUpdateManyWithoutIngradientsNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutIngradientsNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutIngradientsNestedInput>;
}

@InputType()
export class IngradientLabelUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput)
    toppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput>;
    @Field(() => DishUncheckedUpdateManyWithoutIngradientsNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutIngradientsNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutIngradientsNestedInput>;
}

@InputType()
export class IngradientLabelUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
}

@InputType()
export class IngradientLabelUpdateManyWithWhereWithoutDishesInput {
    @Field(() => IngradientLabelScalarWhereInput, {nullable:false})
    @Type(() => IngradientLabelScalarWhereInput)
    where!: InstanceType<typeof IngradientLabelScalarWhereInput>;
    @Field(() => IngradientLabelUpdateManyMutationInput, {nullable:false})
    @Type(() => IngradientLabelUpdateManyMutationInput)
    data!: InstanceType<typeof IngradientLabelUpdateManyMutationInput>;
}

@InputType()
export class IngradientLabelUpdateManyWithoutDishesNestedInput {
    @Field(() => [IngradientLabelCreateWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create?: Array<IngradientLabelCreateWithoutDishesInput>;
    @Field(() => [IngradientLabelCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<IngradientLabelCreateOrConnectWithoutDishesInput>;
    @Field(() => [IngradientLabelUpsertWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpsertWithWhereUniqueWithoutDishesInput)
    upsert?: Array<IngradientLabelUpsertWithWhereUniqueWithoutDishesInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    set?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    disconnect?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    delete?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelWhereUniqueInput], {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: Array<IngradientLabelWhereUniqueInput>;
    @Field(() => [IngradientLabelUpdateWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpdateWithWhereUniqueWithoutDishesInput)
    update?: Array<IngradientLabelUpdateWithWhereUniqueWithoutDishesInput>;
    @Field(() => [IngradientLabelUpdateManyWithWhereWithoutDishesInput], {nullable:true})
    @Type(() => IngradientLabelUpdateManyWithWhereWithoutDishesInput)
    updateMany?: Array<IngradientLabelUpdateManyWithWhereWithoutDishesInput>;
    @Field(() => [IngradientLabelScalarWhereInput], {nullable:true})
    @Type(() => IngradientLabelScalarWhereInput)
    deleteMany?: Array<IngradientLabelScalarWhereInput>;
}

@InputType()
export class IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput {
    @Field(() => IngradientLabelCreateWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelCreateWithoutToppingsInput)
    create?: InstanceType<typeof IngradientLabelCreateWithoutToppingsInput>;
    @Field(() => IngradientLabelCreateOrConnectWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelCreateOrConnectWithoutToppingsInput)
    connectOrCreate?: InstanceType<typeof IngradientLabelCreateOrConnectWithoutToppingsInput>;
    @Field(() => IngradientLabelUpsertWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelUpsertWithoutToppingsInput)
    upsert?: InstanceType<typeof IngradientLabelUpsertWithoutToppingsInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    @Type(() => IngradientLabelWhereUniqueInput)
    connect?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelUpdateWithoutToppingsInput, {nullable:true})
    @Type(() => IngradientLabelUpdateWithoutToppingsInput)
    update?: InstanceType<typeof IngradientLabelUpdateWithoutToppingsInput>;
}

@InputType()
export class IngradientLabelUpdateWithWhereUniqueWithoutDishesInput {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelUpdateWithoutDishesInput, {nullable:false})
    @Type(() => IngradientLabelUpdateWithoutDishesInput)
    data!: InstanceType<typeof IngradientLabelUpdateWithoutDishesInput>;
}

@InputType()
export class IngradientLabelUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => ToppingUpdateManyWithoutIngredientLabelNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutIngredientLabelNestedInput)
    toppings?: InstanceType<typeof ToppingUpdateManyWithoutIngredientLabelNestedInput>;
}

@InputType()
export class IngradientLabelUpdateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => DishUpdateManyWithoutIngradientsNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutIngradientsNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutIngradientsNestedInput>;
}

@InputType()
export class IngradientLabelUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    image?: string;
    @Field(() => ToppingUpdateManyWithoutIngredientLabelNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutIngredientLabelNestedInput)
    toppings?: InstanceType<typeof ToppingUpdateManyWithoutIngredientLabelNestedInput>;
    @Field(() => DishUpdateManyWithoutIngradientsNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutIngradientsNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutIngradientsNestedInput>;
}

@InputType()
export class IngradientLabelUpsertWithWhereUniqueWithoutDishesInput {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelUpdateWithoutDishesInput, {nullable:false})
    @Type(() => IngradientLabelUpdateWithoutDishesInput)
    update!: InstanceType<typeof IngradientLabelUpdateWithoutDishesInput>;
    @Field(() => IngradientLabelCreateWithoutDishesInput, {nullable:false})
    @Type(() => IngradientLabelCreateWithoutDishesInput)
    create!: InstanceType<typeof IngradientLabelCreateWithoutDishesInput>;
}

@InputType()
export class IngradientLabelUpsertWithoutToppingsInput {
    @Field(() => IngradientLabelUpdateWithoutToppingsInput, {nullable:false})
    @Type(() => IngradientLabelUpdateWithoutToppingsInput)
    update!: InstanceType<typeof IngradientLabelUpdateWithoutToppingsInput>;
    @Field(() => IngradientLabelCreateWithoutToppingsInput, {nullable:false})
    @Type(() => IngradientLabelCreateWithoutToppingsInput)
    create!: InstanceType<typeof IngradientLabelCreateWithoutToppingsInput>;
}

@InputType()
export class IngradientLabelWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
}

@InputType()
export class IngradientLabelWhereInput {
    @Field(() => [IngradientLabelWhereInput], {nullable:true})
    AND?: Array<IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelWhereInput], {nullable:true})
    OR?: Array<IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelWhereInput], {nullable:true})
    NOT?: Array<IngradientLabelWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    image?: InstanceType<typeof StringFilter>;
    @Field(() => ToppingListRelationFilter, {nullable:true})
    @Type(() => ToppingListRelationFilter)
    toppings?: InstanceType<typeof ToppingListRelationFilter>;
    @Field(() => DishListRelationFilter, {nullable:true})
    @Type(() => DishListRelationFilter)
    dishes?: InstanceType<typeof DishListRelationFilter>;
}

@ObjectType()
export class IngradientLabel {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false})
    image!: string;
    @Field(() => [Topping], {nullable:true})
    toppings?: Array<Topping>;
    @Field(() => [Dish], {nullable:true})
    dishes?: Array<Dish>;
    @Field(() => IngradientLabelCount, {nullable:false})
    _count?: InstanceType<typeof IngradientLabelCount>;
}

@ArgsType()
export class UpdateManyIngradientLabelArgs {
    @Field(() => IngradientLabelUpdateManyMutationInput, {nullable:false})
    @Type(() => IngradientLabelUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof IngradientLabelUpdateManyMutationInput>;
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
}

@ArgsType()
export class UpdateOneIngradientLabelArgs {
    @Field(() => IngradientLabelUpdateInput, {nullable:false})
    @Type(() => IngradientLabelUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof IngradientLabelUpdateInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneIngradientLabelArgs {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => IngradientLabelCreateInput, {nullable:false})
    @Type(() => IngradientLabelCreateInput)
    create!: InstanceType<typeof IngradientLabelCreateInput>;
    @Field(() => IngradientLabelUpdateInput, {nullable:false})
    @Type(() => IngradientLabelUpdateInput)
    update!: InstanceType<typeof IngradientLabelUpdateInput>;
}

@ObjectType()
export class AggregateOptions {
    @Field(() => OptionsCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OptionsCountAggregate>;
    @Field(() => OptionsAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OptionsAvgAggregate>;
    @Field(() => OptionsSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OptionsSumAggregate>;
    @Field(() => OptionsMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OptionsMinAggregate>;
    @Field(() => OptionsMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OptionsMaxAggregate>;
}

@ArgsType()
export class CreateManyOptionsArgs {
    @Field(() => [OptionsCreateManyInput], {nullable:false})
    @Type(() => OptionsCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<OptionsCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneOptionsArgs {
    @Field(() => OptionsCreateInput, {nullable:false})
    @Type(() => OptionsCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OptionsCreateInput>;
}

@ArgsType()
export class DeleteManyOptionsArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
}

@ArgsType()
export class DeleteOneOptionsArgs {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
}

@ArgsType()
export class FindFirstOptionsArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => [OptionsOrderByWithRelationInput], {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    orderBy?: Array<OptionsOrderByWithRelationInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    cursor?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OptionsScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OptionsScalarFieldEnum>;
}

@ArgsType()
export class FindManyOptionsArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => [OptionsOrderByWithRelationInput], {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    orderBy?: Array<OptionsOrderByWithRelationInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    cursor?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OptionsScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OptionsScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueOptionsArgs {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
}

@ArgsType()
export class OptionsAggregateArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => [OptionsOrderByWithRelationInput], {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    orderBy?: Array<OptionsOrderByWithRelationInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    cursor?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OptionsCountAggregateInput, {nullable:true})
    @Type(() => OptionsCountAggregateInput)
    _count?: InstanceType<typeof OptionsCountAggregateInput>;
    @Field(() => OptionsAvgAggregateInput, {nullable:true})
    @Type(() => OptionsAvgAggregateInput)
    _avg?: InstanceType<typeof OptionsAvgAggregateInput>;
    @Field(() => OptionsSumAggregateInput, {nullable:true})
    @Type(() => OptionsSumAggregateInput)
    _sum?: InstanceType<typeof OptionsSumAggregateInput>;
    @Field(() => OptionsMinAggregateInput, {nullable:true})
    @Type(() => OptionsMinAggregateInput)
    _min?: InstanceType<typeof OptionsMinAggregateInput>;
    @Field(() => OptionsMaxAggregateInput, {nullable:true})
    @Type(() => OptionsMaxAggregateInput)
    _max?: InstanceType<typeof OptionsMaxAggregateInput>;
}

@InputType()
export class OptionsAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OptionsAvgAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OptionsAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OptionsCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class OptionsCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    description!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class OptionsCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@ObjectType()
export class OptionsCount {
    @Field(() => Int, {nullable:false})
    dishes?: number;
    @Field(() => Int, {nullable:false})
    toppings?: number;
    @Field(() => Int, {nullable:false})
    CustomerDish?: number;
}

@InputType()
export class OptionsCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
}

@InputType()
export class OptionsCreateNestedManyWithoutDishesInput {
    @Field(() => [OptionsCreateWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateWithoutDishesInput)
    create?: Array<OptionsCreateWithoutDishesInput>;
    @Field(() => [OptionsCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<OptionsCreateOrConnectWithoutDishesInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: Array<OptionsWhereUniqueInput>;
}

@InputType()
export class OptionsCreateNestedOneWithoutCustomerDishInput {
    @Field(() => OptionsCreateWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsCreateWithoutCustomerDishInput)
    create?: InstanceType<typeof OptionsCreateWithoutCustomerDishInput>;
    @Field(() => OptionsCreateOrConnectWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: InstanceType<typeof OptionsCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: InstanceType<typeof OptionsWhereUniqueInput>;
}

@InputType()
export class OptionsCreateNestedOneWithoutToppingsInput {
    @Field(() => OptionsCreateWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsCreateWithoutToppingsInput)
    create?: InstanceType<typeof OptionsCreateWithoutToppingsInput>;
    @Field(() => OptionsCreateOrConnectWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutToppingsInput)
    connectOrCreate?: InstanceType<typeof OptionsCreateOrConnectWithoutToppingsInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: InstanceType<typeof OptionsWhereUniqueInput>;
}

@InputType()
export class OptionsCreateOrConnectWithoutCustomerDishInput {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsCreateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateWithoutCustomerDishInput)
    create!: InstanceType<typeof OptionsCreateWithoutCustomerDishInput>;
}

@InputType()
export class OptionsCreateOrConnectWithoutDishesInput {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsCreateWithoutDishesInput, {nullable:false})
    @Type(() => OptionsCreateWithoutDishesInput)
    create!: InstanceType<typeof OptionsCreateWithoutDishesInput>;
}

@InputType()
export class OptionsCreateOrConnectWithoutToppingsInput {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsCreateWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsCreateWithoutToppingsInput)
    create!: InstanceType<typeof OptionsCreateWithoutToppingsInput>;
}

@InputType()
export class OptionsCreateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutOptionsInput>;
    @Field(() => ToppingCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingCreateNestedManyWithoutOptionInput>;
}

@InputType()
export class OptionsCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingCreateNestedManyWithoutOptionInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedOptionInput>;
}

@InputType()
export class OptionsCreateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutOptionsInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedOptionInput>;
}

@InputType()
export class OptionsCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishCreateNestedManyWithoutOptionsInput>;
    @Field(() => ToppingCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingCreateNestedManyWithoutOptionInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedOptionInput>;
}

@ArgsType()
export class OptionsGroupByArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => [OptionsOrderByWithAggregationInput], {nullable:true})
    @Type(() => OptionsOrderByWithAggregationInput)
    orderBy?: Array<OptionsOrderByWithAggregationInput>;
    @Field(() => [OptionsScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof OptionsScalarFieldEnum>;
    @Field(() => OptionsScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => OptionsScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof OptionsScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OptionsCountAggregateInput, {nullable:true})
    @Type(() => OptionsCountAggregateInput)
    _count?: InstanceType<typeof OptionsCountAggregateInput>;
    @Field(() => OptionsAvgAggregateInput, {nullable:true})
    @Type(() => OptionsAvgAggregateInput)
    _avg?: InstanceType<typeof OptionsAvgAggregateInput>;
    @Field(() => OptionsSumAggregateInput, {nullable:true})
    @Type(() => OptionsSumAggregateInput)
    _sum?: InstanceType<typeof OptionsSumAggregateInput>;
    @Field(() => OptionsMinAggregateInput, {nullable:true})
    @Type(() => OptionsMinAggregateInput)
    _min?: InstanceType<typeof OptionsMinAggregateInput>;
    @Field(() => OptionsMaxAggregateInput, {nullable:true})
    @Type(() => OptionsMaxAggregateInput)
    _max?: InstanceType<typeof OptionsMaxAggregateInput>;
}

@ObjectType()
export class OptionsGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => OptionsCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OptionsCountAggregate>;
    @Field(() => OptionsAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OptionsAvgAggregate>;
    @Field(() => OptionsSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OptionsSumAggregate>;
    @Field(() => OptionsMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OptionsMinAggregate>;
    @Field(() => OptionsMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OptionsMaxAggregate>;
}

@InputType()
export class OptionsListRelationFilter {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    every?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    some?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    none?: InstanceType<typeof OptionsWhereInput>;
}

@InputType()
export class OptionsMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OptionsMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OptionsMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OptionsMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OptionsMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OptionsMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OptionsOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class OptionsOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => OptionsCountOrderByAggregateInput, {nullable:true})
    @Type(() => OptionsCountOrderByAggregateInput)
    _count?: InstanceType<typeof OptionsCountOrderByAggregateInput>;
    @Field(() => OptionsAvgOrderByAggregateInput, {nullable:true})
    @Type(() => OptionsAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof OptionsAvgOrderByAggregateInput>;
    @Field(() => OptionsMaxOrderByAggregateInput, {nullable:true})
    @Type(() => OptionsMaxOrderByAggregateInput)
    _max?: InstanceType<typeof OptionsMaxOrderByAggregateInput>;
    @Field(() => OptionsMinOrderByAggregateInput, {nullable:true})
    @Type(() => OptionsMinOrderByAggregateInput)
    _min?: InstanceType<typeof OptionsMinOrderByAggregateInput>;
    @Field(() => OptionsSumOrderByAggregateInput, {nullable:true})
    @Type(() => OptionsSumOrderByAggregateInput)
    _sum?: InstanceType<typeof OptionsSumOrderByAggregateInput>;
}

@InputType()
export class OptionsOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => DishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => DishOrderByRelationAggregateInput)
    dishes?: InstanceType<typeof DishOrderByRelationAggregateInput>;
    @Field(() => ToppingOrderByRelationAggregateInput, {nullable:true})
    @Type(() => ToppingOrderByRelationAggregateInput)
    toppings?: InstanceType<typeof ToppingOrderByRelationAggregateInput>;
    @Field(() => CustomerDishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => CustomerDishOrderByRelationAggregateInput)
    CustomerDish?: InstanceType<typeof CustomerDishOrderByRelationAggregateInput>;
}

@InputType()
export class OptionsRelationFilter {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    is?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    isNot?: InstanceType<typeof OptionsWhereInput>;
}

@InputType()
export class OptionsScalarWhereWithAggregatesInput {
    @Field(() => [OptionsScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OptionsScalarWhereWithAggregatesInput)
    AND?: Array<OptionsScalarWhereWithAggregatesInput>;
    @Field(() => [OptionsScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OptionsScalarWhereWithAggregatesInput)
    OR?: Array<OptionsScalarWhereWithAggregatesInput>;
    @Field(() => [OptionsScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OptionsScalarWhereWithAggregatesInput)
    NOT?: Array<OptionsScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    description?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
}

@InputType()
export class OptionsScalarWhereInput {
    @Field(() => [OptionsScalarWhereInput], {nullable:true})
    @Type(() => OptionsScalarWhereInput)
    AND?: Array<OptionsScalarWhereInput>;
    @Field(() => [OptionsScalarWhereInput], {nullable:true})
    @Type(() => OptionsScalarWhereInput)
    OR?: Array<OptionsScalarWhereInput>;
    @Field(() => [OptionsScalarWhereInput], {nullable:true})
    @Type(() => OptionsScalarWhereInput)
    NOT?: Array<OptionsScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
}

@InputType()
export class OptionsSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OptionsSumAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OptionsSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OptionsUncheckedCreateNestedManyWithoutDishesInput {
    @Field(() => [OptionsCreateWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateWithoutDishesInput)
    create?: Array<OptionsCreateWithoutDishesInput>;
    @Field(() => [OptionsCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<OptionsCreateOrConnectWithoutDishesInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: Array<OptionsWhereUniqueInput>;
}

@InputType()
export class OptionsUncheckedCreateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishUncheckedCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutOptionsInput>;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutOptionInput>;
}

@InputType()
export class OptionsUncheckedCreateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutOptionInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput>;
}

@InputType()
export class OptionsUncheckedCreateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishUncheckedCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutOptionsInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput>;
}

@InputType()
export class OptionsUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => DishUncheckedCreateNestedManyWithoutOptionsInput, {nullable:true})
    @Type(() => DishUncheckedCreateNestedManyWithoutOptionsInput)
    dishes?: InstanceType<typeof DishUncheckedCreateNestedManyWithoutOptionsInput>;
    @Field(() => ToppingUncheckedCreateNestedManyWithoutOptionInput, {nullable:true})
    @Type(() => ToppingUncheckedCreateNestedManyWithoutOptionInput)
    toppings?: InstanceType<typeof ToppingUncheckedCreateNestedManyWithoutOptionInput>;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedOptionInput>;
}

@InputType()
export class OptionsUncheckedUpdateManyWithoutDishesNestedInput {
    @Field(() => [OptionsCreateWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateWithoutDishesInput)
    create?: Array<OptionsCreateWithoutDishesInput>;
    @Field(() => [OptionsCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<OptionsCreateOrConnectWithoutDishesInput>;
    @Field(() => [OptionsUpsertWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpsertWithWhereUniqueWithoutDishesInput)
    upsert?: Array<OptionsUpsertWithWhereUniqueWithoutDishesInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    set?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    disconnect?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    delete?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsUpdateWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpdateWithWhereUniqueWithoutDishesInput)
    update?: Array<OptionsUpdateWithWhereUniqueWithoutDishesInput>;
    @Field(() => [OptionsUpdateManyWithWhereWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpdateManyWithWhereWithoutDishesInput)
    updateMany?: Array<OptionsUpdateManyWithWhereWithoutDishesInput>;
    @Field(() => [OptionsScalarWhereInput], {nullable:true})
    @Type(() => OptionsScalarWhereInput)
    deleteMany?: Array<OptionsScalarWhereInput>;
}

@InputType()
export class OptionsUncheckedUpdateManyWithoutOptionsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class OptionsUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class OptionsUncheckedUpdateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUncheckedUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutOptionsNestedInput>;
    @Field(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutOptionNestedInput>;
}

@InputType()
export class OptionsUncheckedUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutOptionNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUncheckedUpdateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUncheckedUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutOptionsNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUncheckedUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUncheckedUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUncheckedUpdateManyWithoutOptionsNestedInput>;
    @Field(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUncheckedUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUncheckedUpdateManyWithoutOptionNestedInput>;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class OptionsUpdateManyWithWhereWithoutDishesInput {
    @Field(() => OptionsScalarWhereInput, {nullable:false})
    @Type(() => OptionsScalarWhereInput)
    where!: InstanceType<typeof OptionsScalarWhereInput>;
    @Field(() => OptionsUpdateManyMutationInput, {nullable:false})
    @Type(() => OptionsUpdateManyMutationInput)
    data!: InstanceType<typeof OptionsUpdateManyMutationInput>;
}

@InputType()
export class OptionsUpdateManyWithoutDishesNestedInput {
    @Field(() => [OptionsCreateWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateWithoutDishesInput)
    create?: Array<OptionsCreateWithoutDishesInput>;
    @Field(() => [OptionsCreateOrConnectWithoutDishesInput], {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutDishesInput)
    connectOrCreate?: Array<OptionsCreateOrConnectWithoutDishesInput>;
    @Field(() => [OptionsUpsertWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpsertWithWhereUniqueWithoutDishesInput)
    upsert?: Array<OptionsUpsertWithWhereUniqueWithoutDishesInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    set?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    disconnect?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    delete?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsWhereUniqueInput], {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: Array<OptionsWhereUniqueInput>;
    @Field(() => [OptionsUpdateWithWhereUniqueWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpdateWithWhereUniqueWithoutDishesInput)
    update?: Array<OptionsUpdateWithWhereUniqueWithoutDishesInput>;
    @Field(() => [OptionsUpdateManyWithWhereWithoutDishesInput], {nullable:true})
    @Type(() => OptionsUpdateManyWithWhereWithoutDishesInput)
    updateMany?: Array<OptionsUpdateManyWithWhereWithoutDishesInput>;
    @Field(() => [OptionsScalarWhereInput], {nullable:true})
    @Type(() => OptionsScalarWhereInput)
    deleteMany?: Array<OptionsScalarWhereInput>;
}

@InputType()
export class OptionsUpdateOneRequiredWithoutCustomerDishNestedInput {
    @Field(() => OptionsCreateWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsCreateWithoutCustomerDishInput)
    create?: InstanceType<typeof OptionsCreateWithoutCustomerDishInput>;
    @Field(() => OptionsCreateOrConnectWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: InstanceType<typeof OptionsCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => OptionsUpsertWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsUpsertWithoutCustomerDishInput)
    upsert?: InstanceType<typeof OptionsUpsertWithoutCustomerDishInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsUpdateWithoutCustomerDishInput, {nullable:true})
    @Type(() => OptionsUpdateWithoutCustomerDishInput)
    update?: InstanceType<typeof OptionsUpdateWithoutCustomerDishInput>;
}

@InputType()
export class OptionsUpdateOneRequiredWithoutToppingsNestedInput {
    @Field(() => OptionsCreateWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsCreateWithoutToppingsInput)
    create?: InstanceType<typeof OptionsCreateWithoutToppingsInput>;
    @Field(() => OptionsCreateOrConnectWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsCreateOrConnectWithoutToppingsInput)
    connectOrCreate?: InstanceType<typeof OptionsCreateOrConnectWithoutToppingsInput>;
    @Field(() => OptionsUpsertWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsUpsertWithoutToppingsInput)
    upsert?: InstanceType<typeof OptionsUpsertWithoutToppingsInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    connect?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsUpdateWithoutToppingsInput, {nullable:true})
    @Type(() => OptionsUpdateWithoutToppingsInput)
    update?: InstanceType<typeof OptionsUpdateWithoutToppingsInput>;
}

@InputType()
export class OptionsUpdateWithWhereUniqueWithoutDishesInput {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsUpdateWithoutDishesInput, {nullable:false})
    @Type(() => OptionsUpdateWithoutDishesInput)
    data!: InstanceType<typeof OptionsUpdateWithoutDishesInput>;
}

@InputType()
export class OptionsUpdateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutOptionsNestedInput>;
    @Field(() => ToppingUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUpdateManyWithoutOptionNestedInput>;
}

@InputType()
export class OptionsUpdateWithoutDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => ToppingUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUpdateManyWithoutOptionNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUpdateWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutOptionsNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => DishUpdateManyWithoutOptionsNestedInput, {nullable:true})
    @Type(() => DishUpdateManyWithoutOptionsNestedInput)
    dishes?: InstanceType<typeof DishUpdateManyWithoutOptionsNestedInput>;
    @Field(() => ToppingUpdateManyWithoutOptionNestedInput, {nullable:true})
    @Type(() => ToppingUpdateManyWithoutOptionNestedInput)
    toppings?: InstanceType<typeof ToppingUpdateManyWithoutOptionNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedOptionNestedInput)
    CustomerDish?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedOptionNestedInput>;
}

@InputType()
export class OptionsUpsertWithWhereUniqueWithoutDishesInput {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsUpdateWithoutDishesInput, {nullable:false})
    @Type(() => OptionsUpdateWithoutDishesInput)
    update!: InstanceType<typeof OptionsUpdateWithoutDishesInput>;
    @Field(() => OptionsCreateWithoutDishesInput, {nullable:false})
    @Type(() => OptionsCreateWithoutDishesInput)
    create!: InstanceType<typeof OptionsCreateWithoutDishesInput>;
}

@InputType()
export class OptionsUpsertWithoutCustomerDishInput {
    @Field(() => OptionsUpdateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsUpdateWithoutCustomerDishInput)
    update!: InstanceType<typeof OptionsUpdateWithoutCustomerDishInput>;
    @Field(() => OptionsCreateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OptionsCreateWithoutCustomerDishInput)
    create!: InstanceType<typeof OptionsCreateWithoutCustomerDishInput>;
}

@InputType()
export class OptionsUpsertWithoutToppingsInput {
    @Field(() => OptionsUpdateWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsUpdateWithoutToppingsInput)
    update!: InstanceType<typeof OptionsUpdateWithoutToppingsInput>;
    @Field(() => OptionsCreateWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsCreateWithoutToppingsInput)
    create!: InstanceType<typeof OptionsCreateWithoutToppingsInput>;
}

@InputType()
export class OptionsWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class OptionsWhereInput {
    @Field(() => [OptionsWhereInput], {nullable:true})
    @Type(() => OptionsWhereInput)
    AND?: Array<OptionsWhereInput>;
    @Field(() => [OptionsWhereInput], {nullable:true})
    @Type(() => OptionsWhereInput)
    OR?: Array<OptionsWhereInput>;
    @Field(() => [OptionsWhereInput], {nullable:true})
    @Type(() => OptionsWhereInput)
    NOT?: Array<OptionsWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => DishListRelationFilter, {nullable:true})
    @Type(() => DishListRelationFilter)
    dishes?: InstanceType<typeof DishListRelationFilter>;
    @Field(() => ToppingListRelationFilter, {nullable:true})
    @Type(() => ToppingListRelationFilter)
    toppings?: InstanceType<typeof ToppingListRelationFilter>;
    @Field(() => CustomerDishListRelationFilter, {nullable:true})
    @Type(() => CustomerDishListRelationFilter)
    CustomerDish?: InstanceType<typeof CustomerDishListRelationFilter>;
}

@ObjectType()
export class Options {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false,defaultValue:''})
    description!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => [Dish], {nullable:true})
    dishes?: Array<Dish>;
    @Field(() => [Topping], {nullable:true})
    toppings?: Array<Topping>;
    @Field(() => [CustomerDish], {nullable:true})
    CustomerDish?: Array<CustomerDish>;
    @Field(() => OptionsCount, {nullable:false})
    _count?: InstanceType<typeof OptionsCount>;
}

@ArgsType()
export class UpdateManyOptionsArgs {
    @Field(() => OptionsUpdateManyMutationInput, {nullable:false})
    @Type(() => OptionsUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OptionsUpdateManyMutationInput>;
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
}

@ArgsType()
export class UpdateOneOptionsArgs {
    @Field(() => OptionsUpdateInput, {nullable:false})
    @Type(() => OptionsUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OptionsUpdateInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneOptionsArgs {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => OptionsCreateInput, {nullable:false})
    @Type(() => OptionsCreateInput)
    create!: InstanceType<typeof OptionsCreateInput>;
    @Field(() => OptionsUpdateInput, {nullable:false})
    @Type(() => OptionsUpdateInput)
    update!: InstanceType<typeof OptionsUpdateInput>;
}

@ObjectType()
export class AggregateOrder {
    @Field(() => OrderCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OrderCountAggregate>;
    @Field(() => OrderAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OrderAvgAggregate>;
    @Field(() => OrderSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OrderSumAggregate>;
    @Field(() => OrderMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OrderMinAggregate>;
    @Field(() => OrderMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OrderMaxAggregate>;
}

@ArgsType()
export class CreateManyOrderArgs {
    @Field(() => [OrderCreateManyInput], {nullable:false})
    @Type(() => OrderCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<OrderCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneOrderArgs {
    @Field(() => OrderCreateInput, {nullable:false})
    @Type(() => OrderCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderCreateInput>;
}

@ArgsType()
export class DeleteManyOrderArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
}

@ArgsType()
export class DeleteOneOrderArgs {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderWhereUniqueInput>;
}

@ArgsType()
export class FindFirstOrderArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
    @Field(() => [OrderOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderOrderByWithRelationInput)
    orderBy?: Array<OrderOrderByWithRelationInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    cursor?: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderScalarFieldEnum>;
}

@ArgsType()
export class FindManyOrderArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
    @Field(() => [OrderOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderOrderByWithRelationInput)
    orderBy?: Array<OrderOrderByWithRelationInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    cursor?: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueOrderArgs {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderWhereUniqueInput>;
}

@ArgsType()
export class OrderAggregateArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
    @Field(() => [OrderOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderOrderByWithRelationInput)
    orderBy?: Array<OrderOrderByWithRelationInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    cursor?: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OrderCountAggregateInput, {nullable:true})
    @Type(() => OrderCountAggregateInput)
    _count?: InstanceType<typeof OrderCountAggregateInput>;
    @Field(() => OrderAvgAggregateInput, {nullable:true})
    @Type(() => OrderAvgAggregateInput)
    _avg?: InstanceType<typeof OrderAvgAggregateInput>;
    @Field(() => OrderSumAggregateInput, {nullable:true})
    @Type(() => OrderSumAggregateInput)
    _sum?: InstanceType<typeof OrderSumAggregateInput>;
    @Field(() => OrderMinAggregateInput, {nullable:true})
    @Type(() => OrderMinAggregateInput)
    _min?: InstanceType<typeof OrderMinAggregateInput>;
    @Field(() => OrderMaxAggregateInput, {nullable:true})
    @Type(() => OrderMaxAggregateInput)
    _max?: InstanceType<typeof OrderMaxAggregateInput>;
}

@InputType()
export class OrderAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    number?: true;
    @Field(() => Boolean, {nullable:true})
    totalPrice?: true;
    @Field(() => Boolean, {nullable:true})
    changeFor?: true;
}

@ObjectType()
export class OrderAvgAggregate {
    @Field(() => Float, {nullable:true})
    number?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    totalPrice?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    changeFor?: Decimal;
}

@InputType()
export class OrderAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
}

@InputType()
export class OrderCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    number?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    status?: true;
    @Field(() => Boolean, {nullable:true})
    orderType?: true;
    @Field(() => Boolean, {nullable:true})
    paymentType?: true;
    @Field(() => Boolean, {nullable:true})
    totalPrice?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    deliveryAddress?: true;
    @Field(() => Boolean, {nullable:true})
    changeFor?: true;
    @Field(() => Boolean, {nullable:true})
    userId?: true;
    @Field(() => Boolean, {nullable:true})
    restaurantId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class OrderCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    number!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    status!: number;
    @Field(() => Int, {nullable:false})
    orderType!: number;
    @Field(() => Int, {nullable:false})
    paymentType!: number;
    @Field(() => Int, {nullable:false})
    totalPrice!: number;
    @Field(() => Int, {nullable:false})
    description!: number;
    @Field(() => Int, {nullable:false})
    deliveryAddress!: number;
    @Field(() => Int, {nullable:false})
    changeFor!: number;
    @Field(() => Int, {nullable:false})
    userId!: number;
    @Field(() => Int, {nullable:false})
    restaurantId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class OrderCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    paymentType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deliveryAddress?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    userId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    restaurantId?: keyof typeof SortOrder;
}

@ObjectType()
export class OrderCount {
    @Field(() => Int, {nullable:false})
    items?: number;
}

@InputType()
export class OrderCreateManyRestaurantInputEnvelope {
    @Field(() => [OrderCreateManyRestaurantInput], {nullable:false})
    @Type(() => OrderCreateManyRestaurantInput)
    data!: Array<OrderCreateManyRestaurantInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OrderCreateManyRestaurantInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
}

@InputType()
export class OrderCreateManyUserInputEnvelope {
    @Field(() => [OrderCreateManyUserInput], {nullable:false})
    @Type(() => OrderCreateManyUserInput)
    data!: Array<OrderCreateManyUserInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OrderCreateManyUserInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
}

@InputType()
export class OrderCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
}

@InputType()
export class OrderCreateNestedManyWithoutRestaurantInput {
    @Field(() => [OrderCreateWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create?: Array<OrderCreateWithoutRestaurantInput>;
    @Field(() => [OrderCreateOrConnectWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutRestaurantInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutRestaurantInput>;
    @Field(() => OrderCreateManyRestaurantInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyRestaurantInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyRestaurantInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
}

@InputType()
export class OrderCreateNestedManyWithoutUserInput {
    @Field(() => [OrderCreateWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateWithoutUserInput)
    create?: Array<OrderCreateWithoutUserInput>;
    @Field(() => [OrderCreateOrConnectWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutUserInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutUserInput>;
    @Field(() => OrderCreateManyUserInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyUserInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyUserInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
}

@InputType()
export class OrderCreateNestedOneWithoutItemsInput {
    @Field(() => OrderCreateWithoutItemsInput, {nullable:true})
    @Type(() => OrderCreateWithoutItemsInput)
    create?: InstanceType<typeof OrderCreateWithoutItemsInput>;
    @Field(() => OrderCreateOrConnectWithoutItemsInput, {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutItemsInput)
    connectOrCreate?: InstanceType<typeof OrderCreateOrConnectWithoutItemsInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: InstanceType<typeof OrderWhereUniqueInput>;
}

@InputType()
export class OrderCreateOrConnectWithoutItemsInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderCreateWithoutItemsInput, {nullable:false})
    @Type(() => OrderCreateWithoutItemsInput)
    create!: InstanceType<typeof OrderCreateWithoutItemsInput>;
}

@InputType()
export class OrderCreateOrConnectWithoutRestaurantInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderCreateWithoutRestaurantInput, {nullable:false})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create!: InstanceType<typeof OrderCreateWithoutRestaurantInput>;
}

@InputType()
export class OrderCreateOrConnectWithoutUserInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderCreateWithoutUserInput, {nullable:false})
    @Type(() => OrderCreateWithoutUserInput)
    create!: InstanceType<typeof OrderCreateWithoutUserInput>;
}

@InputType()
export class OrderCreateWithoutItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => UserCreateNestedOneWithoutOrdersInput, {nullable:false})
    user!: InstanceType<typeof UserCreateNestedOneWithoutOrdersInput>;
    @Field(() => RestaurantCreateNestedOneWithoutOrderInput, {nullable:false})
    restaurant!: InstanceType<typeof RestaurantCreateNestedOneWithoutOrderInput>;
}

@InputType()
export class OrderCreateWithoutRestaurantInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemCreateNestedManyWithoutOrderInput>;
    @Field(() => UserCreateNestedOneWithoutOrdersInput, {nullable:false})
    user!: InstanceType<typeof UserCreateNestedOneWithoutOrdersInput>;
}

@InputType()
export class OrderCreateWithoutUserInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemCreateNestedManyWithoutOrderInput>;
    @Field(() => RestaurantCreateNestedOneWithoutOrderInput, {nullable:false})
    restaurant!: InstanceType<typeof RestaurantCreateNestedOneWithoutOrderInput>;
}

@InputType()
export class OrderCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemCreateNestedManyWithoutOrderInput>;
    @Field(() => UserCreateNestedOneWithoutOrdersInput, {nullable:false})
    user!: InstanceType<typeof UserCreateNestedOneWithoutOrdersInput>;
    @Field(() => RestaurantCreateNestedOneWithoutOrderInput, {nullable:false})
    restaurant!: InstanceType<typeof RestaurantCreateNestedOneWithoutOrderInput>;
}

@ArgsType()
export class OrderGroupByArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
    @Field(() => [OrderOrderByWithAggregationInput], {nullable:true})
    @Type(() => OrderOrderByWithAggregationInput)
    orderBy?: Array<OrderOrderByWithAggregationInput>;
    @Field(() => [OrderScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof OrderScalarFieldEnum>;
    @Field(() => OrderScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => OrderScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof OrderScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OrderCountAggregateInput, {nullable:true})
    @Type(() => OrderCountAggregateInput)
    _count?: InstanceType<typeof OrderCountAggregateInput>;
    @Field(() => OrderAvgAggregateInput, {nullable:true})
    @Type(() => OrderAvgAggregateInput)
    _avg?: InstanceType<typeof OrderAvgAggregateInput>;
    @Field(() => OrderSumAggregateInput, {nullable:true})
    @Type(() => OrderSumAggregateInput)
    _sum?: InstanceType<typeof OrderSumAggregateInput>;
    @Field(() => OrderMinAggregateInput, {nullable:true})
    @Type(() => OrderMinAggregateInput)
    _min?: InstanceType<typeof OrderMinAggregateInput>;
    @Field(() => OrderMaxAggregateInput, {nullable:true})
    @Type(() => OrderMaxAggregateInput)
    _max?: InstanceType<typeof OrderMaxAggregateInput>;
}

@ObjectType()
export class OrderGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Int, {nullable:false})
    number!: number;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => EnumOrderStatus, {nullable:false})
    @Validator.IsEnum(EnumOrderStatus)
    status!: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:false})
    @Validator.IsEnum(EnumOrderType)
    orderType!: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:false})
    @Validator.IsEnum(EnumPaymentType)
    paymentType!: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    totalPrice!: Decimal;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress!: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    changeFor!: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
    @Field(() => OrderCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OrderCountAggregate>;
    @Field(() => OrderAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OrderAvgAggregate>;
    @Field(() => OrderSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OrderSumAggregate>;
    @Field(() => OrderMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OrderMinAggregate>;
    @Field(() => OrderMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OrderMaxAggregate>;
}

@InputType()
export class OrderListRelationFilter {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    every?: InstanceType<typeof OrderWhereInput>;
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    some?: InstanceType<typeof OrderWhereInput>;
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    none?: InstanceType<typeof OrderWhereInput>;
}

@InputType()
export class OrderMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    number?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    status?: true;
    @Field(() => Boolean, {nullable:true})
    orderType?: true;
    @Field(() => Boolean, {nullable:true})
    paymentType?: true;
    @Field(() => Boolean, {nullable:true})
    totalPrice?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    deliveryAddress?: true;
    @Field(() => Boolean, {nullable:true})
    changeFor?: true;
    @Field(() => Boolean, {nullable:true})
    userId?: true;
    @Field(() => Boolean, {nullable:true})
    restaurantId?: true;
}

@ObjectType()
export class OrderMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
}

@InputType()
export class OrderMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    paymentType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deliveryAddress?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    userId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    restaurantId?: keyof typeof SortOrder;
}

@InputType()
export class OrderMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @Field(() => Boolean, {nullable:true})
    number?: true;
    @Field(() => Boolean, {nullable:true})
    createdAt?: true;
    @Field(() => Boolean, {nullable:true})
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    status?: true;
    @Field(() => Boolean, {nullable:true})
    orderType?: true;
    @Field(() => Boolean, {nullable:true})
    paymentType?: true;
    @Field(() => Boolean, {nullable:true})
    totalPrice?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    deliveryAddress?: true;
    @Field(() => Boolean, {nullable:true})
    changeFor?: true;
    @Field(() => Boolean, {nullable:true})
    userId?: true;
    @Field(() => Boolean, {nullable:true})
    restaurantId?: true;
}

@ObjectType()
export class OrderMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
}

@InputType()
export class OrderMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    paymentType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deliveryAddress?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    userId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    restaurantId?: keyof typeof SortOrder;
}

@InputType()
export class OrderOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class OrderOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    paymentType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deliveryAddress?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    userId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    restaurantId?: keyof typeof SortOrder;
    @Field(() => OrderCountOrderByAggregateInput, {nullable:true})
    @Type(() => OrderCountOrderByAggregateInput)
    _count?: InstanceType<typeof OrderCountOrderByAggregateInput>;
    @Field(() => OrderAvgOrderByAggregateInput, {nullable:true})
    @Type(() => OrderAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof OrderAvgOrderByAggregateInput>;
    @Field(() => OrderMaxOrderByAggregateInput, {nullable:true})
    @Type(() => OrderMaxOrderByAggregateInput)
    _max?: InstanceType<typeof OrderMaxOrderByAggregateInput>;
    @Field(() => OrderMinOrderByAggregateInput, {nullable:true})
    @Type(() => OrderMinOrderByAggregateInput)
    _min?: InstanceType<typeof OrderMinOrderByAggregateInput>;
    @Field(() => OrderSumOrderByAggregateInput, {nullable:true})
    @Type(() => OrderSumOrderByAggregateInput)
    _sum?: InstanceType<typeof OrderSumOrderByAggregateInput>;
}

@InputType()
export class OrderOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    createdAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    status?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    paymentType?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    deliveryAddress?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    userId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    restaurantId?: keyof typeof SortOrder;
    @Field(() => OrderItemOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OrderItemOrderByRelationAggregateInput)
    items?: InstanceType<typeof OrderItemOrderByRelationAggregateInput>;
    @Field(() => UserOrderByWithRelationInput, {nullable:true})
    user?: InstanceType<typeof UserOrderByWithRelationInput>;
    @Field(() => RestaurantOrderByWithRelationInput, {nullable:true})
    restaurant?: InstanceType<typeof RestaurantOrderByWithRelationInput>;
}

@InputType()
export class OrderRelationFilter {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    is?: InstanceType<typeof OrderWhereInput>;
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    isNot?: InstanceType<typeof OrderWhereInput>;
}

@InputType()
export class OrderScalarWhereWithAggregatesInput {
    @Field(() => [OrderScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderScalarWhereWithAggregatesInput)
    AND?: Array<OrderScalarWhereWithAggregatesInput>;
    @Field(() => [OrderScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderScalarWhereWithAggregatesInput)
    OR?: Array<OrderScalarWhereWithAggregatesInput>;
    @Field(() => [OrderScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderScalarWhereWithAggregatesInput)
    NOT?: Array<OrderScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => IntWithAggregatesFilter, {nullable:true})
    number?: InstanceType<typeof IntWithAggregatesFilter>;
    @Field(() => DateTimeWithAggregatesFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => DateTimeWithAggregatesFilter, {nullable:true})
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => EnumEnumOrderStatusWithAggregatesFilter, {nullable:true})
    status?: InstanceType<typeof EnumEnumOrderStatusWithAggregatesFilter>;
    @Field(() => EnumEnumOrderTypeWithAggregatesFilter, {nullable:true})
    orderType?: InstanceType<typeof EnumEnumOrderTypeWithAggregatesFilter>;
    @Field(() => EnumEnumPaymentTypeWithAggregatesFilter, {nullable:true})
    paymentType?: InstanceType<typeof EnumEnumPaymentTypeWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    totalPrice?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    description?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    deliveryAddress?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    changeFor?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    userId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    restaurantId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class OrderScalarWhereInput {
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    AND?: Array<OrderScalarWhereInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    OR?: Array<OrderScalarWhereInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    NOT?: Array<OrderScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @Field(() => IntFilter, {nullable:true})
    number?: InstanceType<typeof IntFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => EnumEnumOrderStatusFilter, {nullable:true})
    status?: InstanceType<typeof EnumEnumOrderStatusFilter>;
    @Field(() => EnumEnumOrderTypeFilter, {nullable:true})
    orderType?: InstanceType<typeof EnumEnumOrderTypeFilter>;
    @Field(() => EnumEnumPaymentTypeFilter, {nullable:true})
    paymentType?: InstanceType<typeof EnumEnumPaymentTypeFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    totalPrice?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    deliveryAddress?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    changeFor?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    userId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    restaurantId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class OrderSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    number?: true;
    @Field(() => Boolean, {nullable:true})
    totalPrice?: true;
    @Field(() => Boolean, {nullable:true})
    changeFor?: true;
}

@ObjectType()
export class OrderSumAggregate {
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    totalPrice?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    changeFor?: Decimal;
}

@InputType()
export class OrderSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    number?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    totalPrice?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    changeFor?: keyof typeof SortOrder;
}

@InputType()
export class OrderUncheckedCreateNestedManyWithoutRestaurantInput {
    @Field(() => [OrderCreateWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create?: Array<OrderCreateWithoutRestaurantInput>;
    @Field(() => [OrderCreateOrConnectWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutRestaurantInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutRestaurantInput>;
    @Field(() => OrderCreateManyRestaurantInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyRestaurantInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyRestaurantInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
}

@InputType()
export class OrderUncheckedCreateNestedManyWithoutUserInput {
    @Field(() => [OrderCreateWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateWithoutUserInput)
    create?: Array<OrderCreateWithoutUserInput>;
    @Field(() => [OrderCreateOrConnectWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutUserInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutUserInput>;
    @Field(() => OrderCreateManyUserInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyUserInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyUserInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
}

@InputType()
export class OrderUncheckedCreateWithoutItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
}

@InputType()
export class OrderUncheckedCreateWithoutRestaurantInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutOrderInput>;
}

@InputType()
export class OrderUncheckedCreateWithoutUserInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutOrderInput>;
}

@InputType()
export class OrderUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice!: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
    @Field(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput, {nullable:true})
    @Type(() => OrderItemUncheckedCreateNestedManyWithoutOrderInput)
    items?: InstanceType<typeof OrderItemUncheckedCreateNestedManyWithoutOrderInput>;
}

@InputType()
export class OrderUncheckedUpdateManyWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
}

@InputType()
export class OrderUncheckedUpdateManyWithoutOrdersInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
}

@InputType()
export class OrderUncheckedUpdateManyWithoutRestaurantNestedInput {
    @Field(() => [OrderCreateWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create?: Array<OrderCreateWithoutRestaurantInput>;
    @Field(() => [OrderCreateOrConnectWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutRestaurantInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutRestaurantInput>;
    @Field(() => [OrderUpsertWithWhereUniqueWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpsertWithWhereUniqueWithoutRestaurantInput)
    upsert?: Array<OrderUpsertWithWhereUniqueWithoutRestaurantInput>;
    @Field(() => OrderCreateManyRestaurantInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyRestaurantInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyRestaurantInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    set?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    disconnect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    delete?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderUpdateWithWhereUniqueWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpdateWithWhereUniqueWithoutRestaurantInput)
    update?: Array<OrderUpdateWithWhereUniqueWithoutRestaurantInput>;
    @Field(() => [OrderUpdateManyWithWhereWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpdateManyWithWhereWithoutRestaurantInput)
    updateMany?: Array<OrderUpdateManyWithWhereWithoutRestaurantInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    deleteMany?: Array<OrderScalarWhereInput>;
}

@InputType()
export class OrderUncheckedUpdateManyWithoutUserNestedInput {
    @Field(() => [OrderCreateWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateWithoutUserInput)
    create?: Array<OrderCreateWithoutUserInput>;
    @Field(() => [OrderCreateOrConnectWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutUserInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutUserInput>;
    @Field(() => [OrderUpsertWithWhereUniqueWithoutUserInput], {nullable:true})
    @Type(() => OrderUpsertWithWhereUniqueWithoutUserInput)
    upsert?: Array<OrderUpsertWithWhereUniqueWithoutUserInput>;
    @Field(() => OrderCreateManyUserInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyUserInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyUserInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    set?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    disconnect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    delete?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderUpdateWithWhereUniqueWithoutUserInput], {nullable:true})
    @Type(() => OrderUpdateWithWhereUniqueWithoutUserInput)
    update?: Array<OrderUpdateWithWhereUniqueWithoutUserInput>;
    @Field(() => [OrderUpdateManyWithWhereWithoutUserInput], {nullable:true})
    @Type(() => OrderUpdateManyWithWhereWithoutUserInput)
    updateMany?: Array<OrderUpdateManyWithWhereWithoutUserInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    deleteMany?: Array<OrderScalarWhereInput>;
}

@InputType()
export class OrderUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
}

@InputType()
export class OrderUncheckedUpdateWithoutItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
}

@InputType()
export class OrderUncheckedUpdateWithoutRestaurantInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutOrderNestedInput>;
}

@InputType()
export class OrderUncheckedUpdateWithoutUserInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
    @Field(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutOrderNestedInput>;
}

@InputType()
export class OrderUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => String, {nullable:true})
    userId?: string;
    @Field(() => String, {nullable:true})
    restaurantId?: string;
    @Field(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUncheckedUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUncheckedUpdateManyWithoutOrderNestedInput>;
}

@InputType()
export class OrderUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
}

@InputType()
export class OrderUpdateManyWithWhereWithoutRestaurantInput {
    @Field(() => OrderScalarWhereInput, {nullable:false})
    @Type(() => OrderScalarWhereInput)
    where!: InstanceType<typeof OrderScalarWhereInput>;
    @Field(() => OrderUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderUpdateManyMutationInput)
    data!: InstanceType<typeof OrderUpdateManyMutationInput>;
}

@InputType()
export class OrderUpdateManyWithWhereWithoutUserInput {
    @Field(() => OrderScalarWhereInput, {nullable:false})
    @Type(() => OrderScalarWhereInput)
    where!: InstanceType<typeof OrderScalarWhereInput>;
    @Field(() => OrderUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderUpdateManyMutationInput)
    data!: InstanceType<typeof OrderUpdateManyMutationInput>;
}

@InputType()
export class OrderUpdateManyWithoutRestaurantNestedInput {
    @Field(() => [OrderCreateWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create?: Array<OrderCreateWithoutRestaurantInput>;
    @Field(() => [OrderCreateOrConnectWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutRestaurantInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutRestaurantInput>;
    @Field(() => [OrderUpsertWithWhereUniqueWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpsertWithWhereUniqueWithoutRestaurantInput)
    upsert?: Array<OrderUpsertWithWhereUniqueWithoutRestaurantInput>;
    @Field(() => OrderCreateManyRestaurantInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyRestaurantInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyRestaurantInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    set?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    disconnect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    delete?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderUpdateWithWhereUniqueWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpdateWithWhereUniqueWithoutRestaurantInput)
    update?: Array<OrderUpdateWithWhereUniqueWithoutRestaurantInput>;
    @Field(() => [OrderUpdateManyWithWhereWithoutRestaurantInput], {nullable:true})
    @Type(() => OrderUpdateManyWithWhereWithoutRestaurantInput)
    updateMany?: Array<OrderUpdateManyWithWhereWithoutRestaurantInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    deleteMany?: Array<OrderScalarWhereInput>;
}

@InputType()
export class OrderUpdateManyWithoutUserNestedInput {
    @Field(() => [OrderCreateWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateWithoutUserInput)
    create?: Array<OrderCreateWithoutUserInput>;
    @Field(() => [OrderCreateOrConnectWithoutUserInput], {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutUserInput)
    connectOrCreate?: Array<OrderCreateOrConnectWithoutUserInput>;
    @Field(() => [OrderUpsertWithWhereUniqueWithoutUserInput], {nullable:true})
    @Type(() => OrderUpsertWithWhereUniqueWithoutUserInput)
    upsert?: Array<OrderUpsertWithWhereUniqueWithoutUserInput>;
    @Field(() => OrderCreateManyUserInputEnvelope, {nullable:true})
    @Type(() => OrderCreateManyUserInputEnvelope)
    createMany?: InstanceType<typeof OrderCreateManyUserInputEnvelope>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    set?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    disconnect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    delete?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderWhereUniqueInput], {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: Array<OrderWhereUniqueInput>;
    @Field(() => [OrderUpdateWithWhereUniqueWithoutUserInput], {nullable:true})
    @Type(() => OrderUpdateWithWhereUniqueWithoutUserInput)
    update?: Array<OrderUpdateWithWhereUniqueWithoutUserInput>;
    @Field(() => [OrderUpdateManyWithWhereWithoutUserInput], {nullable:true})
    @Type(() => OrderUpdateManyWithWhereWithoutUserInput)
    updateMany?: Array<OrderUpdateManyWithWhereWithoutUserInput>;
    @Field(() => [OrderScalarWhereInput], {nullable:true})
    @Type(() => OrderScalarWhereInput)
    deleteMany?: Array<OrderScalarWhereInput>;
}

@InputType()
export class OrderUpdateOneRequiredWithoutItemsNestedInput {
    @Field(() => OrderCreateWithoutItemsInput, {nullable:true})
    @Type(() => OrderCreateWithoutItemsInput)
    create?: InstanceType<typeof OrderCreateWithoutItemsInput>;
    @Field(() => OrderCreateOrConnectWithoutItemsInput, {nullable:true})
    @Type(() => OrderCreateOrConnectWithoutItemsInput)
    connectOrCreate?: InstanceType<typeof OrderCreateOrConnectWithoutItemsInput>;
    @Field(() => OrderUpsertWithoutItemsInput, {nullable:true})
    @Type(() => OrderUpsertWithoutItemsInput)
    upsert?: InstanceType<typeof OrderUpsertWithoutItemsInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    connect?: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderUpdateWithoutItemsInput, {nullable:true})
    @Type(() => OrderUpdateWithoutItemsInput)
    update?: InstanceType<typeof OrderUpdateWithoutItemsInput>;
}

@InputType()
export class OrderUpdateWithWhereUniqueWithoutRestaurantInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderUpdateWithoutRestaurantInput, {nullable:false})
    @Type(() => OrderUpdateWithoutRestaurantInput)
    data!: InstanceType<typeof OrderUpdateWithoutRestaurantInput>;
}

@InputType()
export class OrderUpdateWithWhereUniqueWithoutUserInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderUpdateWithoutUserInput, {nullable:false})
    @Type(() => OrderUpdateWithoutUserInput)
    data!: InstanceType<typeof OrderUpdateWithoutUserInput>;
}

@InputType()
export class OrderUpdateWithoutItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => UserUpdateOneRequiredWithoutOrdersNestedInput, {nullable:true})
    user?: InstanceType<typeof UserUpdateOneRequiredWithoutOrdersNestedInput>;
    @Field(() => RestaurantUpdateOneRequiredWithoutOrderNestedInput, {nullable:true})
    restaurant?: InstanceType<typeof RestaurantUpdateOneRequiredWithoutOrderNestedInput>;
}

@InputType()
export class OrderUpdateWithoutRestaurantInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUpdateManyWithoutOrderNestedInput>;
    @Field(() => UserUpdateOneRequiredWithoutOrdersNestedInput, {nullable:true})
    user?: InstanceType<typeof UserUpdateOneRequiredWithoutOrdersNestedInput>;
}

@InputType()
export class OrderUpdateWithoutUserInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUpdateManyWithoutOrderNestedInput>;
    @Field(() => RestaurantUpdateOneRequiredWithoutOrderNestedInput, {nullable:true})
    restaurant?: InstanceType<typeof RestaurantUpdateOneRequiredWithoutOrderNestedInput>;
}

@InputType()
export class OrderUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => EnumOrderStatus, {nullable:true})
    @Validator.IsEnum(EnumOrderStatus)
    status?: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:true})
    @Validator.IsEnum(EnumOrderType)
    orderType?: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:true})
    @Validator.IsEnum(EnumPaymentType)
    paymentType?: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    totalPrice?: Decimal;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    deliveryAddress?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    changeFor?: Decimal;
    @Field(() => OrderItemUpdateManyWithoutOrderNestedInput, {nullable:true})
    @Type(() => OrderItemUpdateManyWithoutOrderNestedInput)
    items?: InstanceType<typeof OrderItemUpdateManyWithoutOrderNestedInput>;
    @Field(() => UserUpdateOneRequiredWithoutOrdersNestedInput, {nullable:true})
    user?: InstanceType<typeof UserUpdateOneRequiredWithoutOrdersNestedInput>;
    @Field(() => RestaurantUpdateOneRequiredWithoutOrderNestedInput, {nullable:true})
    restaurant?: InstanceType<typeof RestaurantUpdateOneRequiredWithoutOrderNestedInput>;
}

@InputType()
export class OrderUpsertWithWhereUniqueWithoutRestaurantInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderUpdateWithoutRestaurantInput, {nullable:false})
    @Type(() => OrderUpdateWithoutRestaurantInput)
    update!: InstanceType<typeof OrderUpdateWithoutRestaurantInput>;
    @Field(() => OrderCreateWithoutRestaurantInput, {nullable:false})
    @Type(() => OrderCreateWithoutRestaurantInput)
    create!: InstanceType<typeof OrderCreateWithoutRestaurantInput>;
}

@InputType()
export class OrderUpsertWithWhereUniqueWithoutUserInput {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderUpdateWithoutUserInput, {nullable:false})
    @Type(() => OrderUpdateWithoutUserInput)
    update!: InstanceType<typeof OrderUpdateWithoutUserInput>;
    @Field(() => OrderCreateWithoutUserInput, {nullable:false})
    @Type(() => OrderCreateWithoutUserInput)
    create!: InstanceType<typeof OrderCreateWithoutUserInput>;
}

@InputType()
export class OrderUpsertWithoutItemsInput {
    @Field(() => OrderUpdateWithoutItemsInput, {nullable:false})
    @Type(() => OrderUpdateWithoutItemsInput)
    update!: InstanceType<typeof OrderUpdateWithoutItemsInput>;
    @Field(() => OrderCreateWithoutItemsInput, {nullable:false})
    @Type(() => OrderCreateWithoutItemsInput)
    create!: InstanceType<typeof OrderCreateWithoutItemsInput>;
}

@InputType()
export class OrderWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Int, {nullable:true})
    number?: number;
}

@InputType()
export class OrderWhereInput {
    @Field(() => [OrderWhereInput], {nullable:true})
    @Type(() => OrderWhereInput)
    AND?: Array<OrderWhereInput>;
    @Field(() => [OrderWhereInput], {nullable:true})
    @Type(() => OrderWhereInput)
    OR?: Array<OrderWhereInput>;
    @Field(() => [OrderWhereInput], {nullable:true})
    @Type(() => OrderWhereInput)
    NOT?: Array<OrderWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @Field(() => IntFilter, {nullable:true})
    number?: InstanceType<typeof IntFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => DateTimeFilter, {nullable:true})
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => EnumEnumOrderStatusFilter, {nullable:true})
    status?: InstanceType<typeof EnumEnumOrderStatusFilter>;
    @Field(() => EnumEnumOrderTypeFilter, {nullable:true})
    orderType?: InstanceType<typeof EnumEnumOrderTypeFilter>;
    @Field(() => EnumEnumPaymentTypeFilter, {nullable:true})
    paymentType?: InstanceType<typeof EnumEnumPaymentTypeFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    totalPrice?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    description?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    deliveryAddress?: InstanceType<typeof StringFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    changeFor?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    userId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    restaurantId?: InstanceType<typeof StringFilter>;
    @Field(() => OrderItemListRelationFilter, {nullable:true})
    @Type(() => OrderItemListRelationFilter)
    items?: InstanceType<typeof OrderItemListRelationFilter>;
    @Field(() => UserRelationFilter, {nullable:true})
    user?: InstanceType<typeof UserRelationFilter>;
    @Field(() => RestaurantRelationFilter, {nullable:true})
    restaurant?: InstanceType<typeof RestaurantRelationFilter>;
}

@ObjectType()
export class Order {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Int, {nullable:false})
    number!: number;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => EnumOrderStatus, {nullable:false,defaultValue:'PENDING'})
    status!: keyof typeof EnumOrderStatus;
    @Field(() => EnumOrderType, {nullable:false,defaultValue:'DELIVERY'})
    orderType!: keyof typeof EnumOrderType;
    @Field(() => EnumPaymentType, {nullable:false,defaultValue:'CASH'})
    paymentType!: keyof typeof EnumPaymentType;
    @Field(() => GraphQLDecimal, {nullable:false})
    totalPrice!: Decimal;
    @Field(() => String, {nullable:false,defaultValue:''})
    description!: string;
    @Field(() => String, {nullable:false,defaultValue:''})
    deliveryAddress!: string;
    @Field(() => GraphQLDecimal, {nullable:false,defaultValue:0})
    changeFor!: Decimal;
    @Field(() => String, {nullable:false})
    userId!: string;
    @Field(() => String, {nullable:false})
    restaurantId!: string;
    @Field(() => [OrderItem], {nullable:true})
    items?: Array<OrderItem>;
    @Field(() => User, {nullable:false})
    user?: InstanceType<typeof User>;
    @Field(() => Restaurant, {nullable:false})
    restaurant?: InstanceType<typeof Restaurant>;
    @Field(() => OrderCount, {nullable:false})
    _count?: InstanceType<typeof OrderCount>;
}

@ArgsType()
export class UpdateManyOrderArgs {
    @Field(() => OrderUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderUpdateManyMutationInput>;
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
}

@ArgsType()
export class UpdateOneOrderArgs {
    @Field(() => OrderUpdateInput, {nullable:false})
    @Type(() => OrderUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderUpdateInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneOrderArgs {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => OrderCreateInput, {nullable:false})
    @Type(() => OrderCreateInput)
    create!: InstanceType<typeof OrderCreateInput>;
    @Field(() => OrderUpdateInput, {nullable:false})
    @Type(() => OrderUpdateInput)
    update!: InstanceType<typeof OrderUpdateInput>;
}

@ObjectType()
export class AggregateOrderItem {
    @Field(() => OrderItemCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OrderItemCountAggregate>;
    @Field(() => OrderItemAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OrderItemAvgAggregate>;
    @Field(() => OrderItemSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OrderItemSumAggregate>;
    @Field(() => OrderItemMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OrderItemMinAggregate>;
    @Field(() => OrderItemMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OrderItemMaxAggregate>;
}

@ArgsType()
export class CreateManyOrderItemArgs {
    @Field(() => [OrderItemCreateManyInput], {nullable:false})
    @Type(() => OrderItemCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<OrderItemCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneOrderItemArgs {
    @Field(() => OrderItemCreateInput, {nullable:false})
    @Type(() => OrderItemCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderItemCreateInput>;
}

@ArgsType()
export class DeleteManyOrderItemArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
}

@ArgsType()
export class DeleteOneOrderItemArgs {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
}

@ArgsType()
export class FindFirstOrderItemArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => [OrderItemOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderItemOrderByWithRelationInput)
    orderBy?: Array<OrderItemOrderByWithRelationInput>;
    @Field(() => OrderItemWhereUniqueInput, {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    cursor?: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderItemScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderItemScalarFieldEnum>;
}

@ArgsType()
export class FindManyOrderItemArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => [OrderItemOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderItemOrderByWithRelationInput)
    orderBy?: Array<OrderItemOrderByWithRelationInput>;
    @Field(() => OrderItemWhereUniqueInput, {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    cursor?: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderItemScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderItemScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueOrderItemArgs {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
}

@ArgsType()
export class OrderItemAggregateArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => [OrderItemOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderItemOrderByWithRelationInput)
    orderBy?: Array<OrderItemOrderByWithRelationInput>;
    @Field(() => OrderItemWhereUniqueInput, {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    cursor?: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OrderItemCountAggregateInput, {nullable:true})
    @Type(() => OrderItemCountAggregateInput)
    _count?: InstanceType<typeof OrderItemCountAggregateInput>;
    @Field(() => OrderItemAvgAggregateInput, {nullable:true})
    @Type(() => OrderItemAvgAggregateInput)
    _avg?: InstanceType<typeof OrderItemAvgAggregateInput>;
    @Field(() => OrderItemSumAggregateInput, {nullable:true})
    @Type(() => OrderItemSumAggregateInput)
    _sum?: InstanceType<typeof OrderItemSumAggregateInput>;
    @Field(() => OrderItemMinAggregateInput, {nullable:true})
    @Type(() => OrderItemMinAggregateInput)
    _min?: InstanceType<typeof OrderItemMinAggregateInput>;
    @Field(() => OrderItemMaxAggregateInput, {nullable:true})
    @Type(() => OrderItemMaxAggregateInput)
    _max?: InstanceType<typeof OrderItemMaxAggregateInput>;
}

@InputType()
export class OrderItemAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    quantity?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OrderItemAvgAggregate {
    @Field(() => Float, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OrderItemAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    quantity?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    orderId?: true;
    @Field(() => Boolean, {nullable:true})
    goodId?: true;
    @Field(() => Boolean, {nullable:true})
    customerDishId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class OrderItemCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    orderId!: number;
    @Field(() => Int, {nullable:false})
    goodId!: number;
    @Field(() => Int, {nullable:false})
    customerDishId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class OrderItemCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    goodId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    customerDishId?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemCreateManyCustomerDishInputEnvelope {
    @Field(() => [OrderItemCreateManyCustomerDishInput], {nullable:false})
    @Type(() => OrderItemCreateManyCustomerDishInput)
    data!: Array<OrderItemCreateManyCustomerDishInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OrderItemCreateManyCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
}

@InputType()
export class OrderItemCreateManyGoodInputEnvelope {
    @Field(() => [OrderItemCreateManyGoodInput], {nullable:false})
    @Type(() => OrderItemCreateManyGoodInput)
    data!: Array<OrderItemCreateManyGoodInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OrderItemCreateManyGoodInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemCreateManyOrderInputEnvelope {
    @Field(() => [OrderItemCreateManyOrderInput], {nullable:false})
    @Type(() => OrderItemCreateManyOrderInput)
    data!: Array<OrderItemCreateManyOrderInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class OrderItemCreateManyOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemCreateNestedManyWithoutCustomerDishInput {
    @Field(() => [OrderItemCreateWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create?: Array<OrderItemCreateWithoutCustomerDishInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateManyCustomerDishInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyCustomerDishInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyCustomerDishInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemCreateNestedManyWithoutGoodInput {
    @Field(() => [OrderItemCreateWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create?: Array<OrderItemCreateWithoutGoodInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutGoodInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutGoodInput>;
    @Field(() => OrderItemCreateManyGoodInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyGoodInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyGoodInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemCreateNestedManyWithoutOrderInput {
    @Field(() => [OrderItemCreateWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create?: Array<OrderItemCreateWithoutOrderInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutOrderInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutOrderInput>;
    @Field(() => OrderItemCreateManyOrderInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyOrderInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyOrderInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemCreateOrConnectWithoutCustomerDishInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemCreateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create!: InstanceType<typeof OrderItemCreateWithoutCustomerDishInput>;
}

@InputType()
export class OrderItemCreateOrConnectWithoutGoodInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemCreateWithoutGoodInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create!: InstanceType<typeof OrderItemCreateWithoutGoodInput>;
}

@InputType()
export class OrderItemCreateOrConnectWithoutOrderInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemCreateWithoutOrderInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create!: InstanceType<typeof OrderItemCreateWithoutOrderInput>;
}

@InputType()
export class OrderItemCreateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OrderCreateNestedOneWithoutItemsInput, {nullable:false})
    @Type(() => OrderCreateNestedOneWithoutItemsInput)
    order!: InstanceType<typeof OrderCreateNestedOneWithoutItemsInput>;
    @Field(() => GoodCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateNestedOneWithoutOrderItemsInput)
    good?: InstanceType<typeof GoodCreateNestedOneWithoutOrderItemsInput>;
}

@InputType()
export class OrderItemCreateWithoutGoodInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OrderCreateNestedOneWithoutItemsInput, {nullable:false})
    @Type(() => OrderCreateNestedOneWithoutItemsInput)
    order!: InstanceType<typeof OrderCreateNestedOneWithoutItemsInput>;
    @Field(() => CustomerDishCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedOneWithoutOrderItemsInput)
    customerDish?: InstanceType<typeof CustomerDishCreateNestedOneWithoutOrderItemsInput>;
}

@InputType()
export class OrderItemCreateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => GoodCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateNestedOneWithoutOrderItemsInput)
    good?: InstanceType<typeof GoodCreateNestedOneWithoutOrderItemsInput>;
    @Field(() => CustomerDishCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedOneWithoutOrderItemsInput)
    customerDish?: InstanceType<typeof CustomerDishCreateNestedOneWithoutOrderItemsInput>;
}

@InputType()
export class OrderItemCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OrderCreateNestedOneWithoutItemsInput, {nullable:false})
    @Type(() => OrderCreateNestedOneWithoutItemsInput)
    order!: InstanceType<typeof OrderCreateNestedOneWithoutItemsInput>;
    @Field(() => GoodCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => GoodCreateNestedOneWithoutOrderItemsInput)
    good?: InstanceType<typeof GoodCreateNestedOneWithoutOrderItemsInput>;
    @Field(() => CustomerDishCreateNestedOneWithoutOrderItemsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedOneWithoutOrderItemsInput)
    customerDish?: InstanceType<typeof CustomerDishCreateNestedOneWithoutOrderItemsInput>;
}

@ArgsType()
export class OrderItemGroupByArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => [OrderItemOrderByWithAggregationInput], {nullable:true})
    @Type(() => OrderItemOrderByWithAggregationInput)
    orderBy?: Array<OrderItemOrderByWithAggregationInput>;
    @Field(() => [OrderItemScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof OrderItemScalarFieldEnum>;
    @Field(() => OrderItemScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => OrderItemScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof OrderItemScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => OrderItemCountAggregateInput, {nullable:true})
    @Type(() => OrderItemCountAggregateInput)
    _count?: InstanceType<typeof OrderItemCountAggregateInput>;
    @Field(() => OrderItemAvgAggregateInput, {nullable:true})
    @Type(() => OrderItemAvgAggregateInput)
    _avg?: InstanceType<typeof OrderItemAvgAggregateInput>;
    @Field(() => OrderItemSumAggregateInput, {nullable:true})
    @Type(() => OrderItemSumAggregateInput)
    _sum?: InstanceType<typeof OrderItemSumAggregateInput>;
    @Field(() => OrderItemMinAggregateInput, {nullable:true})
    @Type(() => OrderItemMinAggregateInput)
    _min?: InstanceType<typeof OrderItemMinAggregateInput>;
    @Field(() => OrderItemMaxAggregateInput, {nullable:true})
    @Type(() => OrderItemMaxAggregateInput)
    _max?: InstanceType<typeof OrderItemMaxAggregateInput>;
}

@ObjectType()
export class OrderItemGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
    @Field(() => OrderItemCountAggregate, {nullable:true})
    _count?: InstanceType<typeof OrderItemCountAggregate>;
    @Field(() => OrderItemAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof OrderItemAvgAggregate>;
    @Field(() => OrderItemSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof OrderItemSumAggregate>;
    @Field(() => OrderItemMinAggregate, {nullable:true})
    _min?: InstanceType<typeof OrderItemMinAggregate>;
    @Field(() => OrderItemMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof OrderItemMaxAggregate>;
}

@InputType()
export class OrderItemListRelationFilter {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    every?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    some?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    none?: InstanceType<typeof OrderItemWhereInput>;
}

@InputType()
export class OrderItemMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    quantity?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    orderId?: true;
    @Field(() => Boolean, {nullable:true})
    goodId?: true;
    @Field(() => Boolean, {nullable:true})
    customerDishId?: true;
}

@ObjectType()
export class OrderItemMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    goodId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    customerDishId?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    quantity?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    orderId?: true;
    @Field(() => Boolean, {nullable:true})
    goodId?: true;
    @Field(() => Boolean, {nullable:true})
    customerDishId?: true;
}

@ObjectType()
export class OrderItemMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    goodId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    customerDishId?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    goodId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    customerDishId?: keyof typeof SortOrder;
    @Field(() => OrderItemCountOrderByAggregateInput, {nullable:true})
    @Type(() => OrderItemCountOrderByAggregateInput)
    _count?: InstanceType<typeof OrderItemCountOrderByAggregateInput>;
    @Field(() => OrderItemAvgOrderByAggregateInput, {nullable:true})
    @Type(() => OrderItemAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof OrderItemAvgOrderByAggregateInput>;
    @Field(() => OrderItemMaxOrderByAggregateInput, {nullable:true})
    @Type(() => OrderItemMaxOrderByAggregateInput)
    _max?: InstanceType<typeof OrderItemMaxOrderByAggregateInput>;
    @Field(() => OrderItemMinOrderByAggregateInput, {nullable:true})
    @Type(() => OrderItemMinOrderByAggregateInput)
    _min?: InstanceType<typeof OrderItemMinOrderByAggregateInput>;
    @Field(() => OrderItemSumOrderByAggregateInput, {nullable:true})
    @Type(() => OrderItemSumOrderByAggregateInput)
    _sum?: InstanceType<typeof OrderItemSumOrderByAggregateInput>;
}

@InputType()
export class OrderItemOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    orderId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    goodId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    customerDishId?: keyof typeof SortOrder;
    @Field(() => OrderOrderByWithRelationInput, {nullable:true})
    @Type(() => OrderOrderByWithRelationInput)
    order?: InstanceType<typeof OrderOrderByWithRelationInput>;
    @Field(() => GoodOrderByWithRelationInput, {nullable:true})
    @Type(() => GoodOrderByWithRelationInput)
    good?: InstanceType<typeof GoodOrderByWithRelationInput>;
    @Field(() => CustomerDishOrderByWithRelationInput, {nullable:true})
    @Type(() => CustomerDishOrderByWithRelationInput)
    customerDish?: InstanceType<typeof CustomerDishOrderByWithRelationInput>;
}

@InputType()
export class OrderItemScalarWhereWithAggregatesInput {
    @Field(() => [OrderItemScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderItemScalarWhereWithAggregatesInput)
    AND?: Array<OrderItemScalarWhereWithAggregatesInput>;
    @Field(() => [OrderItemScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderItemScalarWhereWithAggregatesInput)
    OR?: Array<OrderItemScalarWhereWithAggregatesInput>;
    @Field(() => [OrderItemScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => OrderItemScalarWhereWithAggregatesInput)
    NOT?: Array<OrderItemScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => IntWithAggregatesFilter, {nullable:true})
    quantity?: InstanceType<typeof IntWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    orderId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    goodId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    customerDishId?: InstanceType<typeof StringNullableWithAggregatesFilter>;
}

@InputType()
export class OrderItemScalarWhereInput {
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    AND?: Array<OrderItemScalarWhereInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    OR?: Array<OrderItemScalarWhereInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    NOT?: Array<OrderItemScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => IntFilter, {nullable:true})
    quantity?: InstanceType<typeof IntFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    orderId?: InstanceType<typeof StringFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    goodId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    customerDishId?: InstanceType<typeof StringNullableFilter>;
}

@InputType()
export class OrderItemSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    quantity?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class OrderItemSumAggregate {
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class OrderItemSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    quantity?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class OrderItemUncheckedCreateNestedManyWithoutCustomerDishInput {
    @Field(() => [OrderItemCreateWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create?: Array<OrderItemCreateWithoutCustomerDishInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateManyCustomerDishInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyCustomerDishInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyCustomerDishInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemUncheckedCreateNestedManyWithoutGoodInput {
    @Field(() => [OrderItemCreateWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create?: Array<OrderItemCreateWithoutGoodInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutGoodInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutGoodInput>;
    @Field(() => OrderItemCreateManyGoodInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyGoodInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyGoodInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemUncheckedCreateNestedManyWithoutOrderInput {
    @Field(() => [OrderItemCreateWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create?: Array<OrderItemCreateWithoutOrderInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutOrderInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutOrderInput>;
    @Field(() => OrderItemCreateManyOrderInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyOrderInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyOrderInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
}

@InputType()
export class OrderItemUncheckedCreateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
}

@InputType()
export class OrderItemUncheckedCreateWithoutGoodInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedCreateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:false})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateManyWithoutCustomerDishNestedInput {
    @Field(() => [OrderItemCreateWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create?: Array<OrderItemCreateWithoutCustomerDishInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateManyCustomerDishInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyCustomerDishInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyCustomerDishInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutCustomerDishInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutCustomerDishInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUncheckedUpdateManyWithoutGoodNestedInput {
    @Field(() => [OrderItemCreateWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create?: Array<OrderItemCreateWithoutGoodInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutGoodInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutGoodInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutGoodInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutGoodInput>;
    @Field(() => OrderItemCreateManyGoodInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyGoodInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyGoodInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutGoodInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutGoodInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutGoodInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutGoodInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUncheckedUpdateManyWithoutItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateManyWithoutOrderItemsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateManyWithoutOrderNestedInput {
    @Field(() => [OrderItemCreateWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create?: Array<OrderItemCreateWithoutOrderInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutOrderInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutOrderInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutOrderInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutOrderInput>;
    @Field(() => OrderItemCreateManyOrderInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyOrderInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyOrderInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutOrderInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutOrderInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutOrderInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutOrderInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateWithoutGoodInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    orderId?: string;
    @Field(() => String, {nullable:true})
    goodId?: string;
    @Field(() => String, {nullable:true})
    customerDishId?: string;
}

@InputType()
export class OrderItemUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class OrderItemUpdateManyWithWhereWithoutCustomerDishInput {
    @Field(() => OrderItemScalarWhereInput, {nullable:false})
    @Type(() => OrderItemScalarWhereInput)
    where!: InstanceType<typeof OrderItemScalarWhereInput>;
    @Field(() => OrderItemUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderItemUpdateManyMutationInput)
    data!: InstanceType<typeof OrderItemUpdateManyMutationInput>;
}

@InputType()
export class OrderItemUpdateManyWithWhereWithoutGoodInput {
    @Field(() => OrderItemScalarWhereInput, {nullable:false})
    @Type(() => OrderItemScalarWhereInput)
    where!: InstanceType<typeof OrderItemScalarWhereInput>;
    @Field(() => OrderItemUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderItemUpdateManyMutationInput)
    data!: InstanceType<typeof OrderItemUpdateManyMutationInput>;
}

@InputType()
export class OrderItemUpdateManyWithWhereWithoutOrderInput {
    @Field(() => OrderItemScalarWhereInput, {nullable:false})
    @Type(() => OrderItemScalarWhereInput)
    where!: InstanceType<typeof OrderItemScalarWhereInput>;
    @Field(() => OrderItemUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderItemUpdateManyMutationInput)
    data!: InstanceType<typeof OrderItemUpdateManyMutationInput>;
}

@InputType()
export class OrderItemUpdateManyWithoutCustomerDishNestedInput {
    @Field(() => [OrderItemCreateWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create?: Array<OrderItemCreateWithoutCustomerDishInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutCustomerDishInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutCustomerDishInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateManyCustomerDishInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyCustomerDishInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyCustomerDishInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutCustomerDishInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutCustomerDishInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutCustomerDishInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUpdateManyWithoutGoodNestedInput {
    @Field(() => [OrderItemCreateWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create?: Array<OrderItemCreateWithoutGoodInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutGoodInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutGoodInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutGoodInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutGoodInput>;
    @Field(() => OrderItemCreateManyGoodInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyGoodInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyGoodInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutGoodInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutGoodInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutGoodInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutGoodInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutGoodInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUpdateManyWithoutOrderNestedInput {
    @Field(() => [OrderItemCreateWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create?: Array<OrderItemCreateWithoutOrderInput>;
    @Field(() => [OrderItemCreateOrConnectWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemCreateOrConnectWithoutOrderInput)
    connectOrCreate?: Array<OrderItemCreateOrConnectWithoutOrderInput>;
    @Field(() => [OrderItemUpsertWithWhereUniqueWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpsertWithWhereUniqueWithoutOrderInput)
    upsert?: Array<OrderItemUpsertWithWhereUniqueWithoutOrderInput>;
    @Field(() => OrderItemCreateManyOrderInputEnvelope, {nullable:true})
    @Type(() => OrderItemCreateManyOrderInputEnvelope)
    createMany?: InstanceType<typeof OrderItemCreateManyOrderInputEnvelope>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    set?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    disconnect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    delete?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemWhereUniqueInput], {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    connect?: Array<OrderItemWhereUniqueInput>;
    @Field(() => [OrderItemUpdateWithWhereUniqueWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpdateWithWhereUniqueWithoutOrderInput)
    update?: Array<OrderItemUpdateWithWhereUniqueWithoutOrderInput>;
    @Field(() => [OrderItemUpdateManyWithWhereWithoutOrderInput], {nullable:true})
    @Type(() => OrderItemUpdateManyWithWhereWithoutOrderInput)
    updateMany?: Array<OrderItemUpdateManyWithWhereWithoutOrderInput>;
    @Field(() => [OrderItemScalarWhereInput], {nullable:true})
    @Type(() => OrderItemScalarWhereInput)
    deleteMany?: Array<OrderItemScalarWhereInput>;
}

@InputType()
export class OrderItemUpdateWithWhereUniqueWithoutCustomerDishInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutCustomerDishInput)
    data!: InstanceType<typeof OrderItemUpdateWithoutCustomerDishInput>;
}

@InputType()
export class OrderItemUpdateWithWhereUniqueWithoutGoodInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutGoodInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutGoodInput)
    data!: InstanceType<typeof OrderItemUpdateWithoutGoodInput>;
}

@InputType()
export class OrderItemUpdateWithWhereUniqueWithoutOrderInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutOrderInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutOrderInput)
    data!: InstanceType<typeof OrderItemUpdateWithoutOrderInput>;
}

@InputType()
export class OrderItemUpdateWithoutCustomerDishInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OrderUpdateOneRequiredWithoutItemsNestedInput, {nullable:true})
    @Type(() => OrderUpdateOneRequiredWithoutItemsNestedInput)
    order?: InstanceType<typeof OrderUpdateOneRequiredWithoutItemsNestedInput>;
    @Field(() => GoodUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => GoodUpdateOneWithoutOrderItemsNestedInput)
    good?: InstanceType<typeof GoodUpdateOneWithoutOrderItemsNestedInput>;
}

@InputType()
export class OrderItemUpdateWithoutGoodInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OrderUpdateOneRequiredWithoutItemsNestedInput, {nullable:true})
    @Type(() => OrderUpdateOneRequiredWithoutItemsNestedInput)
    order?: InstanceType<typeof OrderUpdateOneRequiredWithoutItemsNestedInput>;
    @Field(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput)
    customerDish?: InstanceType<typeof CustomerDishUpdateOneWithoutOrderItemsNestedInput>;
}

@InputType()
export class OrderItemUpdateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => GoodUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => GoodUpdateOneWithoutOrderItemsNestedInput)
    good?: InstanceType<typeof GoodUpdateOneWithoutOrderItemsNestedInput>;
    @Field(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput)
    customerDish?: InstanceType<typeof CustomerDishUpdateOneWithoutOrderItemsNestedInput>;
}

@InputType()
export class OrderItemUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => Int, {nullable:true})
    quantity?: number;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OrderUpdateOneRequiredWithoutItemsNestedInput, {nullable:true})
    @Type(() => OrderUpdateOneRequiredWithoutItemsNestedInput)
    order?: InstanceType<typeof OrderUpdateOneRequiredWithoutItemsNestedInput>;
    @Field(() => GoodUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => GoodUpdateOneWithoutOrderItemsNestedInput)
    good?: InstanceType<typeof GoodUpdateOneWithoutOrderItemsNestedInput>;
    @Field(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateOneWithoutOrderItemsNestedInput)
    customerDish?: InstanceType<typeof CustomerDishUpdateOneWithoutOrderItemsNestedInput>;
}

@InputType()
export class OrderItemUpsertWithWhereUniqueWithoutCustomerDishInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutCustomerDishInput)
    update!: InstanceType<typeof OrderItemUpdateWithoutCustomerDishInput>;
    @Field(() => OrderItemCreateWithoutCustomerDishInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutCustomerDishInput)
    create!: InstanceType<typeof OrderItemCreateWithoutCustomerDishInput>;
}

@InputType()
export class OrderItemUpsertWithWhereUniqueWithoutGoodInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutGoodInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutGoodInput)
    update!: InstanceType<typeof OrderItemUpdateWithoutGoodInput>;
    @Field(() => OrderItemCreateWithoutGoodInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutGoodInput)
    create!: InstanceType<typeof OrderItemCreateWithoutGoodInput>;
}

@InputType()
export class OrderItemUpsertWithWhereUniqueWithoutOrderInput {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemUpdateWithoutOrderInput, {nullable:false})
    @Type(() => OrderItemUpdateWithoutOrderInput)
    update!: InstanceType<typeof OrderItemUpdateWithoutOrderInput>;
    @Field(() => OrderItemCreateWithoutOrderInput, {nullable:false})
    @Type(() => OrderItemCreateWithoutOrderInput)
    create!: InstanceType<typeof OrderItemCreateWithoutOrderInput>;
}

@InputType()
export class OrderItemWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class OrderItemWhereInput {
    @Field(() => [OrderItemWhereInput], {nullable:true})
    @Type(() => OrderItemWhereInput)
    AND?: Array<OrderItemWhereInput>;
    @Field(() => [OrderItemWhereInput], {nullable:true})
    @Type(() => OrderItemWhereInput)
    OR?: Array<OrderItemWhereInput>;
    @Field(() => [OrderItemWhereInput], {nullable:true})
    @Type(() => OrderItemWhereInput)
    NOT?: Array<OrderItemWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => IntFilter, {nullable:true})
    quantity?: InstanceType<typeof IntFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    orderId?: InstanceType<typeof StringFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    goodId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    customerDishId?: InstanceType<typeof StringNullableFilter>;
    @Field(() => OrderRelationFilter, {nullable:true})
    @Type(() => OrderRelationFilter)
    order?: InstanceType<typeof OrderRelationFilter>;
    @Field(() => GoodRelationFilter, {nullable:true})
    @Type(() => GoodRelationFilter)
    good?: InstanceType<typeof GoodRelationFilter>;
    @Field(() => CustomerDishRelationFilter, {nullable:true})
    @Type(() => CustomerDishRelationFilter)
    customerDish?: InstanceType<typeof CustomerDishRelationFilter>;
}

@ObjectType()
export class OrderItem {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    /**
     * @Validator .@IsDefined()
     * @Validator .@IsInt()
     * @Validator .@NotEquals(0)
     * @Validator .@Min(-1)
     * @Validator .@Max(10)
     */
    @Field(() => Int, {nullable:false,description:'@Validator.@IsDefined()\n@Validator.@IsInt()\n@Validator.@NotEquals(0)\n@Validator.@Min(-1)\n@Validator.@Max(10)'})
    quantity!: number;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => String, {nullable:false})
    orderId!: string;
    @Field(() => String, {nullable:true})
    goodId!: string | null;
    @Field(() => String, {nullable:true})
    customerDishId!: string | null;
    @Field(() => Order, {nullable:false})
    order?: InstanceType<typeof Order>;
    @Field(() => Good, {nullable:true})
    good?: InstanceType<typeof Good> | null;
    @Field(() => CustomerDish, {nullable:true})
    customerDish?: InstanceType<typeof CustomerDish> | null;
}

@ArgsType()
export class UpdateManyOrderItemArgs {
    @Field(() => OrderItemUpdateManyMutationInput, {nullable:false})
    @Type(() => OrderItemUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderItemUpdateManyMutationInput>;
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
}

@ArgsType()
export class UpdateOneOrderItemArgs {
    @Field(() => OrderItemUpdateInput, {nullable:false})
    @Type(() => OrderItemUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof OrderItemUpdateInput>;
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneOrderItemArgs {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => OrderItemCreateInput, {nullable:false})
    @Type(() => OrderItemCreateInput)
    create!: InstanceType<typeof OrderItemCreateInput>;
    @Field(() => OrderItemUpdateInput, {nullable:false})
    @Type(() => OrderItemUpdateInput)
    update!: InstanceType<typeof OrderItemUpdateInput>;
}

@ObjectType()
export class AffectedRows {
    @Field(() => Int, {nullable:false})
    count!: number;
}

@InputType()
export class DateTimeFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class DateTimeWithAggregatesFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDateTimeFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class DecimalFilter {
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    equals?: Decimal;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    in?: Array<Decimal>;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    notIn?: Array<Decimal>;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lte?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gte?: Decimal;
    @Field(() => NestedDecimalFilter, {nullable:true})
    not?: InstanceType<typeof NestedDecimalFilter>;
}

@InputType()
export class DecimalWithAggregatesFilter {
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    equals?: Decimal;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    in?: Array<Decimal>;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    notIn?: Array<Decimal>;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lte?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gte?: Decimal;
    @Field(() => NestedDecimalWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDecimalWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDecimalFilter>;
}

@InputType()
export class EnumEnumOrderStatusFilter {
    @Field(() => EnumOrderStatus, {nullable:true})
    equals?: keyof typeof EnumOrderStatus;
    @Field(() => [EnumOrderStatus], {nullable:true})
    in?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => [EnumOrderStatus], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
}

@InputType()
export class EnumEnumOrderStatusWithAggregatesFilter {
    @Field(() => EnumOrderStatus, {nullable:true})
    equals?: keyof typeof EnumOrderStatus;
    @Field(() => [EnumOrderStatus], {nullable:true})
    in?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => [EnumOrderStatus], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => NestedEnumEnumOrderStatusWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderStatusWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
}

@InputType()
export class EnumEnumOrderTypeFilter {
    @Field(() => EnumOrderType, {nullable:true})
    equals?: keyof typeof EnumOrderType;
    @Field(() => [EnumOrderType], {nullable:true})
    in?: Array<keyof typeof EnumOrderType>;
    @Field(() => [EnumOrderType], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderType>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
}

@InputType()
export class EnumEnumOrderTypeWithAggregatesFilter {
    @Field(() => EnumOrderType, {nullable:true})
    equals?: keyof typeof EnumOrderType;
    @Field(() => [EnumOrderType], {nullable:true})
    in?: Array<keyof typeof EnumOrderType>;
    @Field(() => [EnumOrderType], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderType>;
    @Field(() => NestedEnumEnumOrderTypeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderTypeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
}

@InputType()
export class EnumEnumPaymentTypeFilter {
    @Field(() => EnumPaymentType, {nullable:true})
    equals?: keyof typeof EnumPaymentType;
    @Field(() => [EnumPaymentType], {nullable:true})
    in?: Array<keyof typeof EnumPaymentType>;
    @Field(() => [EnumPaymentType], {nullable:true})
    notIn?: Array<keyof typeof EnumPaymentType>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
}

@InputType()
export class EnumEnumPaymentTypeWithAggregatesFilter {
    @Field(() => EnumPaymentType, {nullable:true})
    equals?: keyof typeof EnumPaymentType;
    @Field(() => [EnumPaymentType], {nullable:true})
    in?: Array<keyof typeof EnumPaymentType>;
    @Field(() => [EnumPaymentType], {nullable:true})
    notIn?: Array<keyof typeof EnumPaymentType>;
    @Field(() => NestedEnumEnumPaymentTypeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumPaymentTypeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
}

@InputType()
export class EnumEnumRoleNullableListFilter {
    @Field(() => [EnumRole], {nullable:true})
    equals?: Array<keyof typeof EnumRole>;
    @Field(() => EnumRole, {nullable:true})
    has?: keyof typeof EnumRole;
    @Field(() => [EnumRole], {nullable:true})
    hasEvery?: Array<keyof typeof EnumRole>;
    @Field(() => [EnumRole], {nullable:true})
    hasSome?: Array<keyof typeof EnumRole>;
    @Field(() => Boolean, {nullable:true})
    isEmpty?: boolean;
}

@ArgsType()
export class FindFirstCategoryOrThrowArgs {
    @Field(() => CategoryWhereInput, {nullable:true})
    @Type(() => CategoryWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CategoryWhereInput>;
    @Field(() => [CategoryOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<CategoryOrderByWithRelationInput>;
    @Field(() => CategoryWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof CategoryWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CategoryScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CategoryScalarFieldEnum>;
}

@ArgsType()
export class FindFirstCustomerDishOrThrowArgs {
    @Field(() => CustomerDishWhereInput, {nullable:true})
    @Type(() => CustomerDishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof CustomerDishWhereInput>;
    @Field(() => [CustomerDishOrderByWithRelationInput], {nullable:true})
    @Type(() => CustomerDishOrderByWithRelationInput)
    orderBy?: Array<CustomerDishOrderByWithRelationInput>;
    @Field(() => CustomerDishWhereUniqueInput, {nullable:true})
    @Type(() => CustomerDishWhereUniqueInput)
    cursor?: InstanceType<typeof CustomerDishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [CustomerDishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof CustomerDishScalarFieldEnum>;
}

@ArgsType()
export class FindFirstDishOrThrowArgs {
    @Field(() => DishWhereInput, {nullable:true})
    @Type(() => DishWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof DishWhereInput>;
    @Field(() => [DishOrderByWithRelationInput], {nullable:true})
    @Type(() => DishOrderByWithRelationInput)
    orderBy?: Array<DishOrderByWithRelationInput>;
    @Field(() => DishWhereUniqueInput, {nullable:true})
    @Type(() => DishWhereUniqueInput)
    cursor?: InstanceType<typeof DishWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [DishScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof DishScalarFieldEnum>;
}

@ArgsType()
export class FindFirstGoodOrThrowArgs {
    @Field(() => GoodWhereInput, {nullable:true})
    @Type(() => GoodWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof GoodWhereInput>;
    @Field(() => [GoodOrderByWithRelationInput], {nullable:true})
    @Type(() => GoodOrderByWithRelationInput)
    orderBy?: Array<GoodOrderByWithRelationInput>;
    @Field(() => GoodWhereUniqueInput, {nullable:true})
    @Type(() => GoodWhereUniqueInput)
    cursor?: InstanceType<typeof GoodWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [GoodScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof GoodScalarFieldEnum>;
}

@ArgsType()
export class FindFirstIngradientLabelOrThrowArgs {
    @Field(() => IngradientLabelWhereInput, {nullable:true})
    @Type(() => IngradientLabelWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof IngradientLabelWhereInput>;
    @Field(() => [IngradientLabelOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<IngradientLabelOrderByWithRelationInput>;
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof IngradientLabelWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [IngradientLabelScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof IngradientLabelScalarFieldEnum>;
}

@ArgsType()
export class FindFirstOptionsOrThrowArgs {
    @Field(() => OptionsWhereInput, {nullable:true})
    @Type(() => OptionsWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OptionsWhereInput>;
    @Field(() => [OptionsOrderByWithRelationInput], {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    orderBy?: Array<OptionsOrderByWithRelationInput>;
    @Field(() => OptionsWhereUniqueInput, {nullable:true})
    @Type(() => OptionsWhereUniqueInput)
    cursor?: InstanceType<typeof OptionsWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OptionsScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OptionsScalarFieldEnum>;
}

@ArgsType()
export class FindFirstOrderItemOrThrowArgs {
    @Field(() => OrderItemWhereInput, {nullable:true})
    @Type(() => OrderItemWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderItemWhereInput>;
    @Field(() => [OrderItemOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderItemOrderByWithRelationInput)
    orderBy?: Array<OrderItemOrderByWithRelationInput>;
    @Field(() => OrderItemWhereUniqueInput, {nullable:true})
    @Type(() => OrderItemWhereUniqueInput)
    cursor?: InstanceType<typeof OrderItemWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderItemScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderItemScalarFieldEnum>;
}

@ArgsType()
export class FindFirstOrderOrThrowArgs {
    @Field(() => OrderWhereInput, {nullable:true})
    @Type(() => OrderWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof OrderWhereInput>;
    @Field(() => [OrderOrderByWithRelationInput], {nullable:true})
    @Type(() => OrderOrderByWithRelationInput)
    orderBy?: Array<OrderOrderByWithRelationInput>;
    @Field(() => OrderWhereUniqueInput, {nullable:true})
    @Type(() => OrderWhereUniqueInput)
    cursor?: InstanceType<typeof OrderWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [OrderScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof OrderScalarFieldEnum>;
}

@ArgsType()
export class FindFirstRestaurantOrThrowArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => [RestaurantOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<RestaurantOrderByWithRelationInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [RestaurantScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof RestaurantScalarFieldEnum>;
}

@ArgsType()
export class FindFirstToppingOrThrowArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => [ToppingOrderByWithRelationInput], {nullable:true})
    @Type(() => ToppingOrderByWithRelationInput)
    orderBy?: Array<ToppingOrderByWithRelationInput>;
    @Field(() => ToppingWhereUniqueInput, {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    cursor?: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ToppingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ToppingScalarFieldEnum>;
}

@ArgsType()
export class FindFirstUserOrThrowArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
    @Field(() => [UserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<UserOrderByWithRelationInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [UserScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof UserScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueCategoryOrThrowArgs {
    @Field(() => CategoryWhereUniqueInput, {nullable:false})
    @Type(() => CategoryWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CategoryWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueCustomerDishOrThrowArgs {
    @Field(() => CustomerDishWhereUniqueInput, {nullable:false})
    @Type(() => CustomerDishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof CustomerDishWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueDishOrThrowArgs {
    @Field(() => DishWhereUniqueInput, {nullable:false})
    @Type(() => DishWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof DishWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueGoodOrThrowArgs {
    @Field(() => GoodWhereUniqueInput, {nullable:false})
    @Type(() => GoodWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof GoodWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueIngradientLabelOrThrowArgs {
    @Field(() => IngradientLabelWhereUniqueInput, {nullable:false})
    @Type(() => IngradientLabelWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof IngradientLabelWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueOptionsOrThrowArgs {
    @Field(() => OptionsWhereUniqueInput, {nullable:false})
    @Type(() => OptionsWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OptionsWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueOrderItemOrThrowArgs {
    @Field(() => OrderItemWhereUniqueInput, {nullable:false})
    @Type(() => OrderItemWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderItemWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueOrderOrThrowArgs {
    @Field(() => OrderWhereUniqueInput, {nullable:false})
    @Type(() => OrderWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof OrderWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueRestaurantOrThrowArgs {
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueToppingOrThrowArgs {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
}

@ArgsType()
export class FindUniqueUserOrThrowArgs {
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof UserWhereUniqueInput>;
}

@InputType()
export class IntFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class IntWithAggregatesFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedFloatFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedFloatFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _min?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _max?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class NestedDateTimeFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class NestedDateTimeWithAggregatesFilter {
    @Field(() => Date, {nullable:true})
    equals?: Date | string;
    @Field(() => [Date], {nullable:true})
    in?: Array<Date> | Array<string>;
    @Field(() => [Date], {nullable:true})
    notIn?: Array<Date> | Array<string>;
    @Field(() => Date, {nullable:true})
    lt?: Date | string;
    @Field(() => Date, {nullable:true})
    lte?: Date | string;
    @Field(() => Date, {nullable:true})
    gt?: Date | string;
    @Field(() => Date, {nullable:true})
    gte?: Date | string;
    @Field(() => NestedDateTimeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDateTimeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDateTimeFilter>;
    @Field(() => NestedDateTimeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDateTimeFilter>;
}

@InputType()
export class NestedDecimalFilter {
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    equals?: Decimal;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    in?: Array<Decimal>;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    notIn?: Array<Decimal>;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lte?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gte?: Decimal;
    @Field(() => NestedDecimalFilter, {nullable:true})
    not?: InstanceType<typeof NestedDecimalFilter>;
}

@InputType()
export class NestedDecimalWithAggregatesFilter {
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    equals?: Decimal;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    in?: Array<Decimal>;
    @Field(() => [GraphQLDecimal], {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    notIn?: Array<Decimal>;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    lte?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gt?: Decimal;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    gte?: Decimal;
    @Field(() => NestedDecimalWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedDecimalWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _min?: InstanceType<typeof NestedDecimalFilter>;
    @Field(() => NestedDecimalFilter, {nullable:true})
    _max?: InstanceType<typeof NestedDecimalFilter>;
}

@InputType()
export class NestedEnumEnumOrderStatusFilter {
    @Field(() => EnumOrderStatus, {nullable:true})
    equals?: keyof typeof EnumOrderStatus;
    @Field(() => [EnumOrderStatus], {nullable:true})
    in?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => [EnumOrderStatus], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
}

@InputType()
export class NestedEnumEnumOrderStatusWithAggregatesFilter {
    @Field(() => EnumOrderStatus, {nullable:true})
    equals?: keyof typeof EnumOrderStatus;
    @Field(() => [EnumOrderStatus], {nullable:true})
    in?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => [EnumOrderStatus], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderStatus>;
    @Field(() => NestedEnumEnumOrderStatusWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderStatusWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
    @Field(() => NestedEnumEnumOrderStatusFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumOrderStatusFilter>;
}

@InputType()
export class NestedEnumEnumOrderTypeFilter {
    @Field(() => EnumOrderType, {nullable:true})
    equals?: keyof typeof EnumOrderType;
    @Field(() => [EnumOrderType], {nullable:true})
    in?: Array<keyof typeof EnumOrderType>;
    @Field(() => [EnumOrderType], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderType>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
}

@InputType()
export class NestedEnumEnumOrderTypeWithAggregatesFilter {
    @Field(() => EnumOrderType, {nullable:true})
    equals?: keyof typeof EnumOrderType;
    @Field(() => [EnumOrderType], {nullable:true})
    in?: Array<keyof typeof EnumOrderType>;
    @Field(() => [EnumOrderType], {nullable:true})
    notIn?: Array<keyof typeof EnumOrderType>;
    @Field(() => NestedEnumEnumOrderTypeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumOrderTypeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
    @Field(() => NestedEnumEnumOrderTypeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumOrderTypeFilter>;
}

@InputType()
export class NestedEnumEnumPaymentTypeFilter {
    @Field(() => EnumPaymentType, {nullable:true})
    equals?: keyof typeof EnumPaymentType;
    @Field(() => [EnumPaymentType], {nullable:true})
    in?: Array<keyof typeof EnumPaymentType>;
    @Field(() => [EnumPaymentType], {nullable:true})
    notIn?: Array<keyof typeof EnumPaymentType>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
}

@InputType()
export class NestedEnumEnumPaymentTypeWithAggregatesFilter {
    @Field(() => EnumPaymentType, {nullable:true})
    equals?: keyof typeof EnumPaymentType;
    @Field(() => [EnumPaymentType], {nullable:true})
    in?: Array<keyof typeof EnumPaymentType>;
    @Field(() => [EnumPaymentType], {nullable:true})
    notIn?: Array<keyof typeof EnumPaymentType>;
    @Field(() => NestedEnumEnumPaymentTypeWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedEnumEnumPaymentTypeWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    _min?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
    @Field(() => NestedEnumEnumPaymentTypeFilter, {nullable:true})
    _max?: InstanceType<typeof NestedEnumEnumPaymentTypeFilter>;
}

@InputType()
export class NestedFloatFilter {
    @Field(() => Float, {nullable:true})
    equals?: number;
    @Field(() => [Float], {nullable:true})
    in?: Array<number>;
    @Field(() => [Float], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Float, {nullable:true})
    lt?: number;
    @Field(() => Float, {nullable:true})
    lte?: number;
    @Field(() => Float, {nullable:true})
    gt?: number;
    @Field(() => Float, {nullable:true})
    gte?: number;
    @Field(() => NestedFloatFilter, {nullable:true})
    not?: InstanceType<typeof NestedFloatFilter>;
}

@InputType()
export class NestedIntFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class NestedIntNullableFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntNullableFilter>;
}

@InputType()
export class NestedIntWithAggregatesFilter {
    @Field(() => Int, {nullable:true})
    equals?: number;
    @Field(() => [Int], {nullable:true})
    in?: Array<number>;
    @Field(() => [Int], {nullable:true})
    notIn?: Array<number>;
    @Field(() => Int, {nullable:true})
    lt?: number;
    @Field(() => Int, {nullable:true})
    lte?: number;
    @Field(() => Int, {nullable:true})
    gt?: number;
    @Field(() => Int, {nullable:true})
    gte?: number;
    @Field(() => NestedIntWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedIntWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedFloatFilter, {nullable:true})
    _avg?: InstanceType<typeof NestedFloatFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _sum?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _min?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _max?: InstanceType<typeof NestedIntFilter>;
}

@InputType()
export class NestedStringFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class NestedStringNullableFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class NestedStringNullableWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringNullableWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableWithAggregatesFilter>;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class NestedStringWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => NestedStringWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class StringFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringFilter>;
}

@InputType()
export class StringNullableFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class StringNullableListFilter {
    @Field(() => [String], {nullable:true})
    equals?: Array<string>;
    @Field(() => String, {nullable:true})
    has?: string;
    @Field(() => [String], {nullable:true})
    hasEvery?: Array<string>;
    @Field(() => [String], {nullable:true})
    hasSome?: Array<string>;
    @Field(() => Boolean, {nullable:true})
    isEmpty?: boolean;
}

@InputType()
export class StringNullableWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringNullableWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringNullableWithAggregatesFilter>;
    @Field(() => NestedIntNullableFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringNullableFilter>;
    @Field(() => NestedStringNullableFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringNullableFilter>;
}

@InputType()
export class StringWithAggregatesFilter {
    @Field(() => String, {nullable:true})
    equals?: string;
    @Field(() => [String], {nullable:true})
    in?: Array<string>;
    @Field(() => [String], {nullable:true})
    notIn?: Array<string>;
    @Field(() => String, {nullable:true})
    lt?: string;
    @Field(() => String, {nullable:true})
    lte?: string;
    @Field(() => String, {nullable:true})
    gt?: string;
    @Field(() => String, {nullable:true})
    gte?: string;
    @Field(() => String, {nullable:true})
    contains?: string;
    @Field(() => String, {nullable:true})
    startsWith?: string;
    @Field(() => String, {nullable:true})
    endsWith?: string;
    @Field(() => QueryMode, {nullable:true})
    mode?: keyof typeof QueryMode;
    @Field(() => NestedStringWithAggregatesFilter, {nullable:true})
    not?: InstanceType<typeof NestedStringWithAggregatesFilter>;
    @Field(() => NestedIntFilter, {nullable:true})
    _count?: InstanceType<typeof NestedIntFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _min?: InstanceType<typeof NestedStringFilter>;
    @Field(() => NestedStringFilter, {nullable:true})
    _max?: InstanceType<typeof NestedStringFilter>;
}

@ObjectType()
export class AggregateRestaurant {
    @Field(() => RestaurantCountAggregate, {nullable:true})
    _count?: InstanceType<typeof RestaurantCountAggregate>;
    @Field(() => RestaurantMinAggregate, {nullable:true})
    _min?: InstanceType<typeof RestaurantMinAggregate>;
    @Field(() => RestaurantMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof RestaurantMaxAggregate>;
}

@ArgsType()
export class CreateManyRestaurantArgs {
    @Field(() => [RestaurantCreateManyInput], {nullable:false})
    @Type(() => RestaurantCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<RestaurantCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneRestaurantArgs {
    @Field(() => RestaurantCreateInput, {nullable:false})
    @Type(() => RestaurantCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof RestaurantCreateInput>;
}

@ArgsType()
export class DeleteManyRestaurantArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
}

@ArgsType()
export class DeleteOneRestaurantArgs {
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
}

@ArgsType()
export class FindFirstRestaurantArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => [RestaurantOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<RestaurantOrderByWithRelationInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [RestaurantScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof RestaurantScalarFieldEnum>;
}

@ArgsType()
export class FindManyRestaurantArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => [RestaurantOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<RestaurantOrderByWithRelationInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [RestaurantScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof RestaurantScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueRestaurantArgs {
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
}

@ArgsType()
export class RestaurantAggregateArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => [RestaurantOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<RestaurantOrderByWithRelationInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => RestaurantCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof RestaurantCountAggregateInput>;
    @Field(() => RestaurantMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof RestaurantMinAggregateInput>;
    @Field(() => RestaurantMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof RestaurantMaxAggregateInput>;
}

@InputType()
export class RestaurantCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    address?: true;
    @Field(() => Boolean, {nullable:true})
    city?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class RestaurantCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    address!: number;
    @Field(() => Int, {nullable:false})
    city!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class RestaurantCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    address?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    city?: keyof typeof SortOrder;
}

@ObjectType()
export class RestaurantCount {
    @Field(() => Int, {nullable:false})
    order?: number;
}

@InputType()
export class RestaurantCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
}

@InputType()
export class RestaurantCreateNestedOneWithoutOrderInput {
    @Field(() => RestaurantCreateWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantCreateWithoutOrderInput)
    create?: InstanceType<typeof RestaurantCreateWithoutOrderInput>;
    @Field(() => RestaurantCreateOrConnectWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantCreateOrConnectWithoutOrderInput)
    connectOrCreate?: InstanceType<typeof RestaurantCreateOrConnectWithoutOrderInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    @Type(() => RestaurantWhereUniqueInput)
    connect?: InstanceType<typeof RestaurantWhereUniqueInput>;
}

@InputType()
export class RestaurantCreateOrConnectWithoutOrderInput {
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => RestaurantCreateWithoutOrderInput, {nullable:false})
    @Type(() => RestaurantCreateWithoutOrderInput)
    create!: InstanceType<typeof RestaurantCreateWithoutOrderInput>;
}

@InputType()
export class RestaurantCreateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
}

@InputType()
export class RestaurantCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
    @Field(() => OrderCreateNestedManyWithoutRestaurantInput, {nullable:true})
    @Type(() => OrderCreateNestedManyWithoutRestaurantInput)
    order?: InstanceType<typeof OrderCreateNestedManyWithoutRestaurantInput>;
}

@ArgsType()
export class RestaurantGroupByArgs {
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => [RestaurantOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<RestaurantOrderByWithAggregationInput>;
    @Field(() => [RestaurantScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof RestaurantScalarFieldEnum>;
    @Field(() => RestaurantScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof RestaurantScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => RestaurantCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof RestaurantCountAggregateInput>;
    @Field(() => RestaurantMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof RestaurantMinAggregateInput>;
    @Field(() => RestaurantMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof RestaurantMaxAggregateInput>;
}

@ObjectType()
export class RestaurantGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
    @Field(() => RestaurantCountAggregate, {nullable:true})
    _count?: InstanceType<typeof RestaurantCountAggregate>;
    @Field(() => RestaurantMinAggregate, {nullable:true})
    _min?: InstanceType<typeof RestaurantMinAggregate>;
    @Field(() => RestaurantMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof RestaurantMaxAggregate>;
}

@InputType()
export class RestaurantMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    address?: true;
    @Field(() => Boolean, {nullable:true})
    city?: true;
}

@ObjectType()
export class RestaurantMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    address?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    city?: keyof typeof SortOrder;
}

@InputType()
export class RestaurantMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    address?: true;
    @Field(() => Boolean, {nullable:true})
    city?: true;
}

@ObjectType()
export class RestaurantMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    address?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    city?: keyof typeof SortOrder;
}

@InputType()
export class RestaurantOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    address?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    city?: keyof typeof SortOrder;
    @Field(() => RestaurantCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof RestaurantCountOrderByAggregateInput>;
    @Field(() => RestaurantMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof RestaurantMaxOrderByAggregateInput>;
    @Field(() => RestaurantMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof RestaurantMinOrderByAggregateInput>;
}

@InputType()
export class RestaurantOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    address?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    city?: keyof typeof SortOrder;
    @Field(() => OrderOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OrderOrderByRelationAggregateInput)
    order?: InstanceType<typeof OrderOrderByRelationAggregateInput>;
}

@InputType()
export class RestaurantRelationFilter {
    @Field(() => RestaurantWhereInput, {nullable:true})
    is?: InstanceType<typeof RestaurantWhereInput>;
    @Field(() => RestaurantWhereInput, {nullable:true})
    isNot?: InstanceType<typeof RestaurantWhereInput>;
}

@InputType()
export class RestaurantScalarWhereWithAggregatesInput {
    @Field(() => [RestaurantScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<RestaurantScalarWhereWithAggregatesInput>;
    @Field(() => [RestaurantScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<RestaurantScalarWhereWithAggregatesInput>;
    @Field(() => [RestaurantScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<RestaurantScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    address?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    city?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class RestaurantUncheckedCreateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
}

@InputType()
export class RestaurantUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city!: string;
    @Field(() => OrderUncheckedCreateNestedManyWithoutRestaurantInput, {nullable:true})
    @Type(() => OrderUncheckedCreateNestedManyWithoutRestaurantInput)
    order?: InstanceType<typeof OrderUncheckedCreateNestedManyWithoutRestaurantInput>;
}

@InputType()
export class RestaurantUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantUncheckedUpdateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
    @Field(() => OrderUncheckedUpdateManyWithoutRestaurantNestedInput, {nullable:true})
    @Type(() => OrderUncheckedUpdateManyWithoutRestaurantNestedInput)
    order?: InstanceType<typeof OrderUncheckedUpdateManyWithoutRestaurantNestedInput>;
}

@InputType()
export class RestaurantUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantUpdateOneRequiredWithoutOrderNestedInput {
    @Field(() => RestaurantCreateWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantCreateWithoutOrderInput)
    create?: InstanceType<typeof RestaurantCreateWithoutOrderInput>;
    @Field(() => RestaurantCreateOrConnectWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantCreateOrConnectWithoutOrderInput)
    connectOrCreate?: InstanceType<typeof RestaurantCreateOrConnectWithoutOrderInput>;
    @Field(() => RestaurantUpsertWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantUpsertWithoutOrderInput)
    upsert?: InstanceType<typeof RestaurantUpsertWithoutOrderInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:true})
    @Type(() => RestaurantWhereUniqueInput)
    connect?: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => RestaurantUpdateWithoutOrderInput, {nullable:true})
    @Type(() => RestaurantUpdateWithoutOrderInput)
    update?: InstanceType<typeof RestaurantUpdateWithoutOrderInput>;
}

@InputType()
export class RestaurantUpdateWithoutOrderInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
}

@InputType()
export class RestaurantUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    address?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    city?: string;
    @Field(() => OrderUpdateManyWithoutRestaurantNestedInput, {nullable:true})
    @Type(() => OrderUpdateManyWithoutRestaurantNestedInput)
    order?: InstanceType<typeof OrderUpdateManyWithoutRestaurantNestedInput>;
}

@InputType()
export class RestaurantUpsertWithoutOrderInput {
    @Field(() => RestaurantUpdateWithoutOrderInput, {nullable:false})
    @Type(() => RestaurantUpdateWithoutOrderInput)
    update!: InstanceType<typeof RestaurantUpdateWithoutOrderInput>;
    @Field(() => RestaurantCreateWithoutOrderInput, {nullable:false})
    @Type(() => RestaurantCreateWithoutOrderInput)
    create!: InstanceType<typeof RestaurantCreateWithoutOrderInput>;
}

@InputType()
export class RestaurantWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
}

@InputType()
export class RestaurantWhereInput {
    @Field(() => [RestaurantWhereInput], {nullable:true})
    AND?: Array<RestaurantWhereInput>;
    @Field(() => [RestaurantWhereInput], {nullable:true})
    OR?: Array<RestaurantWhereInput>;
    @Field(() => [RestaurantWhereInput], {nullable:true})
    NOT?: Array<RestaurantWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    address?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    city?: InstanceType<typeof StringFilter>;
    @Field(() => OrderListRelationFilter, {nullable:true})
    @Type(() => OrderListRelationFilter)
    order?: InstanceType<typeof OrderListRelationFilter>;
}

@ObjectType()
export class Restaurant {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    name!: string;
    @Field(() => String, {nullable:false})
    address!: string;
    @Field(() => String, {nullable:false})
    city!: string;
    @Field(() => [Order], {nullable:true})
    order?: Array<Order>;
    @Field(() => RestaurantCount, {nullable:false})
    _count?: InstanceType<typeof RestaurantCount>;
}

@ArgsType()
export class UpdateManyRestaurantArgs {
    @Field(() => RestaurantUpdateManyMutationInput, {nullable:false})
    @Type(() => RestaurantUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof RestaurantUpdateManyMutationInput>;
    @Field(() => RestaurantWhereInput, {nullable:true})
    @Type(() => RestaurantWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof RestaurantWhereInput>;
}

@ArgsType()
export class UpdateOneRestaurantArgs {
    @Field(() => RestaurantUpdateInput, {nullable:false})
    @Type(() => RestaurantUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof RestaurantUpdateInput>;
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneRestaurantArgs {
    @Field(() => RestaurantWhereUniqueInput, {nullable:false})
    @Type(() => RestaurantWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof RestaurantWhereUniqueInput>;
    @Field(() => RestaurantCreateInput, {nullable:false})
    @Type(() => RestaurantCreateInput)
    create!: InstanceType<typeof RestaurantCreateInput>;
    @Field(() => RestaurantUpdateInput, {nullable:false})
    @Type(() => RestaurantUpdateInput)
    update!: InstanceType<typeof RestaurantUpdateInput>;
}

@ObjectType()
export class AggregateTopping {
    @Field(() => ToppingCountAggregate, {nullable:true})
    _count?: InstanceType<typeof ToppingCountAggregate>;
    @Field(() => ToppingAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof ToppingAvgAggregate>;
    @Field(() => ToppingSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof ToppingSumAggregate>;
    @Field(() => ToppingMinAggregate, {nullable:true})
    _min?: InstanceType<typeof ToppingMinAggregate>;
    @Field(() => ToppingMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof ToppingMaxAggregate>;
}

@ArgsType()
export class CreateManyToppingArgs {
    @Field(() => [ToppingCreateManyInput], {nullable:false})
    @Type(() => ToppingCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<ToppingCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneToppingArgs {
    @Field(() => ToppingCreateInput, {nullable:false})
    @Type(() => ToppingCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof ToppingCreateInput>;
}

@ArgsType()
export class DeleteManyToppingArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
}

@ArgsType()
export class DeleteOneToppingArgs {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
}

@ArgsType()
export class FindFirstToppingArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => [ToppingOrderByWithRelationInput], {nullable:true})
    @Type(() => ToppingOrderByWithRelationInput)
    orderBy?: Array<ToppingOrderByWithRelationInput>;
    @Field(() => ToppingWhereUniqueInput, {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    cursor?: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ToppingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ToppingScalarFieldEnum>;
}

@ArgsType()
export class FindManyToppingArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => [ToppingOrderByWithRelationInput], {nullable:true})
    @Type(() => ToppingOrderByWithRelationInput)
    orderBy?: Array<ToppingOrderByWithRelationInput>;
    @Field(() => ToppingWhereUniqueInput, {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    cursor?: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [ToppingScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof ToppingScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueToppingArgs {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
}

@ArgsType()
export class ToppingAggregateArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => [ToppingOrderByWithRelationInput], {nullable:true})
    @Type(() => ToppingOrderByWithRelationInput)
    orderBy?: Array<ToppingOrderByWithRelationInput>;
    @Field(() => ToppingWhereUniqueInput, {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    cursor?: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => ToppingCountAggregateInput, {nullable:true})
    @Type(() => ToppingCountAggregateInput)
    _count?: InstanceType<typeof ToppingCountAggregateInput>;
    @Field(() => ToppingAvgAggregateInput, {nullable:true})
    @Type(() => ToppingAvgAggregateInput)
    _avg?: InstanceType<typeof ToppingAvgAggregateInput>;
    @Field(() => ToppingSumAggregateInput, {nullable:true})
    @Type(() => ToppingSumAggregateInput)
    _sum?: InstanceType<typeof ToppingSumAggregateInput>;
    @Field(() => ToppingMinAggregateInput, {nullable:true})
    @Type(() => ToppingMinAggregateInput)
    _min?: InstanceType<typeof ToppingMinAggregateInput>;
    @Field(() => ToppingMaxAggregateInput, {nullable:true})
    @Type(() => ToppingMaxAggregateInput)
    _max?: InstanceType<typeof ToppingMaxAggregateInput>;
}

@InputType()
export class ToppingAvgAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class ToppingAvgAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class ToppingAvgOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class ToppingCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    optionId?: true;
    @Field(() => Boolean, {nullable:true})
    ingredientLabelId?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class ToppingCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    description!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @Field(() => Int, {nullable:false})
    price!: number;
    @Field(() => Int, {nullable:false})
    optionId!: number;
    @Field(() => Int, {nullable:false})
    ingredientLabelId!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class ToppingCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    ingredientLabelId?: keyof typeof SortOrder;
}

@ObjectType()
export class ToppingCount {
    @Field(() => Int, {nullable:false})
    customerDishes?: number;
}

@InputType()
export class ToppingCreateManyIngredientLabelInputEnvelope {
    @Field(() => [ToppingCreateManyIngredientLabelInput], {nullable:false})
    @Type(() => ToppingCreateManyIngredientLabelInput)
    data!: Array<ToppingCreateManyIngredientLabelInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class ToppingCreateManyIngredientLabelInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
}

@InputType()
export class ToppingCreateManyOptionInputEnvelope {
    @Field(() => [ToppingCreateManyOptionInput], {nullable:false})
    @Type(() => ToppingCreateManyOptionInput)
    data!: Array<ToppingCreateManyOptionInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@InputType()
export class ToppingCreateManyOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
}

@InputType()
export class ToppingCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
}

@InputType()
export class ToppingCreateNestedManyWithoutCustomerDishesInput {
    @Field(() => [ToppingCreateWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create?: Array<ToppingCreateWithoutCustomerDishesInput>;
    @Field(() => [ToppingCreateOrConnectWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingCreateNestedManyWithoutIngredientLabelInput {
    @Field(() => [ToppingCreateWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create?: Array<ToppingCreateWithoutIngredientLabelInput>;
    @Field(() => [ToppingCreateOrConnectWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutIngredientLabelInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutIngredientLabelInput>;
    @Field(() => ToppingCreateManyIngredientLabelInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyIngredientLabelInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyIngredientLabelInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingCreateNestedManyWithoutOptionInput {
    @Field(() => [ToppingCreateWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateWithoutOptionInput)
    create?: Array<ToppingCreateWithoutOptionInput>;
    @Field(() => [ToppingCreateOrConnectWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutOptionInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutOptionInput>;
    @Field(() => ToppingCreateManyOptionInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyOptionInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyOptionInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingCreateOrConnectWithoutCustomerDishesInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingCreateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create!: InstanceType<typeof ToppingCreateWithoutCustomerDishesInput>;
}

@InputType()
export class ToppingCreateOrConnectWithoutIngredientLabelInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingCreateWithoutIngredientLabelInput, {nullable:false})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create!: InstanceType<typeof ToppingCreateWithoutIngredientLabelInput>;
}

@InputType()
export class ToppingCreateOrConnectWithoutOptionInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingCreateWithoutOptionInput, {nullable:false})
    @Type(() => ToppingCreateWithoutOptionInput)
    create!: InstanceType<typeof ToppingCreateWithoutOptionInput>;
}

@InputType()
export class ToppingCreateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OptionsCreateNestedOneWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutToppingsInput)
    option!: InstanceType<typeof OptionsCreateNestedOneWithoutToppingsInput>;
    @Field(() => IngradientLabelCreateNestedOneWithoutToppingsInput, {nullable:false})
    ingredientLabel!: InstanceType<typeof IngradientLabelCreateNestedOneWithoutToppingsInput>;
}

@InputType()
export class ToppingCreateWithoutIngredientLabelInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OptionsCreateNestedOneWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutToppingsInput)
    option!: InstanceType<typeof OptionsCreateNestedOneWithoutToppingsInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedToppingsInput>;
}

@InputType()
export class ToppingCreateWithoutOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => IngradientLabelCreateNestedOneWithoutToppingsInput, {nullable:false})
    ingredientLabel!: InstanceType<typeof IngradientLabelCreateNestedOneWithoutToppingsInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedToppingsInput>;
}

@InputType()
export class ToppingCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => OptionsCreateNestedOneWithoutToppingsInput, {nullable:false})
    @Type(() => OptionsCreateNestedOneWithoutToppingsInput)
    option!: InstanceType<typeof OptionsCreateNestedOneWithoutToppingsInput>;
    @Field(() => IngradientLabelCreateNestedOneWithoutToppingsInput, {nullable:false})
    ingredientLabel!: InstanceType<typeof IngradientLabelCreateNestedOneWithoutToppingsInput>;
    @Field(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishCreateNestedManyWithoutSelectedToppingsInput>;
}

@ArgsType()
export class ToppingGroupByArgs {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => [ToppingOrderByWithAggregationInput], {nullable:true})
    @Type(() => ToppingOrderByWithAggregationInput)
    orderBy?: Array<ToppingOrderByWithAggregationInput>;
    @Field(() => [ToppingScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof ToppingScalarFieldEnum>;
    @Field(() => ToppingScalarWhereWithAggregatesInput, {nullable:true})
    @Type(() => ToppingScalarWhereWithAggregatesInput)
    having?: InstanceType<typeof ToppingScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => ToppingCountAggregateInput, {nullable:true})
    @Type(() => ToppingCountAggregateInput)
    _count?: InstanceType<typeof ToppingCountAggregateInput>;
    @Field(() => ToppingAvgAggregateInput, {nullable:true})
    @Type(() => ToppingAvgAggregateInput)
    _avg?: InstanceType<typeof ToppingAvgAggregateInput>;
    @Field(() => ToppingSumAggregateInput, {nullable:true})
    @Type(() => ToppingSumAggregateInput)
    _sum?: InstanceType<typeof ToppingSumAggregateInput>;
    @Field(() => ToppingMinAggregateInput, {nullable:true})
    @Type(() => ToppingMinAggregateInput)
    _min?: InstanceType<typeof ToppingMinAggregateInput>;
    @Field(() => ToppingMaxAggregateInput, {nullable:true})
    @Type(() => ToppingMaxAggregateInput)
    _max?: InstanceType<typeof ToppingMaxAggregateInput>;
}

@ObjectType()
export class ToppingGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
    @Field(() => ToppingCountAggregate, {nullable:true})
    _count?: InstanceType<typeof ToppingCountAggregate>;
    @Field(() => ToppingAvgAggregate, {nullable:true})
    _avg?: InstanceType<typeof ToppingAvgAggregate>;
    @Field(() => ToppingSumAggregate, {nullable:true})
    _sum?: InstanceType<typeof ToppingSumAggregate>;
    @Field(() => ToppingMinAggregate, {nullable:true})
    _min?: InstanceType<typeof ToppingMinAggregate>;
    @Field(() => ToppingMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof ToppingMaxAggregate>;
}

@InputType()
export class ToppingListRelationFilter {
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    every?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    some?: InstanceType<typeof ToppingWhereInput>;
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    none?: InstanceType<typeof ToppingWhereInput>;
}

@InputType()
export class ToppingMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    optionId?: true;
    @Field(() => Boolean, {nullable:true})
    ingredientLabelId?: true;
}

@ObjectType()
export class ToppingMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    ingredientLabelId?: keyof typeof SortOrder;
}

@InputType()
export class ToppingMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    description?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    price?: true;
    @Field(() => Boolean, {nullable:true})
    optionId?: true;
    @Field(() => Boolean, {nullable:true})
    ingredientLabelId?: true;
}

@ObjectType()
export class ToppingMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    ingredientLabelId?: keyof typeof SortOrder;
}

@InputType()
export class ToppingOrderByRelationAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    _count?: keyof typeof SortOrder;
}

@InputType()
export class ToppingOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    ingredientLabelId?: keyof typeof SortOrder;
    @Field(() => ToppingCountOrderByAggregateInput, {nullable:true})
    @Type(() => ToppingCountOrderByAggregateInput)
    _count?: InstanceType<typeof ToppingCountOrderByAggregateInput>;
    @Field(() => ToppingAvgOrderByAggregateInput, {nullable:true})
    @Type(() => ToppingAvgOrderByAggregateInput)
    _avg?: InstanceType<typeof ToppingAvgOrderByAggregateInput>;
    @Field(() => ToppingMaxOrderByAggregateInput, {nullable:true})
    @Type(() => ToppingMaxOrderByAggregateInput)
    _max?: InstanceType<typeof ToppingMaxOrderByAggregateInput>;
    @Field(() => ToppingMinOrderByAggregateInput, {nullable:true})
    @Type(() => ToppingMinOrderByAggregateInput)
    _min?: InstanceType<typeof ToppingMinOrderByAggregateInput>;
    @Field(() => ToppingSumOrderByAggregateInput, {nullable:true})
    @Type(() => ToppingSumOrderByAggregateInput)
    _sum?: InstanceType<typeof ToppingSumOrderByAggregateInput>;
}

@InputType()
export class ToppingOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    description?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    optionId?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    ingredientLabelId?: keyof typeof SortOrder;
    @Field(() => OptionsOrderByWithRelationInput, {nullable:true})
    @Type(() => OptionsOrderByWithRelationInput)
    option?: InstanceType<typeof OptionsOrderByWithRelationInput>;
    @Field(() => IngradientLabelOrderByWithRelationInput, {nullable:true})
    ingredientLabel?: InstanceType<typeof IngradientLabelOrderByWithRelationInput>;
    @Field(() => CustomerDishOrderByRelationAggregateInput, {nullable:true})
    @Type(() => CustomerDishOrderByRelationAggregateInput)
    customerDishes?: InstanceType<typeof CustomerDishOrderByRelationAggregateInput>;
}

@InputType()
export class ToppingScalarWhereWithAggregatesInput {
    @Field(() => [ToppingScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => ToppingScalarWhereWithAggregatesInput)
    AND?: Array<ToppingScalarWhereWithAggregatesInput>;
    @Field(() => [ToppingScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => ToppingScalarWhereWithAggregatesInput)
    OR?: Array<ToppingScalarWhereWithAggregatesInput>;
    @Field(() => [ToppingScalarWhereWithAggregatesInput], {nullable:true})
    @Type(() => ToppingScalarWhereWithAggregatesInput)
    NOT?: Array<ToppingScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    description?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => StringNullableWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringNullableWithAggregatesFilter>;
    @Field(() => DecimalWithAggregatesFilter, {nullable:true})
    @Type(() => DecimalWithAggregatesFilter)
    price?: InstanceType<typeof DecimalWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    optionId?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    ingredientLabelId?: InstanceType<typeof StringWithAggregatesFilter>;
}

@InputType()
export class ToppingScalarWhereInput {
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    AND?: Array<ToppingScalarWhereInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    OR?: Array<ToppingScalarWhereInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    NOT?: Array<ToppingScalarWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    description?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    name?: InstanceType<typeof StringNullableFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    optionId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    ingredientLabelId?: InstanceType<typeof StringFilter>;
}

@InputType()
export class ToppingSumAggregateInput {
    @Field(() => Boolean, {nullable:true})
    price?: true;
}

@ObjectType()
export class ToppingSumAggregate {
    @Field(() => GraphQLDecimal, {nullable:true})
    price?: Decimal;
}

@InputType()
export class ToppingSumOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    price?: keyof typeof SortOrder;
}

@InputType()
export class ToppingUncheckedCreateNestedManyWithoutCustomerDishesInput {
    @Field(() => [ToppingCreateWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create?: Array<ToppingCreateWithoutCustomerDishesInput>;
    @Field(() => [ToppingCreateOrConnectWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingUncheckedCreateNestedManyWithoutIngredientLabelInput {
    @Field(() => [ToppingCreateWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create?: Array<ToppingCreateWithoutIngredientLabelInput>;
    @Field(() => [ToppingCreateOrConnectWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutIngredientLabelInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutIngredientLabelInput>;
    @Field(() => ToppingCreateManyIngredientLabelInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyIngredientLabelInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyIngredientLabelInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingUncheckedCreateNestedManyWithoutOptionInput {
    @Field(() => [ToppingCreateWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateWithoutOptionInput)
    create?: Array<ToppingCreateWithoutOptionInput>;
    @Field(() => [ToppingCreateOrConnectWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutOptionInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutOptionInput>;
    @Field(() => ToppingCreateManyOptionInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyOptionInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyOptionInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
}

@InputType()
export class ToppingUncheckedCreateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
}

@InputType()
export class ToppingUncheckedCreateWithoutIngredientLabelInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput>;
}

@InputType()
export class ToppingUncheckedCreateWithoutOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput>;
}

@InputType()
export class ToppingUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:false})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
    @Field(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput, {nullable:true})
    @Type(() => CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedCreateNestedManyWithoutSelectedToppingsInput>;
}

@InputType()
export class ToppingUncheckedUpdateManyWithoutCustomerDishesNestedInput {
    @Field(() => [ToppingCreateWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create?: Array<ToppingCreateWithoutCustomerDishesInput>;
    @Field(() => [ToppingCreateOrConnectWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutCustomerDishesInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutCustomerDishesInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUncheckedUpdateManyWithoutIngredientLabelNestedInput {
    @Field(() => [ToppingCreateWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create?: Array<ToppingCreateWithoutIngredientLabelInput>;
    @Field(() => [ToppingCreateOrConnectWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutIngredientLabelInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutIngredientLabelInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput>;
    @Field(() => ToppingCreateManyIngredientLabelInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyIngredientLabelInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyIngredientLabelInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutIngredientLabelInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutIngredientLabelInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUncheckedUpdateManyWithoutOptionNestedInput {
    @Field(() => [ToppingCreateWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateWithoutOptionInput)
    create?: Array<ToppingCreateWithoutOptionInput>;
    @Field(() => [ToppingCreateOrConnectWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutOptionInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutOptionInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutOptionInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutOptionInput>;
    @Field(() => ToppingCreateManyOptionInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyOptionInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyOptionInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutOptionInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutOptionInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutOptionInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutOptionInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUncheckedUpdateManyWithoutSelectedToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingUncheckedUpdateManyWithoutToppingsInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingUncheckedUpdateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
}

@InputType()
export class ToppingUncheckedUpdateWithoutIngredientLabelInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUncheckedUpdateWithoutOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => String, {nullable:true})
    optionId?: string;
    @Field(() => String, {nullable:true})
    ingredientLabelId?: string;
    @Field(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUncheckedUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
}

@InputType()
export class ToppingUpdateManyWithWhereWithoutCustomerDishesInput {
    @Field(() => ToppingScalarWhereInput, {nullable:false})
    @Type(() => ToppingScalarWhereInput)
    where!: InstanceType<typeof ToppingScalarWhereInput>;
    @Field(() => ToppingUpdateManyMutationInput, {nullable:false})
    @Type(() => ToppingUpdateManyMutationInput)
    data!: InstanceType<typeof ToppingUpdateManyMutationInput>;
}

@InputType()
export class ToppingUpdateManyWithWhereWithoutIngredientLabelInput {
    @Field(() => ToppingScalarWhereInput, {nullable:false})
    @Type(() => ToppingScalarWhereInput)
    where!: InstanceType<typeof ToppingScalarWhereInput>;
    @Field(() => ToppingUpdateManyMutationInput, {nullable:false})
    @Type(() => ToppingUpdateManyMutationInput)
    data!: InstanceType<typeof ToppingUpdateManyMutationInput>;
}

@InputType()
export class ToppingUpdateManyWithWhereWithoutOptionInput {
    @Field(() => ToppingScalarWhereInput, {nullable:false})
    @Type(() => ToppingScalarWhereInput)
    where!: InstanceType<typeof ToppingScalarWhereInput>;
    @Field(() => ToppingUpdateManyMutationInput, {nullable:false})
    @Type(() => ToppingUpdateManyMutationInput)
    data!: InstanceType<typeof ToppingUpdateManyMutationInput>;
}

@InputType()
export class ToppingUpdateManyWithoutCustomerDishesNestedInput {
    @Field(() => [ToppingCreateWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create?: Array<ToppingCreateWithoutCustomerDishesInput>;
    @Field(() => [ToppingCreateOrConnectWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutCustomerDishesInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutCustomerDishesInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutCustomerDishesInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutCustomerDishesInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutCustomerDishesInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUpdateManyWithoutIngredientLabelNestedInput {
    @Field(() => [ToppingCreateWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create?: Array<ToppingCreateWithoutIngredientLabelInput>;
    @Field(() => [ToppingCreateOrConnectWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutIngredientLabelInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutIngredientLabelInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput>;
    @Field(() => ToppingCreateManyIngredientLabelInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyIngredientLabelInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyIngredientLabelInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutIngredientLabelInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutIngredientLabelInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutIngredientLabelInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUpdateManyWithoutOptionNestedInput {
    @Field(() => [ToppingCreateWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateWithoutOptionInput)
    create?: Array<ToppingCreateWithoutOptionInput>;
    @Field(() => [ToppingCreateOrConnectWithoutOptionInput], {nullable:true})
    @Type(() => ToppingCreateOrConnectWithoutOptionInput)
    connectOrCreate?: Array<ToppingCreateOrConnectWithoutOptionInput>;
    @Field(() => [ToppingUpsertWithWhereUniqueWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpsertWithWhereUniqueWithoutOptionInput)
    upsert?: Array<ToppingUpsertWithWhereUniqueWithoutOptionInput>;
    @Field(() => ToppingCreateManyOptionInputEnvelope, {nullable:true})
    @Type(() => ToppingCreateManyOptionInputEnvelope)
    createMany?: InstanceType<typeof ToppingCreateManyOptionInputEnvelope>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    set?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    disconnect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    delete?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingWhereUniqueInput], {nullable:true})
    @Type(() => ToppingWhereUniqueInput)
    connect?: Array<ToppingWhereUniqueInput>;
    @Field(() => [ToppingUpdateWithWhereUniqueWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpdateWithWhereUniqueWithoutOptionInput)
    update?: Array<ToppingUpdateWithWhereUniqueWithoutOptionInput>;
    @Field(() => [ToppingUpdateManyWithWhereWithoutOptionInput], {nullable:true})
    @Type(() => ToppingUpdateManyWithWhereWithoutOptionInput)
    updateMany?: Array<ToppingUpdateManyWithWhereWithoutOptionInput>;
    @Field(() => [ToppingScalarWhereInput], {nullable:true})
    @Type(() => ToppingScalarWhereInput)
    deleteMany?: Array<ToppingScalarWhereInput>;
}

@InputType()
export class ToppingUpdateWithWhereUniqueWithoutCustomerDishesInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutCustomerDishesInput)
    data!: InstanceType<typeof ToppingUpdateWithoutCustomerDishesInput>;
}

@InputType()
export class ToppingUpdateWithWhereUniqueWithoutIngredientLabelInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutIngredientLabelInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutIngredientLabelInput)
    data!: InstanceType<typeof ToppingUpdateWithoutIngredientLabelInput>;
}

@InputType()
export class ToppingUpdateWithWhereUniqueWithoutOptionInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutOptionInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutOptionInput)
    data!: InstanceType<typeof ToppingUpdateWithoutOptionInput>;
}

@InputType()
export class ToppingUpdateWithoutCustomerDishesInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput)
    option?: InstanceType<typeof OptionsUpdateOneRequiredWithoutToppingsNestedInput>;
    @Field(() => IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    ingredientLabel?: InstanceType<typeof IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput>;
}

@InputType()
export class ToppingUpdateWithoutIngredientLabelInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput)
    option?: InstanceType<typeof OptionsUpdateOneRequiredWithoutToppingsNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUpdateWithoutOptionInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    ingredientLabel?: InstanceType<typeof IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(512)
    @Validator.MinLength(3)
    description?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => GraphQLDecimal, {nullable:true})
    @Type(() => Object)
    @Transform(transformToDecimal)
    price?: Decimal;
    @Field(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    @Type(() => OptionsUpdateOneRequiredWithoutToppingsNestedInput)
    option?: InstanceType<typeof OptionsUpdateOneRequiredWithoutToppingsNestedInput>;
    @Field(() => IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput, {nullable:true})
    ingredientLabel?: InstanceType<typeof IngradientLabelUpdateOneRequiredWithoutToppingsNestedInput>;
    @Field(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput, {nullable:true})
    @Type(() => CustomerDishUpdateManyWithoutSelectedToppingsNestedInput)
    customerDishes?: InstanceType<typeof CustomerDishUpdateManyWithoutSelectedToppingsNestedInput>;
}

@InputType()
export class ToppingUpsertWithWhereUniqueWithoutCustomerDishesInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutCustomerDishesInput)
    update!: InstanceType<typeof ToppingUpdateWithoutCustomerDishesInput>;
    @Field(() => ToppingCreateWithoutCustomerDishesInput, {nullable:false})
    @Type(() => ToppingCreateWithoutCustomerDishesInput)
    create!: InstanceType<typeof ToppingCreateWithoutCustomerDishesInput>;
}

@InputType()
export class ToppingUpsertWithWhereUniqueWithoutIngredientLabelInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutIngredientLabelInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutIngredientLabelInput)
    update!: InstanceType<typeof ToppingUpdateWithoutIngredientLabelInput>;
    @Field(() => ToppingCreateWithoutIngredientLabelInput, {nullable:false})
    @Type(() => ToppingCreateWithoutIngredientLabelInput)
    create!: InstanceType<typeof ToppingCreateWithoutIngredientLabelInput>;
}

@InputType()
export class ToppingUpsertWithWhereUniqueWithoutOptionInput {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingUpdateWithoutOptionInput, {nullable:false})
    @Type(() => ToppingUpdateWithoutOptionInput)
    update!: InstanceType<typeof ToppingUpdateWithoutOptionInput>;
    @Field(() => ToppingCreateWithoutOptionInput, {nullable:false})
    @Type(() => ToppingCreateWithoutOptionInput)
    create!: InstanceType<typeof ToppingCreateWithoutOptionInput>;
}

@InputType()
export class ToppingWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
}

@InputType()
export class ToppingWhereInput {
    @Field(() => [ToppingWhereInput], {nullable:true})
    @Type(() => ToppingWhereInput)
    AND?: Array<ToppingWhereInput>;
    @Field(() => [ToppingWhereInput], {nullable:true})
    @Type(() => ToppingWhereInput)
    OR?: Array<ToppingWhereInput>;
    @Field(() => [ToppingWhereInput], {nullable:true})
    @Type(() => ToppingWhereInput)
    NOT?: Array<ToppingWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    description?: InstanceType<typeof StringNullableFilter>;
    @Field(() => StringNullableFilter, {nullable:true})
    name?: InstanceType<typeof StringNullableFilter>;
    @Field(() => DecimalFilter, {nullable:true})
    @Type(() => DecimalFilter)
    price?: InstanceType<typeof DecimalFilter>;
    @Field(() => StringFilter, {nullable:true})
    optionId?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    ingredientLabelId?: InstanceType<typeof StringFilter>;
    @Field(() => OptionsRelationFilter, {nullable:true})
    @Type(() => OptionsRelationFilter)
    option?: InstanceType<typeof OptionsRelationFilter>;
    @Field(() => IngradientLabelRelationFilter, {nullable:true})
    ingredientLabel?: InstanceType<typeof IngradientLabelRelationFilter>;
    @Field(() => CustomerDishListRelationFilter, {nullable:true})
    @Type(() => CustomerDishListRelationFilter)
    customerDishes?: InstanceType<typeof CustomerDishListRelationFilter>;
}

@ObjectType()
export class Topping {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:true})
    description!: string | null;
    @Field(() => String, {nullable:true})
    name!: string | null;
    @Field(() => GraphQLDecimal, {nullable:false})
    price!: Decimal;
    @Field(() => String, {nullable:false})
    optionId!: string;
    @Field(() => String, {nullable:false})
    ingredientLabelId!: string;
    @Field(() => Options, {nullable:false})
    option?: InstanceType<typeof Options>;
    @Field(() => IngradientLabel, {nullable:false})
    ingredientLabel?: InstanceType<typeof IngradientLabel>;
    @Field(() => [CustomerDish], {nullable:true})
    customerDishes?: Array<CustomerDish>;
    @Field(() => ToppingCount, {nullable:false})
    _count?: InstanceType<typeof ToppingCount>;
}

@ArgsType()
export class UpdateManyToppingArgs {
    @Field(() => ToppingUpdateManyMutationInput, {nullable:false})
    @Type(() => ToppingUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof ToppingUpdateManyMutationInput>;
    @Field(() => ToppingWhereInput, {nullable:true})
    @Type(() => ToppingWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof ToppingWhereInput>;
}

@ArgsType()
export class UpdateOneToppingArgs {
    @Field(() => ToppingUpdateInput, {nullable:false})
    @Type(() => ToppingUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof ToppingUpdateInput>;
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneToppingArgs {
    @Field(() => ToppingWhereUniqueInput, {nullable:false})
    @Type(() => ToppingWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof ToppingWhereUniqueInput>;
    @Field(() => ToppingCreateInput, {nullable:false})
    @Type(() => ToppingCreateInput)
    create!: InstanceType<typeof ToppingCreateInput>;
    @Field(() => ToppingUpdateInput, {nullable:false})
    @Type(() => ToppingUpdateInput)
    update!: InstanceType<typeof ToppingUpdateInput>;
}

@ObjectType()
export class AggregateUser {
    @Field(() => UserCountAggregate, {nullable:true})
    _count?: InstanceType<typeof UserCountAggregate>;
    @Field(() => UserMinAggregate, {nullable:true})
    _min?: InstanceType<typeof UserMinAggregate>;
    @Field(() => UserMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof UserMaxAggregate>;
}

@ArgsType()
export class CreateManyUserArgs {
    @Field(() => [UserCreateManyInput], {nullable:false})
    @Type(() => UserCreateManyInput)
    @ValidateNested({ each: true })
    data!: Array<UserCreateManyInput>;
    @Field(() => Boolean, {nullable:true})
    skipDuplicates?: boolean;
}

@ArgsType()
export class CreateOneUserArgs {
    @Field(() => UserCreateInput, {nullable:false})
    @Type(() => UserCreateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof UserCreateInput>;
}

@ArgsType()
export class DeleteManyUserArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
}

@ArgsType()
export class DeleteOneUserArgs {
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof UserWhereUniqueInput>;
}

@ArgsType()
export class FindFirstUserArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
    @Field(() => [UserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<UserOrderByWithRelationInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [UserScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof UserScalarFieldEnum>;
}

@ArgsType()
export class FindManyUserArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
    @Field(() => [UserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<UserOrderByWithRelationInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => [UserScalarFieldEnum], {nullable:true})
    distinct?: Array<keyof typeof UserScalarFieldEnum>;
}

@ArgsType()
export class FindUniqueUserArgs {
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof UserWhereUniqueInput>;
}

@ArgsType()
export class UpdateManyUserArgs {
    @Field(() => UserUpdateManyMutationInput, {nullable:false})
    @Type(() => UserUpdateManyMutationInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof UserUpdateManyMutationInput>;
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
}

@ArgsType()
export class UpdateOneUserArgs {
    @Field(() => UserUpdateInput, {nullable:false})
    @Type(() => UserUpdateInput)
    @ValidateNested({ each: true })
    data!: InstanceType<typeof UserUpdateInput>;
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof UserWhereUniqueInput>;
}

@ArgsType()
export class UpsertOneUserArgs {
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    @ValidateNested({ each: true })
    where!: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => UserCreateInput, {nullable:false})
    @Type(() => UserCreateInput)
    create!: InstanceType<typeof UserCreateInput>;
    @Field(() => UserUpdateInput, {nullable:false})
    @Type(() => UserUpdateInput)
    update!: InstanceType<typeof UserUpdateInput>;
}

@ArgsType()
export class UserAggregateArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
    @Field(() => [UserOrderByWithRelationInput], {nullable:true})
    orderBy?: Array<UserOrderByWithRelationInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    cursor?: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => UserCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof UserCountAggregateInput>;
    @Field(() => UserMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof UserMinAggregateInput>;
    @Field(() => UserMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof UserMaxAggregateInput>;
}

@InputType()
export class UserCountAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    password?: true;
    @Field(() => Boolean, {nullable:true})
    phone?: true;
    @Field(() => Boolean, {nullable:true})
    roles?: true;
    @Field(() => Boolean, {nullable:true})
    _all?: true;
}

@ObjectType()
export class UserCountAggregate {
    @Field(() => Int, {nullable:false})
    id!: number;
    @Field(() => Int, {nullable:false})
    createdAt!: number;
    @Field(() => Int, {nullable:false})
    updatedAt!: number;
    @Field(() => Int, {nullable:false})
    email!: number;
    @Field(() => Int, {nullable:false})
    name!: number;
    @HideField()
    password!: number;
    @Field(() => Int, {nullable:false})
    phone!: number;
    @Field(() => Int, {nullable:false})
    roles!: number;
    @Field(() => Int, {nullable:false})
    _all!: number;
}

@InputType()
export class UserCountOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    phone?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    roles?: keyof typeof SortOrder;
}

@ObjectType()
export class UserCount {
    @Field(() => Int, {nullable:false})
    orders?: number;
}

@InputType()
export class UserCreateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserCreateNestedOneWithoutOrdersInput {
    @Field(() => UserCreateWithoutOrdersInput, {nullable:true})
    @Type(() => UserCreateWithoutOrdersInput)
    create?: InstanceType<typeof UserCreateWithoutOrdersInput>;
    @Field(() => UserCreateOrConnectWithoutOrdersInput, {nullable:true})
    @Type(() => UserCreateOrConnectWithoutOrdersInput)
    connectOrCreate?: InstanceType<typeof UserCreateOrConnectWithoutOrdersInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    @Type(() => UserWhereUniqueInput)
    connect?: InstanceType<typeof UserWhereUniqueInput>;
}

@InputType()
export class UserCreateOrConnectWithoutOrdersInput {
    @Field(() => UserWhereUniqueInput, {nullable:false})
    @Type(() => UserWhereUniqueInput)
    where!: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => UserCreateWithoutOrdersInput, {nullable:false})
    @Type(() => UserCreateWithoutOrdersInput)
    create!: InstanceType<typeof UserCreateWithoutOrdersInput>;
}

@InputType()
export class UserCreateWithoutOrdersInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
    @Field(() => OrderCreateNestedManyWithoutUserInput, {nullable:true})
    @Type(() => OrderCreateNestedManyWithoutUserInput)
    orders?: InstanceType<typeof OrderCreateNestedManyWithoutUserInput>;
}

@InputType()
export class UserCreaterolesInput {
    @Field(() => [EnumRole], {nullable:false})
    set!: Array<keyof typeof EnumRole>;
}

@ArgsType()
export class UserGroupByArgs {
    @Field(() => UserWhereInput, {nullable:true})
    @Type(() => UserWhereInput)
    @ValidateNested({ each: true })
    where?: InstanceType<typeof UserWhereInput>;
    @Field(() => [UserOrderByWithAggregationInput], {nullable:true})
    orderBy?: Array<UserOrderByWithAggregationInput>;
    @Field(() => [UserScalarFieldEnum], {nullable:false})
    by!: Array<keyof typeof UserScalarFieldEnum>;
    @Field(() => UserScalarWhereWithAggregatesInput, {nullable:true})
    having?: InstanceType<typeof UserScalarWhereWithAggregatesInput>;
    @Field(() => Int, {nullable:true})
    take?: number;
    @Field(() => Int, {nullable:true})
    skip?: number;
    @Field(() => UserCountAggregateInput, {nullable:true})
    _count?: InstanceType<typeof UserCountAggregateInput>;
    @Field(() => UserMinAggregateInput, {nullable:true})
    _min?: InstanceType<typeof UserMinAggregateInput>;
    @Field(() => UserMaxAggregateInput, {nullable:true})
    _max?: InstanceType<typeof UserMaxAggregateInput>;
}

@ObjectType()
export class UserGroupBy {
    @Field(() => String, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date | string;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @HideField()
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
    @Field(() => UserCountAggregate, {nullable:true})
    _count?: InstanceType<typeof UserCountAggregate>;
    @Field(() => UserMinAggregate, {nullable:true})
    _min?: InstanceType<typeof UserMinAggregate>;
    @Field(() => UserMaxAggregate, {nullable:true})
    _max?: InstanceType<typeof UserMaxAggregate>;
}

@InputType()
export class UserMaxAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    password?: true;
    @Field(() => Boolean, {nullable:true})
    phone?: true;
}

@ObjectType()
export class UserMaxAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @HideField()
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
}

@InputType()
export class UserMaxOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    phone?: keyof typeof SortOrder;
}

@InputType()
export class UserMinAggregateInput {
    @Field(() => Boolean, {nullable:true})
    id?: true;
    @HideField()
    createdAt?: true;
    @HideField()
    updatedAt?: true;
    @Field(() => Boolean, {nullable:true})
    email?: true;
    @Field(() => Boolean, {nullable:true})
    name?: true;
    @Field(() => Boolean, {nullable:true})
    password?: true;
    @Field(() => Boolean, {nullable:true})
    phone?: true;
}

@ObjectType()
export class UserMinAggregate {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => Date, {nullable:true})
    createdAt?: Date | string;
    @Field(() => Date, {nullable:true})
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @HideField()
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
}

@InputType()
export class UserMinOrderByAggregateInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    phone?: keyof typeof SortOrder;
}

@InputType()
export class UserOrderByWithAggregationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    phone?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    roles?: keyof typeof SortOrder;
    @Field(() => UserCountOrderByAggregateInput, {nullable:true})
    _count?: InstanceType<typeof UserCountOrderByAggregateInput>;
    @Field(() => UserMaxOrderByAggregateInput, {nullable:true})
    _max?: InstanceType<typeof UserMaxOrderByAggregateInput>;
    @Field(() => UserMinOrderByAggregateInput, {nullable:true})
    _min?: InstanceType<typeof UserMinOrderByAggregateInput>;
}

@InputType()
export class UserOrderByWithRelationInput {
    @Field(() => SortOrder, {nullable:true})
    id?: keyof typeof SortOrder;
    @HideField()
    createdAt?: keyof typeof SortOrder;
    @HideField()
    updatedAt?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    email?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    name?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    password?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    phone?: keyof typeof SortOrder;
    @Field(() => SortOrder, {nullable:true})
    roles?: keyof typeof SortOrder;
    @Field(() => OrderOrderByRelationAggregateInput, {nullable:true})
    @Type(() => OrderOrderByRelationAggregateInput)
    orders?: InstanceType<typeof OrderOrderByRelationAggregateInput>;
}

@InputType()
export class UserRelationFilter {
    @Field(() => UserWhereInput, {nullable:true})
    is?: InstanceType<typeof UserWhereInput>;
    @Field(() => UserWhereInput, {nullable:true})
    isNot?: InstanceType<typeof UserWhereInput>;
}

@InputType()
export class UserScalarWhereWithAggregatesInput {
    @Field(() => [UserScalarWhereWithAggregatesInput], {nullable:true})
    AND?: Array<UserScalarWhereWithAggregatesInput>;
    @Field(() => [UserScalarWhereWithAggregatesInput], {nullable:true})
    OR?: Array<UserScalarWhereWithAggregatesInput>;
    @Field(() => [UserScalarWhereWithAggregatesInput], {nullable:true})
    NOT?: Array<UserScalarWhereWithAggregatesInput>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    id?: InstanceType<typeof StringWithAggregatesFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    email?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    name?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    password?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => StringWithAggregatesFilter, {nullable:true})
    phone?: InstanceType<typeof StringWithAggregatesFilter>;
    @Field(() => EnumEnumRoleNullableListFilter, {nullable:true})
    roles?: InstanceType<typeof EnumEnumRoleNullableListFilter>;
}

@InputType()
export class UserUncheckedCreateWithoutOrdersInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUncheckedCreateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:false})
    @Validator.IsEmail()
    email!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password!: string;
    @Field(() => String, {nullable:false})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
    @Field(() => OrderUncheckedCreateNestedManyWithoutUserInput, {nullable:true})
    @Type(() => OrderUncheckedCreateNestedManyWithoutUserInput)
    orders?: InstanceType<typeof OrderUncheckedCreateNestedManyWithoutUserInput>;
}

@InputType()
export class UserUncheckedUpdateManyInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUncheckedUpdateWithoutOrdersInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUncheckedUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
    @Field(() => OrderUncheckedUpdateManyWithoutUserNestedInput, {nullable:true})
    @Type(() => OrderUncheckedUpdateManyWithoutUserNestedInput)
    orders?: InstanceType<typeof OrderUncheckedUpdateManyWithoutUserNestedInput>;
}

@InputType()
export class UserUpdateManyMutationInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUpdateOneRequiredWithoutOrdersNestedInput {
    @Field(() => UserCreateWithoutOrdersInput, {nullable:true})
    @Type(() => UserCreateWithoutOrdersInput)
    create?: InstanceType<typeof UserCreateWithoutOrdersInput>;
    @Field(() => UserCreateOrConnectWithoutOrdersInput, {nullable:true})
    @Type(() => UserCreateOrConnectWithoutOrdersInput)
    connectOrCreate?: InstanceType<typeof UserCreateOrConnectWithoutOrdersInput>;
    @Field(() => UserUpsertWithoutOrdersInput, {nullable:true})
    @Type(() => UserUpsertWithoutOrdersInput)
    upsert?: InstanceType<typeof UserUpsertWithoutOrdersInput>;
    @Field(() => UserWhereUniqueInput, {nullable:true})
    @Type(() => UserWhereUniqueInput)
    connect?: InstanceType<typeof UserWhereUniqueInput>;
    @Field(() => UserUpdateWithoutOrdersInput, {nullable:true})
    @Type(() => UserUpdateWithoutOrdersInput)
    update?: InstanceType<typeof UserUpdateWithoutOrdersInput>;
}

@InputType()
export class UserUpdateWithoutOrdersInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUpdateInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @HideField()
    createdAt?: Date | string;
    @HideField()
    updatedAt?: Date | string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    name?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(8)
    password?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
    @Field(() => [EnumRole], {nullable:true})
    @Validator.IsEnum(EnumRole, { each: true })
    @Validator.ArrayUnique()
    roles?: Array<keyof typeof EnumRole>;
    @Field(() => OrderUpdateManyWithoutUserNestedInput, {nullable:true})
    @Type(() => OrderUpdateManyWithoutUserNestedInput)
    orders?: InstanceType<typeof OrderUpdateManyWithoutUserNestedInput>;
}

@InputType()
export class UserUpdaterolesInput {
    @Field(() => [EnumRole], {nullable:true})
    set?: Array<keyof typeof EnumRole>;
    @Field(() => [EnumRole], {nullable:true})
    push?: Array<keyof typeof EnumRole>;
}

@InputType()
export class UserUpsertWithoutOrdersInput {
    @Field(() => UserUpdateWithoutOrdersInput, {nullable:false})
    @Type(() => UserUpdateWithoutOrdersInput)
    update!: InstanceType<typeof UserUpdateWithoutOrdersInput>;
    @Field(() => UserCreateWithoutOrdersInput, {nullable:false})
    @Type(() => UserCreateWithoutOrdersInput)
    create!: InstanceType<typeof UserCreateWithoutOrdersInput>;
}

@InputType()
export class UserWhereUniqueInput {
    @Field(() => String, {nullable:true})
    id?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsEmail()
    email?: string;
    @Field(() => String, {nullable:true})
    @Validator.IsString()
    @Validator.MaxLength(100)
    @Validator.MinLength(3)
    @Validator.IsMobilePhone()
    phone?: string;
}

@InputType()
export class UserWhereInput {
    @Field(() => [UserWhereInput], {nullable:true})
    AND?: Array<UserWhereInput>;
    @Field(() => [UserWhereInput], {nullable:true})
    OR?: Array<UserWhereInput>;
    @Field(() => [UserWhereInput], {nullable:true})
    NOT?: Array<UserWhereInput>;
    @Field(() => StringFilter, {nullable:true})
    id?: InstanceType<typeof StringFilter>;
    @HideField()
    createdAt?: InstanceType<typeof DateTimeFilter>;
    @HideField()
    updatedAt?: InstanceType<typeof DateTimeFilter>;
    @Field(() => StringFilter, {nullable:true})
    email?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    name?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    password?: InstanceType<typeof StringFilter>;
    @Field(() => StringFilter, {nullable:true})
    phone?: InstanceType<typeof StringFilter>;
    @Field(() => EnumEnumRoleNullableListFilter, {nullable:true})
    roles?: InstanceType<typeof EnumEnumRoleNullableListFilter>;
    @Field(() => OrderListRelationFilter, {nullable:true})
    @Type(() => OrderListRelationFilter)
    orders?: InstanceType<typeof OrderListRelationFilter>;
}

@ObjectType()
export class User {
    @Field(() => ID, {nullable:false})
    id!: string;
    @Field(() => Date, {nullable:false})
    createdAt!: Date;
    @Field(() => Date, {nullable:false})
    updatedAt!: Date;
    @Field(() => String, {nullable:false})
    email!: string;
    @Field(() => String, {nullable:false})
    name!: string;
    @HideField()
    password!: string;
    @Field(() => String, {nullable:false})
    phone!: string;
    @Field(() => [EnumRole], {nullable:true})
    roles!: Array<keyof typeof EnumRole>;
    @Field(() => [Order], {nullable:true})
    orders?: Array<Order>;
    @Field(() => UserCount, {nullable:false})
    _count?: InstanceType<typeof UserCount>;
}
